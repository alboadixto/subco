<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model {

    public function user() {
        return $this->belongsTo('App\User');
    }
  

    public function punto() {
        $pto = EmpleadoPunto::where('empleado_id', $this->id)->get();
        $pto_object = Pto_Servicio::findOrFail($pto[0]->pto_servicio_id);
        return User::findOrFail($pto_object->user_id);
    }

    public function tiene_punto() {
        if (count(EmpleadoPunto::where('empleado_id', $this->id)->get()) > 0) {
            $pto = EmpleadoPunto::where('empleado_id', $this->id)->get();
            if (Pto_Servicio::findOrFail($pto[0]->pto_servicio_id)) {
                return TRUE;
            }
        } else {
            return FALSE;
        }
    }

}
