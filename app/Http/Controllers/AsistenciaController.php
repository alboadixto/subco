<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AsistenciaController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function listar() {
        return view('empleados/nomina_todos');
        
    }
    public function destroy($id) {
        //
    }

    public function hoy_view() {
        // $asis_hoy = \App\Asistencia::where('user_id', Auth::user()->id)->where('dia','222')get();
        //  return $asis_hoy;
        return view('asistencia\hoy');
    }

    public function empleados_view() {
        // $asis_hoy = \App\Asistencia::where('user_id', Auth::user()->id)->where('dia','222')get();
        //  return $asis_hoy;
        return view('asistencia\empleados');
    }

    public function dias_entes_view() {
        // $asis_hoy = \App\Asistencia::where('user_id', Auth::user()->id)->where('dia','222')get();
        //  return $asis_hoy;
        $hoy = date('Y-m-d');
        return view('asistencia\dias_antes', compact('hoy'));
    }

    public function informar_hoy(Request $r) {
        $asis_hoy = \App\Asistencia::where('dia', date('Y-m-d'))->where('user_id',Auth::user()->id)->first();

        if ($asis_hoy) {
            $asis_hoy->user_id = Auth::user()->id;
            $asis_hoy->entrada = $r->llegada;
            $asis_hoy->salida = $r->salida;
            $asis_hoy->dia = date('Y-m-d');
            $asis_hoy->confirmado = 'NO';
            $asis_hoy->save();
        } else {
            $asis_hoy = new \App\Asistencia();
            $asis_hoy->user_id = Auth::user()->id;
            $asis_hoy->entrada = $r->llegada;
            $asis_hoy->salida = $r->salida;
            $asis_hoy->dia = date('Y-m-d');
            $asis_hoy->confirmado = 'NO';
            $asis_hoy->save();
        }
        return 'Asistencia Informada';
    }

    public function informar_dia(Request $r) {
        $asis_hoy = \App\Asistencia::where('dia', $r->dia)->where('user_id',Auth::user()->id)->first();

        if ($asis_hoy) {
            $asis_hoy->user_id = Auth::user()->id;
            $asis_hoy->entrada = $r->llegada;
            $asis_hoy->salida = $r->salida;
            $asis_hoy->dia = $r->dia;
            $asis_hoy->confirmado = 'NO';
            $asis_hoy->save();
        } else {
            $asis_hoy = new \App\Asistencia();
            $asis_hoy->user_id = Auth::user()->id;
            $asis_hoy->entrada = $r->llegada;
            $asis_hoy->salida = $r->salida;
            $asis_hoy->dia = $r->dia;
            $asis_hoy->confirmado = 'NO';
            $asis_hoy->save();
        }
        return 'Asistencia Informada';
    }

    public function asistencia_empleados() {
        $users = \App\User::all();
        $out = array();
        foreach ($users as $u) {
            $out[$u->id] = $this->asistencia_empleado($u->id);
        }
        return $out;
    }
    //METODO PARA CALCULAR EL PAGO DE UN TRABAJADOR
    //CALCULA LAS HORAS EN BASE A LAS HORAS LOS MINUTOS
    //$ID ID DEL USUARIO
    //$INICIO FECHA DE INICIO DEL PERIODO
    //$FIN FECHA FIN DEL PERIODO
    //$PROVIDA VALOR DE PROVIDA DEL PERIODO ESTE ES INTRODUCIDO POR EL ADMIN
    //$ANTICIPO UN VALOR INTRODUCIDO POR EL ADMIN PARA DESCONTAR SI EL USUARIO TIENE ALGUN ANTICIPO
    //$BONO LO INTRODUCE EL ADMIN PARA SI EL USUARIO TIENE ALGUN BONO POR BUEN COMPOTAMIENTO
    //$OTRO_PAGO  Y $OTRO_DESCUENTO PARA SI SE NECESITA HACER CUALKIER OTRO TIPO DE PAGO ODESCUENTO
    public function pago_empleados($id, $inicio, $fin,$provida,$anticipo,$bono,$otro_pago,$otro_descuento) {
        //CALCULO DE HORAS LABORADAS
        $user = \App\User::findOrFail($id);
        $empleado = \App\Empleado::where('user_id',$user->id)->first();
                $contrato = \App\ContratoEmpleado::where('empleado_id',$user->id)->first();
                if(!$contrato){
                    return 'Debe tener un contrato de este empleado registrado en el sistema para poder generar su nómina';
                }
        $asistencia = \App\Asistencia::where('user_id', $id)->where('dia', '>=', $inicio)->where('dia', '<=', $fin)->get();
        $total_asis = 0;
        $cant_confir = 0;
        $h = 0;
        $m = 0;
        $h_t = 0;
        $m_t = 0;
        foreach ($asistencia as $a) {
            $total_asis++;
            if ($a->confirmado === 'SI') {
                $cant_confir++;
                $h_inicio = intval(substr($a->entrada, 0, 2));
                $m_inicio = intval(substr($a->entrada, 3, 2));
                $h_final = intval(substr($a->salida, 0, 2));
                $m_final = intval(substr($a->salida, 3, 2));
                if (($m_final - $m_inicio) < 0) {
                    $h += (((($h_final - $h_inicio) - 1)) * 60) + ($m_inicio + $m_final);
                } else {
                    $h += (($h_final - $h_inicio) * 60) + ($m_final - $m_inicio);
                }
            } 
                $h_inicio = intval(substr($a->entrada, 0, 2));
                $m_inicio = intval(substr($a->entrada, 3, 2));
                $h_final = intval(substr($a->salida, 0, 2));
                $m_final = intval(substr($a->salida, 3, 2));
                if (($m_final - $m_inicio) < 0) {
                    $h_t += (((($h_final - $h_inicio) - 1)) * 60) + ($m_inicio + $m_final);
                } else {
                    $h_t += (($h_final - $h_inicio) * 60) + ($m_final - $m_inicio);
                }
            
        }
        $m_t = $h_t % 60;
        $m = $h % 60;
        $h = intval($h / 60).'.'.$m;
        $h_t = intval($h_t / 60).'.'.$m_t;
        //CALCULOS DEL PAGO
        $cant_dias_trabajados=$h/9;
        //$cant_dias_trabajados=30;
        $sueldo_base = $contrato->sueldo_base/30*$cant_dias_trabajados;
        $gratificacion = ($sueldo_base*25)/100;
        $imponible = $sueldo_base+$gratificacion;
        $movilizacion= $contrato->movilizacion*$cant_dias_trabajados;
        $colocacion= ($contrato->colocacion*$cant_dias_trabajados);
        $achs = ($imponible*0.93)/100;
        $afc = ($imponible*3)/100;
        $sis = ($imponible*1.53)/100;
        $provida =($imponible*$provida)/100;
        $fonasa = ($imponible*7)/100;
        $total_haber = $sueldo_base+$gratificacion+$imponible+$colocacion+$movilizacion+$bono+$otro_pago;
        $total_descuentos =$provida+$fonasa+$anticipo+$otro_descuento;
        $total_recibir = $total_haber-$total_descuentos;
      
        
        return ' <div class="modal-body">
                                         <center><img src="'. url('/').'/img/logo.png"></center>
                        <center>Liquidación de Remuneración</center>
                       <center> <p>SERVICIOS DE RECURSOS HUMANOS  Y LIMPIEZA "SUBCO" LTDA</p> </center><center> <p> Fecha Inicio:'.$inicio.'  Fecha Fin:'.$fin.'</p></center>
                        <p><span style="float: left">Nombre: '.$user->name.'</span><span style="float: right">Dias de Trabajo: '.$cant_dias_trabajados.'</span></p><br>
                        <p><span style="float: left">RUT: '.$empleado->rut.'</span><span style="float: right">ACHS 0.93: '.$achs.'</span></p><br>
                        <p><span style="float: left">AFC EMPLEADOR 3%: '.$afc.'</span><span style="float: right">SIS 1.53%:</span></p><br>
                        <p><span style="float: left">CARGO: '.$user->cargo.'</span><span style="float: right">SIS 1.53%: '.$sis.'</span></p><br>
                        <hr>
                            <p><span style="float: left"><h4>HABERES</span><span style="float: right">DESCUENTOS</h4></span></p><br>
                            <p><span style="float: left">SUELDO BASE: '.$sueldo_base.'</span><span style="float: right">PROVIDA: '.$provida.'</span></p><br>
                            <p><span style="float: left">GRATIFICACIÖN LEGAL 25%: '.$gratificacion.'</span><span style="float: right">FONASA 7%: '.$fonasa.'</span></p><br>
                            <p><span style="float: left">REMUNERACIÓN IMPONIBLE: '.$imponible.'</span><span style="float: right">ANTICIPO: '.$anticipo.'</span></p><br>
                            <p><span style="float: left">ASIGNACIÓN POR MOVILIZACIÓN: '.$movilizacion.'</span><span style="float: right">OTRO DESCUENTO: '.$otro_descuento.'</span></p><br>
                            <p><span style="float: left">ASIGNACIÓN POR COLOCACIÓN: '.$colocacion.'</span></p><br>
                            <p><span style="float: left">BONO: '.$bono.'</span></p><br>
                            <p><span style="float: left">OTRO PAGO:  '.$otro_pago.'</span></p><br>
                            <hr>
                                <p><span style="float: left">TOTAL HABERES: '.$total_haber.'</span><span style="float: right">TOTAL DESCUENTO: '.$total_descuentos.'</span></p>
                                <hr>
                                    <p><span style="float: left">LÍQUIDO A RECIBIR: '.$total_recibir.'</p>
                                    <hr>
                                                                               <hr><br>
                                           <p> NOTA: La presente liquidación es valida como certificado de remuneración</p>
                                                                                                                 
                                                                            <p>Firma del empleado</p>
                                                                               _________________
                                             </div>';
        
    //return json_encode(['fecha_incio'=>$inicio,'fecha_fin'=>$fin,'dias_trabajados'=>$cant_dias_trabajados,'nombre'=>$user->name,'rut'=>$empleado->rut,'cargo'=>$user->cargo,'sueldo_base'=>$sueldo_base,'gratificacion'=>$gratificacion,'imponible'=>$imponible,'colocación'=>$colocacion,'movilizacion'=>$movilizacion,'achs'=>$achs,'afs'=>$afc,'sis'=>$sis,'provida'=>$provida,'fonasa'=>$fonasa,'total_haber'=>$total_haber,'total_descuentos'=>$total_descuentos,'total_recibir'=>$total_recibir,'anticipo'=>$anticipo,'bono'=>$bono,'otro_pago'=>$otro_pago,'otro_descuento'=>$otro_descuento]);
        
    }

//MUESTRA LA VISTA DE LA TABLA CON LA ASISTENCIA DE UN EMPLEADO EN UN RANGO DE FECHA CON TOTAL DE HORAS
    public function asistencia_empleado_view($id, $inicio, $fin) {
        $user = \App\User::findOrFail($id);
        $asistencia = \App\Asistencia::where('user_id', $id)->where('dia', '>=', $inicio)->where('dia', '<=', $fin)->get();
        $total_asis = 0;
        $cant_confir = 0;
        $h = 0;
        $m = 0;
        $h_t = 0;
        $m_t = 0;
        foreach ($asistencia as $a) {
            $total_asis++;
            if ($a->confirmado === 'SI') {
                $cant_confir++;
                $h_inicio = intval(substr($a->entrada, 0, 2));
                $m_inicio = intval(substr($a->entrada, 3, 2));
                $h_final = intval(substr($a->salida, 0, 2));
                $m_final = intval(substr($a->salida, 3, 2));
                if (($m_final - $m_inicio) < 0) {
                    $h += (((($h_final - $h_inicio) - 1)) * 60) + ($m_inicio + $m_final);
                } else {
                    $h += (($h_final - $h_inicio) * 60) + ($m_final - $m_inicio);
                }
            } 
                $h_inicio = intval(substr($a->entrada, 0, 2));
                $m_inicio = intval(substr($a->entrada, 3, 2));
                $h_final = intval(substr($a->salida, 0, 2));
                $m_final = intval(substr($a->salida, 3, 2));
                if (($m_final - $m_inicio) < 0) {
                    $h_t += (((($h_final - $h_inicio) - 1)) * 60) + ($m_inicio + $m_final);
                } else {
                    $h_t += (($h_final - $h_inicio) * 60) + ($m_final - $m_inicio);
                }
            
        }
        $m_t = $h_t % 60;
        $m = $h % 60;
        $h = intval($h / 60).'.'.$m;
        $h_t = intval($h_t / 60).'.'.$m_t;


        return view('asistencia\empleado', compact('id', 'inicio', 'fin', 'user', 'total_asis', 'cant_confir', 'h', 'h_t'));
    }

    
    
    
    
//RESULTADOS FORMATEADOS PARA EL DATATABLE
    public function asistencia_empleado($id, $inicio, $fin) {
        $user = \App\User::findOrFail($id);
        $asistencia = \App\Asistencia::where('user_id', $id)->where('dia', '>=', $inicio)->where('dia', '<=', $fin)->get();
        return datatables()->collection($asistencia)->toJson();
    }

    //CAMBIAR ESTADO DE LA ASISTENCIA A CONFIRMADA
    public function asistencia_confirmada($id) {
        $asistencia = \App\Asistencia::findOrFail($id);
        if ($asistencia) {
            $asistencia->confirmado = 'SI';
            $asistencia->save();
            return 'Confirmación Realizada';
        } else {
            return 'Error al cambiar el estado de la asistencia';
        }
    }

    //CAMBIAR ESTADO DE LA ASISTENCIA A DENEGADA
    public function asistencia_denegada($id) {
        $asistencia = \App\Asistencia::findOrFail($id);
        if ($asistencia) {
            $asistencia->confirmado = 'NO';
            $asistencia->save();
            return 'Asistencia Denegada';
        } else {
            return 'Error al cambiar el estado de la asistencia';
        }
    }

}
