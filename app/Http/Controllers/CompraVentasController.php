<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CompraVentasController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return datatables()->collection(\Illuminate\Support\Facades\DB::select("SELECT users.id as id,users.name as nombre,users.cargo as cargo,users.direccion as direccion,users.telefono as telefono,users.email as email,roles.nombre as rol FROM users INNER JOIN roles ON roles.id = users.role_id"))->toJson();
    }

    public function listar_compras()
    {
        return view('compra_venta.mostar_compras');
    }
    public function listar_ventas()
    {
        return view('compra_venta.mostar_ventas');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $productos = \App\Producto::all();
        return view('compra_venta.insertar', compact('productos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
