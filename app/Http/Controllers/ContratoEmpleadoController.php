<?php

namespace App\Http\Controllers;

use App\ContratoEmpleado;
use Illuminate\Http\Request;

class ContratoEmpleadoController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

//MUESTRA LA VISTA DE UN CONTRATO DE UN EMPLEADO ESPECIFICO CON LA POSIBILIDAD DE MODIFICAR E IMPRIMIR
    public function mostrar_contrato_empleado($id_empleado) {
        $empleado = \App\User::find($id_empleado);
        $empleado_obj = \App\Empleado::where('user_id', $id_empleado)->first();
        $contrato = ContratoEmpleado::where('empleado_id', $id_empleado)->first();
        if ($contrato) {

            return view('contratos\contrato', compact('contrato', 'empleado', 'empleado_obj'));
        } else {
            session()->flash('sin_contrato', 'El empleado seleccionado no tiene contrato en el sistema, Puede crearlo si desea');
            return $this->crear_contrato_empleado_view();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContratoEmpleado  $contratoEmpleado
     * @return \Illuminate\Http\Response
     */
    public function show(ContratoEmpleado $contratoEmpleado) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContratoEmpleado  $contratoEmpleado
     * @return \Illuminate\Http\Response
     */
    public function edit(ContratoEmpleado $contratoEmpleado) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContratoEmpleado  $contratoEmpleado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContratoEmpleado $contratoEmpleado) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContratoEmpleado  $contratoEmpleado
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContratoEmpleado $contratoEmpleado) {
        //
    }

//MUESTRA LA VISTA DE CREAR CONTRATO A LOS EMPLEADOS PASANDOLE SOLO LOS EMPLEADOS QUE NO TIENEN CONTRATO AUN EN EL SISTEMA
    public function crear_contrato_empleado_view() {
        $empleados = \App\User::where('role_id', 7)->get();

        foreach ($empleados as $e) {
            if (!$e->contrato()) {
                $out[] = $e;
            }
        }
        if (empty($out)) {
            $empleados = 'NO';
        } else {
            $empleados = $out;
        }
        return view('contratos\crear_contratos_empleados', compact('empleados'));
    }

    //ACCION DE CREAR EL CONTRATO LA CUAL BORRA AL EMPLEADO DEL SISITEMA Y ELIMINA EL CONTRATO DEL SISITEMA
    public function crear_termino_empleado(Request $r) {
        $finiquito = new \App\FinquitoEmpelados();
        $finiquito->fecha_dia = $r->f_fecha_dia;
        $finiquito->fecha_mes = $r->f_fecha_mes;
        $finiquito->fecha_ano = $r->f_fecha_ano;
        $finiquito->nombre_trabajador = $r->f_nombre_trabajador;
        $finiquito->rut_trabajador = $r->f_rut_trabajador;
        $finiquito->direccion_trabajador = $r->f_direccion_trabajador;
        $finiquito->fecha_desde = $r->f_fecha_desde;
        $finiquito->fecha_hasta = $r->f_fecha_hasta;
        $finiquito->total_mes = $r->f_total_mes;
        $finiquito->total = $r->f_total;
        $finiquito->save();
        $termino = new \App\TerminoEmpleado();
        //al no necesitar el empleado_id guardo en este campo el id del finikito para poder eleiminar luego los 2
        $termino->empleado_id = $finiquito->id;
        $termino->nombre_empleado = $r->nombre_empleado;
        $termino->fecha_dia = $r->fecha_dia;
        $termino->fecha_mes = $r->fecha_mes;
        $termino->fecha_ano = $r->fecha_ano;
        $termino->nombre_empleado = $r->nombre_empleado;
        $termino->rut_empleado = $r->rut_empleado;
        $termino->fecha_vinculo_contrato = $r->fecha_vinculo_contrato;
        $termino->ausencias = $r->ausencias;
        $termino->save();
        $usuario = \App\User::findOrFail($r->empleados_select);
        $empleado = $usuario->empleado;
        $empleado->delete();
        $usuario->delete();
        session()->flash('mensaje', 'El usuario ' . $usuario->name . ' ha sido eliminado del sistema');
        return redirect()->back();
    }

    //MUESTRA LA VISTA PARA TERMINO DE CONTRATO A EMPLEADOS PASANDOLE SOLO LOS EMPLEADOS QUE CONSTAN CON CONTRATOS EN EL SISTEMA
    public function crear_termino_empleado_view() {
        $empleados = \App\User::where('role_id', 7)->get();

        foreach ($empleados as $e) {
            if ($e->contrato()) {
                $out[] = $e;
            }
        }
        if (empty($out)) {
            $empleados = 'NO';
        } else {
            $empleados = $out;
        }
        return view('contratos\crear_termino_empleados', compact('empleados'));
    }

    public function termino_empleado_view() {
        return view('contratos\mostar_terminos');
    }

    public function terminos_json() {
        $terminos = \App\TerminoEmpleado::all();
        return datatables()->collection($terminos)->toJson();
    }

    public function actualizar(Request $request) {
        $contrato = ContratoEmpleado::find($request->id_contrato);

        $contrato->fecha_contrato = $request->fecha_contrato;
        $contrato->nombre = $request->nombre;
        $contrato->identidad = $request->identidad;
        $contrato->fecha_nacimiento_contratador = $request->fecha_nacimiento_contratador;
        $contrato->lugar_trabajo = $request->lugar_trabajo;
        $contrato->nombre_trabajo = $request->nombre_trabajo;
        $contrato->lugar_de_servicios = $request->lugar_de_servicios;
        $contrato->turnos_trabajo = $request->turnos_trabajo;
        $contrato->sueldo_base = $request->sueldo_base;
        $contrato->porciento_gratificacion = $request->porciento_gratificacion;
        $contrato->monto_base = $request->monto_base;
        $contrato->movilizacion = $request->movilizacion;
        $contrato->colocacion = $request->colocacion;
        $contrato->fecha_fin = $request->fecha_fin;
        $contrato->inicio_trabajo = $request->inicio_trabajo;
        $contrato->apf = $request->apf;
        $contrato->afp_salud = $request->afp_salud;
        $contrato->anticipo = $request->anticipo;
        $contrato->proteccion_cargo_trabajador = $request->proteccion_cargo_trabajador;
        $contrato->proteccion_intalacion = $request->proteccion_intalacion;
        $contrato->proteccion_entregado_nombre = $request->proteccion_entregado_nombre;
        $contrato->proteccion_supervisor = $request->proteccion_supervisor;
        $contrato->fecha_entrega = $request->fecha_entrega;
        $contrato->fecha_intruccion = $request->fecha_intruccion;
        $contrato->save();
        return redirect('listar_empleados');
    }

    public function crear(Request $request) {
        $contrato = new \App\ContratoEmpleado();
        $contrato->empleado_id = $request->empleados_select;
        $contrato->fecha_contrato = $request->fecha_contrato;
        $contrato->nombre = $request->nombre;
        $contrato->identidad = $request->identidad;
        $contrato->fecha_nacimiento_contratador = $request->fecha_nacimiento_contratador;
        $contrato->lugar_trabajo = $request->lugar_trabajo;
        $contrato->nombre_trabajo = $request->nombre_trabajo;
        $contrato->lugar_de_servicios = $request->lugar_de_servicios;
        $contrato->turnos_trabajo = $request->turnos_trabajo;
        $contrato->sueldo_base = $request->sueldo_base;
        $contrato->porciento_gratificacion = $request->porciento_gratificacion;
        $contrato->monto_base = $request->monto_base;
        $contrato->movilizacion = $request->movilizacion;
        $contrato->colocacion = $request->colocacion;
        $contrato->fecha_fin = $request->fecha_fin;
        $contrato->inicio_trabajo = $request->inicio_trabajo;
        $contrato->apf = $request->apf;
        $contrato->afp_salud = $request->afp_salud;
        $contrato->anticipo = $request->anticipo;
        $contrato->proteccion_cargo_trabajador = $request->proteccion_cargo_trabajador;
        $contrato->proteccion_intalacion = $request->proteccion_intalacion;
        $contrato->proteccion_entregado_nombre = $request->proteccion_entregado_nombre;
        $contrato->proteccion_supervisor = $request->proteccion_supervisor;
        $contrato->fecha_entrega = $request->fecha_entrega;
        $contrato->fecha_intruccion = $request->fecha_intruccion;
        $contrato->save();
        return redirect('listar_empleados');
    }

    public function eliminar_termino_contrato_empleado($id) {
        $termino = \App\TerminoEmpleado::findOrFail($id);
        $finiquito = \App\FinquitoEmpelados::findOrFail($termino->empleado_id);

        if ($termino->delete() && $finiquito->delete()) {
            return 'Constancia de termino de contrato eliminada';
        } else {
            return 'Ha ocurrido un ERROR al eliminar la constancia de termino';
        }
    }

    public function detalles_termino_empleado($id) {
        $termino = \App\TerminoEmpleado::findOrfail($id);
        $finito = \App\FinquitoEmpelados::findOrfail($termino->empleado_id);
        return view('contratos\ver_termino_empleados', compact('termino','finito'));
    }

}
