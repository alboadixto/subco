<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContratoPuntoController extends Controller {

    public function crear_contrato_view() {
        $puntos = \App\User::where('role_id', 5)->get();
        
        foreach ($puntos as $p) {
            if ($p->contrato_punto() !== 'NO') {
                $out[] = $p;
            }
        }
        
        if (empty($out)) {
            $puntos = 'NO';
        } else {
            $puntos = $out;
        }
         // return $puntos;
        return view('contratos\crear_contratos_pto_servicio', compact('puntos'));
    }

    public function crear_contrato(Request $r) {
        $contrato = new \App\ContratoPunto();
        $contrato->pto_servicio = $r->empleados_select;
        $contrato->sociedad = $r->sociedad;
        $contrato->rut_contratista = $r->rut_contratista;
        $contrato->representante_legal = $r->representante_legal;
        $contrato->cedula_contratista = $r->cedula_contratista;
        $contrato->representante_empresa = $r->representante_empresa;
        $contrato->tipo_producto = $r->tipo_producto;
        $contrato->valor = $r->valor;
        $contrato->sucursales = $r->sucursales;
        $contrato->dia = $r->dia;
        $contrato->mes = $r->mes;
        $contrato->ano = $r->ano;
        $contrato->nombrle_completo_contratista = $r->nombrle_completo_contratista;
        $contrato->no_id_contratista = $r->no_id_contratista;
        $contrato->rut_empresa_contratista = $r->rut_empresa_contratista;
        $contrato->nombre_empresa_contratista = $r->nombre_empresa_contratista;
        $contrato->save();
        return redirect('/listar_puntos');
    }

    public function mostrar_contrato_punto($id) {
        $contrato = \App\ContratoPunto::where('pto_servicio', $id)->first();
        return view('contratos\contrato_pto_servicio', compact('contrato'));
    }

    public function edit_contrato_punto($id,Request $r) {
        $contrato = \App\ContratoPunto::findOrFail($id);
        $contrato->sociedad = $r->sociedad;
        $contrato->rut_contratista = $r->rut_contratista;
        $contrato->representante_legal = $r->representante_legal;
        $contrato->cedula_contratista = $r->cedula_contratista;
        $contrato->representante_empresa = $r->representante_empresa;
        $contrato->tipo_producto = $r->tipo_producto;
        $contrato->valor = $r->valor;
        $contrato->sucursales = $r->sucursales;
        $contrato->dia = $r->dia;
        $contrato->mes = $r->mes;
        $contrato->ano = $r->ano;
        $contrato->nombrle_completo_contratista = $r->nombrle_completo_contratista;
        $contrato->no_id_contratista = $r->no_id_contratista;
        $contrato->rut_empresa_contratista = $r->rut_empresa_contratista;
        $contrato->nombre_empresa_contratista = $r->nombre_empresa_contratista;
        $contrato->save();
        return redirect('/listar_puntos');
    }

}
