<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class EmpleadosController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return datatables()->collection(\App\User::all()->where('role_id', 7))->toJson();
    }

    public function asignar_pto($id_user, $id_pto) {
        $id_e = User::find($id_user);
        $id_e= $id_e->empleado->id;
        $buscar_borrar = \App\EmpleadoPunto::where('empleado_id',$id_e);
        if($buscar_borrar){
            $buscar_borrar->delete();
        }
        $id_p = User::find($id_pto);
        $id_p = $id_p->punto->id;

    
        $emp_pto = new \App\EmpleadoPunto();
        $emp_pto->empleado_id = $id_e;
        $emp_pto->pto_servicio_id = $id_p;
       
        if ( $emp_pto->save()) {
            return 'Empleado asosiado a punto de servicio correctamente';
        } else {
            return 'Ha ocurrido un error';
        }
    }

    /**
     * Show the form for creating a new resource.
     * foreach ($empleados as $e){
      var_dump($e->user()) ;
      }
     * @return \Illuminate\Http\Response
     */
    public function create() {
//
    }

    public function listar_view() {
        $puntos = \App\User::where('role_id', '=', 5)->get();
        return view('empleados\mostar', compact('puntos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
//
    }

//devuelve el empleado de un usuario con su punto de servicio vinculado
// si no tiene punto de servicio crea uno falso para mostrar el mensaje de NO TIENE
    public function usuario_de_empleado($id) {
        $emp = \App\User::findOrFail($id)->empleado;
        if ($emp->tiene_punto()) {
            $emp['punto'] = $emp->punto();
        } else {
            $pto_fake = new User();
            $pto_fake->name = 'No Tiene';
            $emp['punto'] = $pto_fake;
        }
        return $emp;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $empleado = \App\Empleado::findOrFail($id);
        $empleado['punto'] = $empleado->punto();
        return $empleado;
    }

// con el id del empleado devuelve el punto de servcio al que corresponde
    public function punto($id) {

        return $this->show($id)->punto();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
//
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
//
    }

}
