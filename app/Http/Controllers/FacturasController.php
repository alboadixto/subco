<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FacturasController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return datatables()->collection(\Illuminate\Support\Facades\DB::select('SELECT facturas.id as id,facturas.folio as folio,users.name as proveedor,facturas.fecha as fecha,facturas.observaciones as observaciones, facturas.valor_neto as valor_neto,facturas.estado_pago as estado_pago,facturas.tipo_transaccion as tipo_transaccion,facturas.fecha_pago as fecha_pago,facturas.operacion as operacion,facturas.tipo_producto as tipo_producto FROM `facturas` INNER JOIN users on facturas.proveedor = users.id '))->toJson();
    }

    public function clientes_view()
    {
        return view('facturas.mostar_clientes');
    }
    public function clientes()
    {
        $facturas = \Illuminate\Support\Facades\DB::select('SELECT facturas.id as id,facturas.folio as folio,users.name as proveedor,facturas.fecha as fecha,facturas.observaciones as observaciones, facturas.valor_neto as valor_neto,facturas.estado_pago as estado_pago,facturas.tipo_transaccion as tipo_transaccion,facturas.fecha_pago as fecha_pago,facturas.operacion as operacion,facturas.tipo_producto as tipo_producto,users.role_id as id_r FROM `facturas` INNER JOIN users on facturas.proveedor = users.id  WHERE facturas.operacion = "Venta"');
        $out = array();
        foreach ($facturas as $f) {
            if ($f->id_r == 3) {
                $out[] = $f;
            }
        }

        return datatables()->collection($out)->toJson();
    }

    public function ventas()
    {
        return datatables()->collection(\Illuminate\Support\Facades\DB::select('SELECT facturas.id as id,facturas.folio as folio,users.name as proveedor,facturas.fecha as fecha,facturas.observaciones as observaciones, facturas.valor_neto as valor_neto,facturas.estado_pago as estado_pago,facturas.tipo_transaccion as tipo_transaccion,facturas.fecha_pago as fecha_pago,facturas.operacion as operacion,facturas.tipo_producto as tipo_producto FROM `facturas` INNER JOIN users on facturas.proveedor = users.id  WHERE facturas.operacion = "Venta"'))->toJson();
    }

    public function compras()
    {
        return datatables()->collection(\Illuminate\Support\Facades\DB::select('SELECT facturas.id as id,facturas.folio as folio,users.name as proveedor,facturas.fecha as fecha,facturas.observaciones as observaciones, facturas.valor_neto as valor_neto,facturas.estado_pago as estado_pago,facturas.tipo_transaccion as tipo_transaccion,facturas.fecha_pago as fecha_pago,facturas.operacion as operacion,facturas.tipo_producto as tipo_producto FROM `facturas` INNER JOIN users on facturas.proveedor = users.id  WHERE facturas.operacion = "Compra"'))->toJson();
    }

    public function compras_rango($fecha_i, $fecha_f)
    {

        return datatables()->collection(\Illuminate\Support\Facades\DB::select('SELECT DISTINCT DATE_FORMAT( fecha, "%Y-%m-%d" ) as fecha, facturas.id as id,facturas.folio as folio,users.name as proveedor,facturas.observaciones as observaciones, facturas.valor_neto as valor_neto,facturas.estado_pago as estado_pago,facturas.tipo_transaccion as tipo_transaccion,facturas.fecha_pago as fecha_pago,facturas.operacion as operacion,facturas.tipo_producto as tipo_producto FROM `facturas` INNER JOIN users on facturas.proveedor = users.id  WHERE facturas.operacion = "Compra" AND facturas.fecha >= "' . $fecha_i . '" AND facturas.fecha <="' . $fecha_f . '"'))->toJson();
    }

    public function ventas_rango($fecha_i, $fecha_f)
    {

        return datatables()->collection(\Illuminate\Support\Facades\DB::select('SELECT DISTINCT DATE_FORMAT( fecha, "%Y-%m-%d" ) as fecha, facturas.id as id,facturas.folio as folio,users.name as proveedor,facturas.observaciones as observaciones, facturas.valor_neto as valor_neto,facturas.estado_pago as estado_pago,facturas.tipo_transaccion as tipo_transaccion,facturas.fecha_pago as fecha_pago,facturas.operacion as operacion,facturas.tipo_producto as tipo_producto FROM `facturas` INNER JOIN users on facturas.proveedor = users.id  WHERE facturas.operacion = "Venta" AND facturas.fecha >= "' . $fecha_i . '" AND facturas.fecha <="' . $fecha_f . '"'))->toJson();
    }

    public function venta_balance($fecha_i, $fecha_f)
    {
        $datos = \Illuminate\Support\Facades\DB::select('SELECT DISTINCT DATE_FORMAT( fecha, "%Y-%m-%d" ) as fecha, facturas.id as id,facturas.folio as folio,users.name as proveedor,facturas.observaciones as observaciones, facturas.valor_neto as valor_neto,facturas.estado_pago as estado_pago,facturas.tipo_transaccion as tipo_transaccion,facturas.fecha_pago as fecha_pago,facturas.operacion as operacion,facturas.tipo_producto as tipo_producto FROM `facturas` INNER JOIN users on facturas.proveedor = users.id  WHERE facturas.operacion = "Venta" AND facturas.fecha >= "' . $fecha_i . '" AND facturas.fecha <="' . $fecha_f . '"');
        $total = 0;
        $cant = 0;
        $cant_insumos = 0;
        $cant_maquinaria = 0;
        $total_insumo = 0;
        $total_maquinaria = 0;
        foreach ($datos as $d) {
            $cant++;
            $total += $d->valor_neto;
            if ($d->tipo_producto == 'Insumos') {
                $cant_insumos++;
                $total_insumo += $d->valor_neto;
            }
            if ($d->tipo_producto == 'Maquinaria') {
                $cant_maquinaria++;
                $total_maquinaria += $d->valor_neto;
            }
        }

        return ' <table style="width: 100%;   border: 1px solid #000;">
                                                <tr><td style="width: 50%;   border: 1px solid #000;" >Cantidad Total:</td><td style="width: 100%;   border: 1px solid #000;" >' . $cant . '</td></tr>
                                                <tr><td style="width: 50%;   border: 1px solid #000;">Monto total:</td><td style="width: 100%;   border: 1px solid #000;" id="td_ventas">' . $total . '</td></tr>
                                                <tr><td style="width: 50%;   border: 1px solid #000;">Cantidad  Insumos:</td><td style="width: 100%;   border: 1px solid #000;">' . $cant_insumos . '</td></tr>
                                                <tr><td style="width: 50%;   border: 1px solid #000;">Monto Insumos:</td><td style="width: 100%;   border: 1px solid #000;">' . $total_insumo . '</td></tr>
                                                <tr><td style="width: 50%;   border: 1px solid #000;">Cantidad Maquinaria:</td><td style="width: 100%;   border: 1px solid #000;">' . $cant_maquinaria . '</td></tr>
                                                <tr><td style="width: 50%;   border: 1px solid #000;">Monto Maquinaria:</td><td style="width: 100%;   border: 1px solid #000;">' . $total_maquinaria . '</td></tr>
                                            </table>
';
    }
    public function compra_balance($fecha_i, $fecha_f)
    {
        $datos = \Illuminate\Support\Facades\DB::select('SELECT DISTINCT DATE_FORMAT( fecha, "%Y-%m-%d" ) as fecha, facturas.id as id,facturas.folio as folio,users.name as proveedor,facturas.observaciones as observaciones, facturas.valor_neto as valor_neto,facturas.estado_pago as estado_pago,facturas.tipo_transaccion as tipo_transaccion,facturas.fecha_pago as fecha_pago,facturas.operacion as operacion,facturas.tipo_producto as tipo_producto FROM `facturas` INNER JOIN users on facturas.proveedor = users.id  WHERE facturas.operacion = "Compra" AND facturas.fecha >= "' . $fecha_i . '" AND facturas.fecha <="' . $fecha_f . '"');
        $total = 0;
        $cant = 0;
        $cant_insumos = 0;
        $cant_maquinaria = 0;
        $total_insumo = 0;
        $total_maquinaria = 0;
        foreach ($datos as $d) {
            $cant++;
            $total += $d->valor_neto;
            if ($d->tipo_producto == 'Insumos') {
                $cant_insumos++;
                $total_insumo += $d->valor_neto;
            }
            if ($d->tipo_producto == 'Maquinaria') {
                $cant_maquinaria++;
                $total_maquinaria += $d->valor_neto;
            }
        }

        return ' <table style="width: 100%;   border: 1px solid #000;">
                                                <tr><td style="width: 50%;   border: 1px solid #000;" >Cantidad Total:</td><td style="width: 100%;   border: 1px solid #000;">' . $cant . '</td></tr>
                                                <tr><td style="width: 50%;   border: 1px solid #000;">Monto total:</td><td style="width: 100%;   border: 1px solid #000;" id="td_compras">' . $total . '</td></tr>
                                                <tr><td style="width: 50%;   border: 1px solid #000;">Cantidad  Insumos:</td><td style="width: 100%;   border: 1px solid #000;">' . $cant_insumos . '</td></tr>
                                                <tr><td style="width: 50%;   border: 1px solid #000;">Monto Insumos:</td><td style="width: 100%;   border: 1px solid #000;">' . $total_insumo . '</td></tr>
                                                <tr><td style="width: 50%;   border: 1px solid #000;">Cantidad Maquinaria:</td><td style="width: 100%;   border: 1px solid #000;">' . $cant_maquinaria . '</td></tr>
                                                <tr><td style="width: 50%;   border: 1px solid #000;">Monto Maquinaria:</td><td style="width: 100%;   border: 1px solid #000;">' . $total_maquinaria . '</td></tr>
                                            </table>
';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proveedor = \App\User::all()->where('role_id', 6);
        $clientes = \App\User::all()->where('role_id', 3);
        return view('facturas.insertar', compact('proveedor', 'clientes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (\App\Factura::all()->where('folio', $request->folio)->count() == 0) {
            $factura = new \App\Factura();
            $factura->folio = $request->folio;
            $factura->fecha = $request->fecha;
            $factura->proveedor = $request->proveedor;
            $factura->observaciones = $request->observaciones;
            $factura->valor_neto = ((($request->valor_neto) * 19) / 100) + $request->valor_neto;
            $factura->estado_pago = $request->estado;
            $factura->tipo_transaccion = $request->transaccion;
            $factura->operacion = $request->operacion;
            $factura->tipo_producto = $request->tipo_producto;
            $factura->fecha_pago = $request->fecha_pago;
            $factura->save();
            session()->flash('mensaje', 'Factura ingresada al sistema exitosamente');
            return redirect('listar_facturas');
        } else {
            session()->flash('mensaje', 'Ya existe una factura con ese folio en el sistema');
            return redirect('listar_facturas');
        }
    }

    public function listar_view()
    {

        return view('facturas.mostar');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $factura = \App\Factura::findOrFail($id);
        $proveedor = \App\User::all()->where('role_id', '6');
        return view('facturas.editar', compact('factura', 'proveedor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $factura = \App\Factura::findOrFail($id);

        $factura->fecha = $request->fecha;
        $factura->proveedor = $request->proveedor;
        $factura->observaciones = $request->observaciones;
        $factura->valor_neto = $request->valor_neto;
        $factura->estado_pago = $request->estado;
        $factura->tipo_transaccion = $request->transaccion;
        $factura->operacion = $request->operacion;
        $factura->tipo_producto = $request->tipo_producto;
        $factura->fecha_pago = $request->fecha_pago;
        $factura->save();
        session()->flash('insertar_usuario', 'Factura Modificada');
        return redirect('listar_facturas');
    }

    public function update_estado(Request $request, $id)
    {
        $factura = \App\Factura::findOrFail($id);
        $factura->estado_pago = $request->estado;
        $factura->save();
        return 'Estado modificado';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $factura = \App\Factura::findOrFail($id);
        $factura->delete();
        return 'Factura Eliminada';
    }
}
