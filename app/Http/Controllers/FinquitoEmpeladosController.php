<?php

namespace App\Http\Controllers;

use App\FinquitoEmpelados;
use Illuminate\Http\Request;

class FinquitoEmpeladosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FinquitoEmpelados  $finquitoEmpelados
     * @return \Illuminate\Http\Response
     */
    public function show(FinquitoEmpelados $finquitoEmpelados)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FinquitoEmpelados  $finquitoEmpelados
     * @return \Illuminate\Http\Response
     */
    public function edit(FinquitoEmpelados $finquitoEmpelados)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FinquitoEmpelados  $finquitoEmpelados
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FinquitoEmpelados $finquitoEmpelados)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FinquitoEmpelados  $finquitoEmpelados
     * @return \Illuminate\Http\Response
     */
    public function destroy(FinquitoEmpelados $finquitoEmpelados)
    {
        //
    }
}
