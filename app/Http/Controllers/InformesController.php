<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InformesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('informes\calculo');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function calcular_pto_servicio() {
        $user = \App\User::where('role_id', 5)->get();
        $cant = 0;
        $monto_total = 0;
        foreach ($user as $u) {
            if ($pto = \App\Pto_Servicio::where('user_id', $u->id)->first()) {
                $cant++;
                $monto_total += $pto->precio_mensual;
            }
        }
        return ' <table style="width: 100%;   border: 1px solid #000;">
            <tr><td style="width: 50%;   border: 1px solid #000;">Monto Total:</td><td style="width: 100%;   border: 1px solid #000;" id="td_pto">' . $monto_total . '</td></tr>
            <tr><td style="width: 50%;   border: 1px solid #000;">Cantidad de Puntos:</td><td style="width: 100%;   border: 1px solid #000;">' . $cant . '</td></tr>
                                                                                          </table>
';
    }

    public function calcular_empleados() {
        $users = \App\User::where('role_id', 7)->get();
        $cant = 0;
        $total_movilizacion = 0;
        $total_colocación = 0;
        $total_sueldos = 0;
        foreach ($users as $u) {
            if (\App\ContratoEmpleado::where('empleado_id', $u->id)->first()) {
                $contrato = \App\ContratoEmpleado::where('empleado_id', $u->id)->first();
                $cant++;
                $total_colocación += $contrato->colocacion;
                $total_movilizacion += $contrato->movilizacion;
                $total_sueldos += $contrato->sueldo_base;
            } else {
                if ($empleados = \App\Empleado::where('user_id', $u->id)->first()) {
                    $empleados = \App\Empleado::where('user_id', $u->id)->first();
                    $cant++;
                    $total_colocación += $empleados->costo_colocación;
                    $total_movilizacion += $empleados->costo_movilizacion;
                    $total_sueldos += $empleados->sueldo_base;
                }
            }
        }
        $total = $total_colocación + $total_movilizacion + $total_sueldos;
        return ' <table style="width: 100%;   border: 1px solid #000;">
            <tr><td style="width: 50%;   border: 1px solid #000;">Monto Total:</td><td style="width: 100%;   border: 1px solid #000;" id="td_empleados">' . $total . '</td></tr>
            <tr><td style="width: 50%;   border: 1px solid #000;">Monto Sueldos:</td><td style="width: 100%;   border: 1px solid #000;">' . $total_sueldos . '</td></tr>
                                                <tr><td style="width: 50%;   border: 1px solid #000;" >Cantidad Empleados:</td><td style="width: 100%;   border: 1px solid #000;">' . $cant . '</td></tr>
                                                <tr><td style="width: 50%;   border: 1px solid #000;">Monto Colocación:</td><td style="width: 100%;   border: 1px solid #000;">' . $total_colocación . '</td></tr>
                                                <tr><td style="width: 50%;   border: 1px solid #000;">Monto Movilización :</td><td style="width: 100%;   border: 1px solid #000;">' . $total_movilizacion . '</td></tr>
                                                
                                             
                                            </table>
';
    }

}
