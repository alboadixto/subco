<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotificacionsController extends Controller {

    public function crear(Request $r) {
        $perdida = new \App\Perdida();
        $producto_punto = \App\ProductosPuntos::findOrfail($r->relacion_1);
        $perdida->id_producto = $producto_punto->id_productos;
        $perdida->id_punto = $producto_punto->id_punto;
        $punto = \App\User::findOrFail($perdida->id_punto);
        $perdida->nombre_punto = $punto->name;
        $producto = \App\Producto::findOrFail($producto_punto->id_productos);
        $perdida->nombre_producto = $producto->nombre;
        $perdida->cant_producto = $producto_punto->cantidad;
        $perdida->informa = auth()->user()->name;
        $perdida->precio=$producto->ultimo_precioxunidad;
        $perdida->fecha= now();
        $perdida->estado='No Vista';
        if (!empty($r->descrip)) {
            $perdida->descrip = $r->descrip;
            $perdida->save();
            $user_admin = \App\User::where('role_id','1')->get();
            
            foreach ($user_admin as $user_a){
                $user_a->notify(new \App\Notifications\PerdidasNotificacion($producto_punto, $r->descrip));
            }
           
            
        } else {
            
            $perdida->descrip = ' Solicitud de Baja';
            $perdida->save();
             $user_admin = \App\User::where('role_id','1')->get();
            foreach ($user_admin as $user_a){
                $user_a->notify(new \App\Notifications\PerdidasNotificacion($producto_punto, $perdida->descrip));
            }
        }
        return 'Se le ha notificado al administrador';
    }

}
