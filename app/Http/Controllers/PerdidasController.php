<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PerdidasController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        
    }

    public function calcular_total_perdidas($inicio, $fin) {
        $perdidas = \App\Perdida::where('fecha', '>=', $inicio)->where('fecha', '<=', $fin)->get();
        $cant = 0;
        $cant_productos = 0;
        $total_importe = 0;
        foreach ($perdidas as $p) {
            $cant++;
            $cant_productos += $p->cant_producto;
            $total_importe += $p->cant_producto * $p->precio;
        }
        return ' <table style="width: 100%;   border: 1px solid #000;">
                                                <tr><td style="width: 50%;   border: 1px solid #000;" >Cantidad Total:</td><td style="width: 100%;   border: 1px solid #000;">' . $cant_productos . '</td></tr>
                                                <tr><td style="width: 50%;   border: 1px solid #000;">Monto total:</td><td style="width: 100%;   border: 1px solid #000;" id="td_perdidas">' . $total_importe . '</td></tr>
                                                                                           
                                            </table>
';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function confirmar($id) {
        $perdida = \App\Perdida::findOrFail($id);
        $producto_punto = \App\ProductosPuntos::where('id_productos', $perdida->id_producto)->where('id_punto', $perdida->id_punto)->where('cantidad', $perdida->cant_producto);
        $producto_punto->delete();
        $perdida->estado = 'Confirmada';
        $perdida->save();
        return 'Perdida Confirmada';
    }

    public function eliminar($id) {
        $perdida = \App\Perdida::findOrFail($id);
        $perdida->delete();
        return 'Perdida Eliminada';
    }

    public function informar_view() {
        $ptos = \App\User::where('role_id', 5)->get();
        return view('info_perdida\mostrar', compact('ptos'));
    }

    public function listar_view() {
        $notifica = auth()->user()->unreadNotifications;
        foreach ($notifica as $n) {
            if ($n->data['texto'] === 'Solicitud de Baja tecnica') {
                $n->markAsRead();
            }
        }
//auth()->user()->unreadNotifications->markAsRead();
        return view('info_perdida\mostrar_perdidas');
    }

    public function listar_view_all() {
        $perdidas = \App\Perdida::all();
        return view('info_perdida\mostrar_perdidas_all', compact('perdidas'));
    }

    public function perdidas_json() {
        $perdidas = \App\Perdida::where('estado', 'No Vista')->get();
        return datatables()->collection($perdidas)->toJson();
    }

    public function perdidas_json_all() {
        $perdidas = \App\Perdida::all();
        return datatables()->collection($perdidas)->toJson();
    }

}
