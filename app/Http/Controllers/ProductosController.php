<?php

namespace App\Http\Controllers;

use App\Producto;
use Illuminate\Http\Request;

class ProductosController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $productos = Producto::all();
        $salida = array();
        foreach ($productos as $p) {
            if (\App\Proveedor::where('user_id', $p->proveedor)->count() == 0) {
                $p->proveedor = 'No se encuentra en el sistema';
                $salida[] = $p;
            } else {
                $u = (\App\User::where('id', $p->proveedor)->get()->first());
                $p->proveedor =$u->name;
                $salida[] =$p;
            }
        }
        return datatables()->collection($salida)->toJson();

        //  return datatables()->collection(\Illuminate\Support\Facades\DB::select('SELECT productos.id as id,productos.codigo as codigo,users.name as proveedor,productos.nombre as nombre,productos.tipo as tipo,productos.stock as stock,productos.tipo as tipo,productos.marca as marca,productos.unidad_medida as unidad_medida,productos.ultimo_precioxunidad as ultimo_precioxunidad FROM productos INNER JOIN users on productos.proveedor =users.id '))->toJson();
    }

    public function productos_pto($id) {

        $productos_pto = new \App\ProductosPuntos();
        $productos_pto = $productos_pto->where('id_punto', $id)->get();
        $salida_ = array();
        $salida = array();
        foreach ($productos_pto as $p) {
            $salida_[] = Producto::findOrFail($p->id_productos);
            $salida_[count($salida_) - 1]['stock'] = $p->cantidad;
            $salida_[count($salida_) - 1]['id'] = $p->id;
        }

        foreach ($salida_ as $p) {
            if (\App\Proveedor::where('id', $p->proveedor)->count() == 0) {
                $p->proveedor = 'No se encuentra en el sistema';
                $salida[] = $p;
            } else {
                $salida[] = $p;
            }
        }
        return datatables()->collection($salida)->toJson();
    }

    public function listar_view() {
        $puntos = \App\User::all();
        $productos = Producto::all();
        $cant = count($productos);
        $cant_insumo = 0;
        $cant_maquinaria = 0;
        $precio_total = 0;
        $precio_insumo = 0;
        $precio_maquinaria = 0;
        foreach ($productos as $p) {
            if ($p->tipo == 'Insumo') {
                $cant_insumo++;
                $precio_insumo += $p->ultimo_precioxunidad;
                $precio_total += $p->ultimo_precioxunidad;
            } else {
                $cant_maquinaria++;
                $precio_maquinaria += $p->ultimo_precioxunidad;
                $precio_total += $p->ultimo_precioxunidad;
            }
        }
        return view('productos\mostar', compact('puntos', 'cant', 'cant_insumo', 'cant_maquinaria', 'precio_total', 'precio_insumo', 'precio_maquinaria'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $proveedor = \App\User::all()->where('role_id', '6');
        return view('productos\insertar', compact('proveedor'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $producto = new Producto();
        $producto->codigo = $request->codigo;
        $producto->proveedor = $request->proveedor;
        $producto->nombre = $request->nombre;
        $producto->stock = $request->stock;
        $producto->tipo = $request->tipo;
        $producto->marca = $request->marca;
        $producto->unidad_medida = $request->unidad_medida;
        $producto->ultimo_precioxunidad = $request->ultimo_precioxunidad;
        $producto->estado = $request->estado;
        $producto->save();
        session()->flash('insertar_productos', 'Producto insertado correctamente');
        return redirect('listar_productos');
    }

    public function enviar(Request $request, $id) {
        $producto = Producto::findOrFail($id);
        $producto->stock = $producto->stock - $request->cant;
        $producto->save();
        $productos_punto = new \App\ProductosPuntos();
        $productos_punto->id_punto = $request->punto;
        $productos_punto->id_productos = $id;
        $productos_punto->cantidad = $request->cant;
        $productos_punto->save();

        return 'Asignación de producto realizada';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $producto = Producto::where('id', $id)->get();
        echo $producto;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $producto = Producto::findOrFail($id);
        $producto->codigo = $request->codigo;
        $producto->nombre = $request->nombre;
        $producto->stock = $request->stock;
        $producto->tipo = $request->tipo;
        $producto->marca = $request->marca;
        $producto->estado = $request->estado;
        $producto->unidad_medida = $request->unidad_medida;
        $producto->ultimo_precioxunidad = $request->ultimo_precioxunidad;
        // $producto->update($request->all());
        $producto->save();
        return 'Producto modificado correctamente';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

        Producto::findOrFail($id)->delete();

        return 'Producto eliminado';
    }

}
