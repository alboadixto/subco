<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class PuntoController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return datatables()->collection(\Illuminate\Support\Facades\DB::select('SELECT pto__servicios.precio_mensual as precio,pto__servicios.cant_persona as cant_personas,pto__servicios.id as id_pto,users.id as id, users.name as nombre, users.cargo as cargo, users.direccion as direccion,users.telefono as telefono,users.email as correo,pto__servicios.folio as folio,pto__servicios.nombre_encargado as encargado FROM users INNER JOIN pto__servicios on users.id = pto__servicios.user_id'))->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    public function listar_view() {

        return view('pto_servcio\mostar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $punto = \App\Pto_Servicio::findOrFail($id);
        return $punto;
    }

    public function inventario($id) {
        return datatables()->collection(\Illuminate\Support\Facades\DB::select('SELECT productos_puntos.id as id,productos.estado as estado,productos_puntos.cantidad as cantidad,productos.codigo as codigo,productos.nombre as nombre,productos.tipo as tipo,productos.marca as marca,productos.ultimo_precioxunidad as precio FROM `productos_puntos` INNER JOIN productos ON productos_puntos.id_productos = productos.id WHERE productos_puntos.id_punto =' . $id))->toJson();
    }

    public function listar_inventario($id) {

        $punto = User::find($id);
        $productos = \App\ProductosPuntos::where('id_punto', '=', '49')->get();

        $cantidad = 0;
        $cant_insumo = 0;
        $cant_maquinaria = 0;
        $total_precio_todos = 0;
        $total_precio_insumo = 0;
        $total_precio_maquinaria = 0;
        foreach ($productos as $p) {
            $pro = \App\Producto::findOrFail($p->id_productos)->first();
            $cantidad++;
            $total_precio_todos += $pro->ultimo_precioxunidad;

            if ($pro->tipo == 'Insumo') {
                $total_precio_insumo += $pro->ultimo_precioxunidad;
                $cant_insumo++;
            }
            if ($pro->tipo == 'Maquinaria') {
                $total_precio_maquinaria += $pro->ultimo_precioxunidad;
                $cant_maquinaria++;
            }
        }

        return view('pto_servcio\mostrar_inventario', compact('punto', 'cantidad', 'cant_insumo', 'cant_maquinaria', 'total_precio_todos', 'total_precio_insumo', 'total_precio_maquinaria'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $usuario = \App\User::findOrFail($id);
        $punto = \App\Pto_Servicio::where('user_id', '=', $id)->first();

        return view('pto_servcio\editar', compact('usuario', 'punto'));
    }
    public function elimiar_prod_inventario($id) {
        $prod = \App\ProductosPuntos::findOrFail($id);
        $prod->delete();
        return 'Producto Eliminado del inventario';
        
    }
    
    public function mod_cant(Request $r,$id) {
        $prod = \App\ProductosPuntos::findOrFail($id);
        $prod->cantidad=$r->cant;
        $prod->save();
        return 'Producto Eliminado del inventario';
        
    }
   
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $usuario = User::findOrFail($id);
        $usuario->name = $request->nombre;
        $usuario->cargo = $request->cargo;
        $usuario->direccion = $request->direccion;
        $usuario->telefono = $request->telefono;
        $usuario->email = $request->correo;
        $usuario->save();
        $pto_servicio = \App\Pto_Servicio::where('user_id', '=', $id)->first();
        $pto_servicio->user_id = $usuario->id;
        $pto_servicio->folio = $request->folio;
        $pto_servicio->nombre_encargado = $request->encargado;
        $pto_servicio->cant_persona = $request->cant_persona;
        $pto_servicio->fecha_desde_antes = $request->fecha_antes;
        $pto_servicio->supervisor_id = $request->supervisor;
        $pto_servicio->descripcion = $request->descripcion;
        $pto_servicio->precio_mensual = $request->precio;
        $pto_servicio->save();
        session()->flash('insertar_usuario', 'Punto de Servvicio modificado');
        return redirect('listar_puntos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $usuarios = \App\User::findOrFail($id);
        $usuarios->delete();
        $punto = \App\Pto_Servicio::where('user_id', '=', $id);
        $punto->delete();

        return 'Punto de Servicio Eliminado';
    }

}
