<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UniformeController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if ($tallas = \App\TallasUniforme::where('user_id', auth()->user()->id)->first()) {

            return view('uniforme\ver', compact('tallas'));
        } else {
            return view('uniforme\ver');
        }
    }

    public function empleados_talla_json() {
//dejar solo los usuarios que son empleados-------------------------------------------
        //$empleados = \App\User::where('role_id', 1)->get();
        $empleados = \App\User::all();
        $out = array();
        $count = 0;
        foreach ($empleados as $e) {
            $talla = \App\TallasUniforme::where('user_id', $e->id)->first();
            if (!empty($talla)) {
                $out[$count]['id_empleado'] = $e->id;
                $out[$count]['nombre'] = $e->name;
                $out[$count]['pantalon'] = $talla->pantalon;
                $out[$count]['polera'] = $talla->polera;
                $out[$count]['delantal'] = $talla->delantal;
                $out[$count]['zapato'] = $talla->zapato;
                $out[$count]['polar'] = $talla->polar;
                $count++;
            } else {
                $out[$count]['id_empleado'] = $e->id;
                $out[$count]['nombre'] = $e->name;
                $out[$count]['pantalon'] = 'Aun no lo especifica';
                $out[$count]['polera'] = 'Aun no lo especifica';
                $out[$count]['delantal'] = 'Aun no lo especifica';
                $out[$count]['zapato'] = 'Aun no lo especifica';
                $out[$count]['polar'] = 'Aun no lo especifica';
            }
        }
        return datatables()->collection($out)->toJson();
    }

    public function listar_empleados() {
        $notifica = auth()->user()->unreadNotifications;
        foreach ($notifica as $n) {
            if ($n->data['texto'] === 'Cambio de tallas en empleado') {
                $n->markAsRead();
            }
        }
        return view('uniforme\mostrar');
    }

    public function guardar_uniforme_empleado(Request $r) {
        $uniforme = new \App\UniformeEmpleado();
        $uniforme->fill($r->all());
        $uniforme->all();
        $uniforme->save();
        session()->flash('mensaje', 'Asignación de uniforme realizada');
        return view('uniforme\mostrar');
    }

    public function eliminar_uniforme_empleado($id) {
        $uniforme = \App\UniformeEmpleado::findOrfail($id);
        $uniforme->delete();
        session()->flash('mensaje', 'Uniforme eliminado');
        return redirect('uniforme/ver/lista_empleados');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('uniforme/insertar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        if ($tiene = \App\TallasUniforme::where('user_id', auth()->user()->id)->first()) {
            $this->edit(auth()->user()->id, $request);
            $user_admin = \App\User::where('role_id', 1)->get();
            foreach ($user_admin as $user_a) {
                $user_a->notify(new \App\Notifications\TallasNotification(auth()->user()->name, 'El usuario actualizo su tallas'));
            }
            session()->flash('mensaje', 'Ya tenia uniforme solo fue modificado por las tallas actuales');
            return redirect('/uniforme');
        } else {
            $datos = $request->all();
            $datos['user_id'] = auth()->user()->id;
            $tallas = new \App\TallasUniforme();
            $tallas->fill($datos);
            $tallas->estado = 0;
            $tallas->save();
            $user_admin = \App\User::where('role_id', 1)->get();
            foreach ($user_admin as $user_a) {
                $user_a->notify(new \App\Notifications\TallasNotification(auth()->user()->name, 'El usuario actualizo su tallas'));
            }
            session()->flash('mensaje', 'Sus tallas han sido ingresadas al sistema correctamente');
            return redirect('/uniforme');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function show($id) {
        $user = \App\User::findOrFail($id);
        $tallas = \App\TallasUniforme::where('user_id', $id)->first();
        $uniforme = \App\UniformeEmpleado::where('empleado_id', $id)->get();
        return view('uniforme\detalles_empleado', compact('user', 'tallas', 'uniforme'));
    }

    public function asignar_view($id) {
        $user = \App\User::findOrFail($id);
        return view('uniforme\asignar', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request) {
        $datos = $request->all();
        $datos['user_id'] = auth()->user()->id;

        $tallas = \App\TallasUniforme::where('user_id', auth()->user()->id)->first();
        $tallas->fill($datos);
        $tallas->estado = 0;
        $tallas->save();
        return redirect('/uniformes');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
    
    
 public function uniforme_gastos_calculo($inicio,$fin) {
        $uniformes = \App\UniformeEmpleado::where('fecha','>=',$inicio)->where('fecha','<=',$fin)->get();
        $cant =0;
        $cant_pantalon=0;
        $cant_poleras=0;
        $cant_delantar=0;
        $cant_zapatos=0;
        $cant_polar=0;
        $monto_pantalon=0;
        $monto_poleras=0;
        $monto_delantar=0;
        $monto_zapatos=0;
        $monto_polar=0;
        $total_monto =0;        
             foreach ($uniformes as $u) {
                  $cant++;
        $cant_pantalon+=$u->pantalon_cant;
        $cant_poleras+=$u->polera_cant;
        $cant_delantar+=$u->delantar_cant;
        $cant_zapatos+=$u->zapato_cant;
        $cant_polar+=$u->polar_cant;
        $monto_pantalon+=$cant_pantalon * $u->pantalon_precio;
        $monto_poleras+=$cant_poleras *$u->polera_precio;
        $monto_delantar+=$cant_delantar * $u->delantal_precio;
        $monto_zapatos+=$cant_zapatos * $u->zapato_precio;
        $monto_polar+=$cant_polar * $u->polar_precio;
        $total_monto+=$monto_delantar+$monto_pantalon+$monto_polar+$monto_poleras+$monto_zapatos;        
             }
                return ' <table style="width: 100%;   border: 1px solid #000;">
                                                <tr><td style="width: 50%;   border: 1px solid #000;" >Cantidad Total:</td><td style="width: 100%;   border: 1px solid #000;" >' . $cant . '</td></tr>
                                                <tr><td style="width: 50%;   border: 1px solid #000;">Monto total:</td><td style="width: 100%;   border: 1px solid #000;" id="td_uniforme">' . $total_monto . '</td></tr>
                                                    <tr><td style="width: 50%;   border: 1px solid #000;">Cantidad Pantalones:</td><td style="width: 100%;   border: 1px solid #000;">' . $cant_pantalon . '</td></tr>
                                                        <tr><td style="width: 50%;   border: 1px solid #000;"> Monto pantalones:</td><td style="width: 100%;   border: 1px solid #000;">' . $monto_pantalon . '</td></tr>
                                                            <tr><td style="width: 50%;   border: 1px solid #000;">Cantidad de Poleras :</td><td style="width: 100%;   border: 1px solid #000;">' . $cant_poleras . '</td></tr>
                                                                <tr><td style="width: 50%;   border: 1px solid #000;">Monto Poleras:</td><td style="width: 100%;   border: 1px solid #000;">' . $monto_poleras . '</td></tr>
                                                                     <tr><td style="width: 50%;   border: 1px solid #000;">Cantidad de Delantar :</td><td style="width: 100%;   border: 1px solid #000;">' . $cant_delantar . '</td></tr>
                                                                <tr><td style="width: 50%;   border: 1px solid #000;">Monto Delantar:</td><td style="width: 100%;   border: 1px solid #000;">' . $monto_delantar . '</td></tr>
                                                                     <tr><td style="width: 50%;   border: 1px solid #000;">Cantidad de Zapatos :</td><td style="width: 100%;   border: 1px solid #000;">' . $cant_zapatos . '</td></tr>
                                                                <tr><td style="width: 50%;   border: 1px solid #000;">Monto Zapatos:</td><td style="width: 100%;   border: 1px solid #000;">' . $monto_zapatos. '</td></tr>
                                                                    <tr><td style="width: 50%;   border: 1px solid #000;">Cantidad de Polares :</td><td style="width: 100%;   border: 1px solid #000;">' . $cant_polar . '</td></tr>
                                                                <tr><td style="width: 50%;   border: 1px solid #000;">Monto polares:</td><td style="width: 100%;   border: 1px solid #000;">' . $monto_polar. '</td></tr>
                                            </table>
';
        
    }
    public function uniforme_gastos() {
        $uniformes = \App\UniformeEmpleado::all();
        $out = array();
        foreach ($uniformes as $u) {
            $empleado = \App\User::findOrFail($u->empleado_id);
            $u->empleado_id = $empleado->name;
            $out[] = $u;
        }

        return view('uniforme\mostrar_gastos', compact('out'));
    }

}
