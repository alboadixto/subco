<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsuariosController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return datatables()->collection(\Illuminate\Support\Facades\DB::select("SELECT users.id as id,users.name as nombre,users.cargo as cargo,users.direccion as direccion,users.telefono as telefono,users.email as email,roles.nombre as rol FROM users INNER JOIN roles ON roles.id = users.role_id"))->toJson();
    }

    public function listar_view()
    {

        return view('usuarios.mostar');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('usuarios.insertar');
    }

    public function mi_usuario()
    {
        return view('usuarios.mi_usuario');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (User::where('email', $request->email)->first()) {
            session()->flash('insertar_usuario', 'Ya existe ese correo en el sistema, Por favor introduzca otro');
            return redirect()->back();
        }
        if (User::where('telefono', $request->telefono)->first()) {
            session()->flash('insertar_usuario', 'Ya existe ese número de teléfono en el sistema, Por favor introduzca otro');
            return redirect()->back();
        }
        $usuario_new = new User();
        $usuario_new->name = $request->name;
        $usuario_new->role_id = $request->role_id;
        $usuario_new->cargo = $request->cargo;
        $usuario_new->direccion = $request->direccion;
        $usuario_new->telefono = $request->telefono;
        $usuario_new->email = $request->email;
        $usuario_new->rut = $request->rut;
        $usuario_new->password = Hash::make($request->password);
        $usuario_new->save();
        //si el tipo de rol es empleado
        if ($request->role_id == 7) {
            $empleado = new \App\Empleado();
            $empleado->user_id = $usuario_new->id;
            $empleado->rut = $request->rut;
            $empleado->sueldo_base = $request->empleado_sueldo_base;
            $empleado->fecha_contrato = $request->empleado_fecha_contrato;
            $empleado->tipo_contrato = $request->empleado_tipo_contrato;
            $empleado->costo_movilizacion = $request->empleado_costo_movilizacion;
            $empleado->costo_colocación = $request->costo_colocación;
            $empleado->save();
            session()->flash('insertar_usuario', 'Usuario insertado correctamente');
            return redirect('listar_empleados');
        }
        //if es cliente el tipo de usuario
        if ($request->role_id == 3) {
            $cliente = new \App\Cliente();
            $cliente->user_id = $usuario_new->id;
            $cliente->actividad = $request->actividad;
            $cliente->rubro = $request->rubro;
            $cliente->save();
            session()->flash('insertar_usuario', 'Usuario insertado correctamente');
            return redirect('listar_usuarios');
        }
        //si es punto de servicio el usuario
        if ($request->role_id == 5) {
            $pto_servicio = new \App\Pto_Servicio();
            $pto_servicio->user_id = $usuario_new->id;
            $pto_servicio->folio = $request->folio;
            $pto_servicio->nombre_encargado = $request->encargado;
            $pto_servicio->cant_persona = $request->cant_personas;
            $pto_servicio->fecha_desde_antes = $request->fecha_antes;
            $pto_servicio->supervisor_id = $request->supervisor;
            $pto_servicio->descripcion = $request->descripcion;
            $pto_servicio->precio_mensual = $request->precio;
            $pto_servicio->save();
            session()->flash('insertar_usuario', 'Usuario insertado correctamente');
            return redirect('listar_puntos');
        }
        //si el usuario es proveedor
        if ($request->role_id == 6) {
            $proveedor = new \App\Proveedor();
            $proveedor->contacto = $request->contacto;
            $proveedor->rut = $request->rut;
            $proveedor->user_id = $usuario_new->id;
            $proveedor->save();
            session()->flash('insertar_usuario', 'Usuario insertado correctamente');
            return redirect('listar_usuarios');
        }
        session()->flash('insertar_usuario', 'Usuario insertado correctamente');
        return redirect('listar_usuarios');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (isset($request->rol)) {
            //si el cambio de rol es para cliente
            if ($request->rol == 3) {
                $user = User::find($request->id);
                $cliente = new \App\Cliente();
                $cliente->actividad = $request->actividad;
                $cliente->rubro = $request->rubro;
                $cliente->user_id = $user->id;
                $cliente->save();
                $user->role_id = $request->rol;
                $user->save();
                return 'Cambio de rol realizado';
            }
            //si el cambio de rol es pára proveedor
            if ($request->rol == 6) {
                $user = User::find($request->id);
                $proveedor = new \App\Proveedor();
                $proveedor->user_id = $user->id;
                $proveedor->rut = $request->rut;
                $proveedor->contacto = $request->contacto;
                $proveedor->save();
                $user->role_id = $request->rol;
                $user->save();
                return 'Cambio de rol realizado';
            }
            if ($request->rol == 7) {
                $user = User::find($request->id);
                $user->role_id = $request->rol;

                $empleado = new \App\Empleado();
                $empleado->user_id = $user->id;
                $empleado->rut = $request->rut;
                $empleado->sueldo_base = $request->sueldo;
                $empleado->fecha_contrato = $request->fecha_contrato;
                $empleado->tipo_contrato = $request->tipo_contrato;
                $empleado->costo_movilizacion = $request->movilizacion;
                $empleado->costo_colocación = $request->colocacion;
                $empleado->save();
                $user->save();
                if ($empleado && $user) {
                    return 'Cambio de rol realizado';
                } else {
                    return 'Revise los campos ha ocurrido un error';
                }
            }
            if ($request->rol !== 6 && $request->rol !== 3) {
                $user = User::find($request->id);
                if ($user->role_id == 6) {
                    \Illuminate\Support\Facades\DB::delete("DELETE FROM proveedors WHERE user_id=" . $user->id);
                }
                $user->role_id = $request->rol;
                $user->save();
                return 'Cambio de rol realizado';
            }
        } else {
            $usuario = User::findOrFail($id);
            $usuario->name = $request->nombre;
            $usuario->cargo = $request->cargo;
            $usuario->direccion = $request->direccion;
            $usuario->telefono = '+' . $request->telefono;
            $usuario->email = $request->correo;
            $usuario->save();

            return 'Usuario modificado';
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuarios = User::findOrFail($id);

        $usuarios->delete();
        return 'Usuario Eliminado';
    }

    public function cambiar_pass(Request $request, $id)
    {
        $usuarios = User::findOrFail($id);
        $usuarios->password = Hash::make($request->pass);
        $usuarios->save();
        return 'Contraseña Modificada';
    }
}
