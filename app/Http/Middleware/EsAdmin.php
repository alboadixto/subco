<?php
namespace App\Http\Middleware;
use Closure;
class EsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
        if(\Illuminate\Support\Facades\Auth::check()){
    $usuario = \Illuminate\Support\Facades\Auth::user();
            if($usuario->esAdmin()){
                 return $next($request);
                 //return redirect('/admin');
                 
            }
        }
       return  redirect('/login');
    }
}
