<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PerdidasNotificacion extends Notification {

    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(\App\ProductosPuntos $punto_producto,String $descrip) {
        $this->punto_producto = $punto_producto;
        $this->descrip=$descrip;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable) {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable) {
        return (new MailMessage)
                        ->line('The introduction to the notification.')
                        ->action('Notification Action', url('/'))
                        ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable) {
        return [
             'producto_punto' => $this->punto_producto->id,
            'producto_id' => $this->punto_producto->id_productos,
            'punto_id' => $this->punto_producto->id_punto,
            'texto'=>'Solicitud de Baja tecnica',
              'descrip'=>$this->descrip,
            'time' => Carbon::now()->diffForHumans(),
        ];
    }

}
