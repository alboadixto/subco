<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class TallasUniforme extends Model
{
  use Notifiable;
     protected $fillable = [
        'user_id', 'pantalon', 'polera', 'delantal','zapato','polar'
    ];
}
