<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class UniformeEmpleado extends Model
{
        use Notifiable;
        
        protected $fillable = [
        'empleado_id', 'fecha', 'pantalon_cant', 'pantalon_precio', 'polera_cant', 'polera_precio', 'delantar_cant', 'delantal_precio', 'zapato_cant', 'zapato_precio', 'polar_cant', 'polar_precio', 'total'
    ];
}
