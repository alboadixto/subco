<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Role;

class User extends Authenticatable {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role() {

        return $this->belongsTo('App\Role');
    }

    public function empleado() {
        if ($this->role->nombre === 'Empleado') {
            return $this->hasOne('App\Empleado');
        } else {
            return 'No es un Empleado';
        }
    }
    public function punto(){
        if($this->role->nombre ==='Punto de Servicio'){
            return $this->hasOne('App\Pto_Servicio');
        }
        else{
                        return 'no es un Punto';}
    }

    
    public function contrato_punto() {
        if(ContratoPunto::where('pto_servicio', $this->id)->get()){
            return ContratoPunto::where('pto_servicio', $this->id)->get()->first();
        }
        else{
            return 'NO';
        }
    }




    public function esAdmin() {

        if ($this->role->nombre === 'Administrador') {
            return true;
        }
        return false;
    }
      public function contrato() {
      $contrato = ContratoEmpleado::where('empleado_id', $this->id)->first();
      return $contrato;
    }

}
