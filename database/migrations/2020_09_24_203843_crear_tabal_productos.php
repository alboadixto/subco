<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTabalProductos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Productos', function (Blueprint $table) {
       $table->increments('id');
       $table->string('codigo');
       $table->integer('proveedor');
       $table->string('nombre');
       $table->string('stock');
       $table->string('tipo');
       $table->string('marca');
       $table->string('unidad_medida');
       $table->string('ultimo_precioxunidad');
       $table->string('estado')->nullable('true');
       
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Productos');
    }
}
