<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('folio');
            $table->date('fecha');
            $table->integer('proveedor');
            $table->text('observaciones');
            $table->float('valor_neto');
            $table->string('estado_pago');
            $table->string('tipo_transaccion');
            $table->date('fecha_pago');
            $table->string('operacion');
             $table->string('tipo_producto');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturas');
    }
}
