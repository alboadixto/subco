<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePtoServiciosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('pto__servicios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('folio');

            $table->string('nombre_encargado');
            $table->integer('cant_persona');
            $table->date('fecha_desde_antes');
            $table->integer('supervisor_id');
            $table->string('descripcion');
            $table->float('precio_mensual');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('pto__servicios');
    }

}
