<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompraVentasTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('compra_ventas', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha');
            $table->integer('num_factura');
            $table->integer('id_producto');
            $table->string('rut');
            $table->string('descripcion');
            $table->float('neto');
            $table->float('iva');
            $table->float('t_compra');
            $table->float('t_venta');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('compra_ventas');
    }

}
