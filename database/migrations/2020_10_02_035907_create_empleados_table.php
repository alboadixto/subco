<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpleadosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('empleados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('rut');
            $table->string('sueldo_base');
            $table->date('fecha_contrato');
            $table->string('tipo_contrato');
            $table->integer('contrato_empleado_id');
            $table->float('costo_movilizacion');
            $table->float('costo_colocación');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('empleados');
    }

}
