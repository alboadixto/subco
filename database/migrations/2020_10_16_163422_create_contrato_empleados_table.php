<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContratoEmpleadosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('contrato_empleados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empleado_id');
            $table->date('fecha_contrato');
            $table->string('nombre');
            $table->string('identidad');
            $table->date('fecha_nacimiento_contratador');
            $table->string('lugar_trabajo');
            $table->string('nombre_trabajo');
            $table->string('lugar_de_servicios');
            $table->string('turnos_trabajo');
            $table->float('sueldo_base');
            $table->float('porciento_gratificacion');
            $table->float('monto_base');
            $table->float('movilizacion');
            $table->float('colocacion');
            $table->date('fecha_fin');
            $table->date('inicio_trabajo');
            $table->string('apf');
            $table->string('afp_salud');
            $table->boolean('anticipo');
            $table->string('proteccion_cargo_trabajador');
            $table->string('proteccion_intalacion');
            $table->string('proteccion_entregado_nombre');
            $table->string('proteccion_supervisor');
            $table->date('fecha_intruccion');
            $table->date('fecha_entrega');
            
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('contrato_empleados');
    }

}
