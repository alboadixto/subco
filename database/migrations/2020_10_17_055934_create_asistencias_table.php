<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAsistenciasTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('asistencias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->time('entrada');
            $table->time('salida');
            $table->date('dia');
            $table->string('confirmado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('asistencias');
    }

}
