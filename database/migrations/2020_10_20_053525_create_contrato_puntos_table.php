<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContratoPuntosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('contrato_puntos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pto_servicio');
            $table->string('sociedad');
            $table->string('rut_contratista');
            $table->string('representante_legal');
            $table->string('cedula_contratista');
            $table->string('representante_empresa');
            $table->string('tipo_producto');
            $table->float('valor');
            $table->string('sucursales');
            $table->string('dia');
            $table->string('mes');
            $table->string('ano');
            $table->string('nombrle_completo_contratista');
            $table->string('no_id_contratista');
            $table->string('rut_empresa_contratista');
            $table->string('nombre_empresa_contratista');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('contrato_puntos');
    }

}
