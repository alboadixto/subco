<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTerminoEmpleadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('termino_empleados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empleado_id');
            $table->integer('fecha_dia');
            $table->string('fecha_mes');
            $table->integer('fecha_ano');
            $table->string('nombre_empleado');
            $table->string('rut_empleado');
            $table->string('fecha_vinculo_contrato');
            $table->string('ausencias');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('termino_empleados');
    }
}
