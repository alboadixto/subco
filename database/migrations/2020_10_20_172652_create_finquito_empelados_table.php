<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinquitoEmpeladosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('finquito_empelados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fecha_dia');
            $table->string('fecha_mes');
            $table->integer('fecha_ano');
            $table->string('nombre_trabajador');
            $table->string('rut_trabajador');
            $table->string('direccion_trabajador');
            $table->date('fecha_desde');
            $table->date('fecha_hasta');
            $table->float('total_mes');
            $table->float('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('finquito_empelados');
    }

}
