<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerdidasTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('perdidas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_producto');
            $table->integer('id_punto');
            $table->string('nombre_punto');
            $table->string('nombre_producto');
            $table->integer('cant_producto');
            $table->string('informa');
            $table->string('descrip');
            $table->string('estado')->nullable(true);
            $table->integer('precio')->nullable(true);
            $table->date('fecha')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('perdidas');
    }

}
