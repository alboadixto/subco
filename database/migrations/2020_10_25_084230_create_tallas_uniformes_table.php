<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTallasUniformesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tallas_uniformes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('pantalon');
            $table->string('polera');
            $table->string('delantal');
            $table->string('zapato');
            $table->string('polar');
            $table->integer('estado')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tallas_uniformes');
    }
}
