<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUniformeEmpleadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uniforme_empleados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empleado_id');
            $table->date('fecha');
            $table->integer('pantalon_cant');
            $table->float('pantalon_precio');
            $table->integer('polera_cant');
            $table->float('polera_precio');
            $table->integer('delantar_cant');
            $table->float('delantal_precio');
            $table->integer('zapato_cant');
            $table->float('zapato_precio');
            $table->integer('polar_cant');
            $table->float('polar_precio');
            $table->double('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uniforme_empleados');
    }
}
