@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <div id="mini_balance">
                        <h2>Asistencia:{{$user->name}}</h2>
                        <h5>Total de Asistencias:{{$total_asis}}</h5>
                        <h5>Confirmadas:{{$cant_confir}}</h5>
                        <h5>NO Confirmadas:{{$total_asis-$cant_confir}}</h5>
                        <h5>Horas Trabajadas Confirmadas:{{$h}}</h5>
                        <h5>Horas Total Horas Trabajadas :{{$h_t}}</h5>
                    </div>
                    <script>
                        function imprimir_mini() {
                            var text = document.getElementById('mini_balance').innerHTML;
                            var ventana = window.open('', 'popimp');
                            ventana.document.write(text);
                            ventana.print();

                        }</script>
                    <button onclick="imprimir_mini()" class="btn-secondary btn" style="float: right;margin-right: 25px">Imprimir mini Balance</button><a href="{{url('asistencia/empleados_view')}}"><button class="btn-success btn" style="float: right;margin-right: 25px">Atras</button></a>
                    <div class="table-responsive mb-4 mt-4">
                        <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Dia</th>
                                    <th>Hora Llegada</th>
                                    <th>Hora Salida</th>
                                    <th>Confirmada</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @section('codigos_especifico')
    <script>
        $('#html5-extension').DataTable({
            dom: '<"row"<"col-md-12"<"row"<"col-md-6"l><"col-md-6"f> > >B<"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >',
            buttons: {
                buttons: [
                    {extend: 'copy', className: 'btn'},
                    {extend: 'csv', className: 'btn'},
                    {extend: 'excel', className: 'btn'},
                    {extend: 'print', className: 'btn'}
                ]
            },
            "oLanguage": {
                "oPaginate": {"sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'},
                "sInfo": "Mostrada pagina _PAGE_ de _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Buscar",
                "sLengthMenu": "Results :  _MENU_",
            },
            "columnDefs": [
                {
                    "targets": -1,
                    "data": null,
                    "searchable": false,
                    "defaultContent": '@if(Auth()->user()->role_id !== 2 && Auth()->user()->role_id !== 5 )<a id="confirmar"   title="Confirmar"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-circle"><path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path><polyline points="22 4 12 14.01 9 11.01"></polyline></svg></a><a id="denegar" title="denegar"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg></a>@endif'
                },
                {
                    "targets": [0],
                    "visible": false,
                    "data": 'id',
                },
                {
                    "targets": [1],
                    "data": 'dia'
                },
                {
                    "targets": [2],
                    "data": 'entrada'
                },
                {
                    "targets": [3],
                    "data": 'salida'
                },
                {
                    "targets": [4],
                    "data": 'confirmado'
                }

            ],
            "stripeClasses": [],
            "lengthMenu": [10, 20, 50, 100, 1000000],
            "pageLength": 10,
            "processing": true,
            "serverSide": true,
            "ajax": "{{url('asistencia/empleado/'.$id.'/'.$inicio.'/'.$fin)}}"
        });
        $(document).ready(function () {

            App.init();
                    @if (Session::has('insertar_usuario'))

            mostrar_notificacion("{{session('insertar_usuario')}}");
            @endif

                    var table = $('#html5-extension').DataTable();
            $('#html5-extension tbody').on('click', '#confirmar', function () {
                var data = table.row($(this).parents('tr')).data();
                var consulta = new XMLHttpRequest();
                if (!window.XMLHttpRequest) {
                    consulta = new ActiveXObject("Microsoft.XMLHTTP");
                }
                consulta.onreadystatechange = function () {
                    if (consulta.readyState === 4 && consulta.status === 200) {

                        var tabla = $("#html5-extension").DataTable();
                        tabla.ajax.reload();
                        mostrar_notificacion(consulta.responseText);
                        $('#eliminarModal').modal('hide');
                    }
                };
                consulta.open("POST", '{{url("asistencia/confirmar/")}}' + '/' + data['id'], true);
                consulta.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                // consulta.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                consulta.send();
            });
            $('#html5-extension tbody').on('click', '#denegar', function () {
                var data = table.row($(this).parents('tr')).data();
                var consulta = new XMLHttpRequest();
                if (!window.XMLHttpRequest) {
                    consulta = new ActiveXObject("Microsoft.XMLHTTP");
                }
                consulta.onreadystatechange = function () {
                    if (consulta.readyState === 4 && consulta.status === 200) {
                        var tabla = $("#html5-extension").DataTable();
                        tabla.ajax.reload();
                        mostrar_notificacion(consulta.responseText);
                        $('#eliminarModal').modal('hide');
                    }
                };
                consulta.open("POST", '{{url("asistencia/denegar/")}}' + '/' + data['id'], true);
                consulta.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                // consulta.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                consulta.send();
            });
        }
        );
    </script>
    @endsection
