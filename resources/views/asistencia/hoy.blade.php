@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <h2>Informar Asistencia del dia de HOY</h2>
                    <hr>
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Hora de Entrada:</h4> 
                        <input id="datatime_entrada" class="form-control flatpickr flatpickr-input active" type="text" placeholder="Seleccione la hora de entrada">
                        <h4>Hora de Salida:</h4> 
                        <input id="datatime_salida" class="form-control flatpickr flatpickr-input active" type="text" placeholder="Seleccione la hora de salida">
                        <br>
                        <button id="informar_asistencia" class="btn btn-primary mb-2">informar</button>
                    </div>
                </div>  </div>
        </div>
    </div>
</div>
@endsection
@section('codigos_especifico')
<script>

    function mostrar_notificacion(mensaje) {
        Snackbar.show({
            showAction: false,
            text: mensaje,
        });
    }
    $(document).ready(function () {
    App.init();
            var f4 = flatpickr(document.getElementById('datatime_entrada'), {
            enableTime: true,
                    noCalendar: true,
                    dateFormat: "H:i",
                    defaultDate: "07:30"
            });
            var f5 = flatpickr(document.getElementById('datatime_salida'), {
            enableTime: true,
                    noCalendar: true,
                    dateFormat: "H:i",
                    defaultDate: "16:45"
            });
            @if (Session::has('insertar_productos'))

            mostrar_notificacion("{{session('insertar_productos')}}");
            @endif


            $('#informar_asistencia').on('click', function (e) {
    e.preventDefault();
            var consulta = new XMLHttpRequest();
            if (!window.XMLHttpRequest) {
    consulta = new ActiveXObject("Microsoft.XMLHTTP");
    }
    consulta.onreadystatechange = function () {
    if (consulta.readyState === 4 && consulta.status === 200) {
    alert(consulta.responseText);
    }

    };
            consulta.open("POST", '{{url("asistencia/hoy/informar")."/".Auth::user()->id}}' + "?llegada=" + document.getElementById('datatime_entrada').value + "&salida=" + document.getElementById('datatime_salida').value, true);
            consulta.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
            // consulta.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            consulta.send();
    });
    }
    );
</script>
@endsection
