@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <h2>Insertar  Compra o Venta</h2>
                    <div class="modal-body">
                        <form action="{{url('cv')}}" method="POST">
                            <div class="form-group row  mb-4">
                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Fecha</label>
                                <div class="col-sm-10">
                                    <input type="date" name="fecha" class="form-control form-control-lg" id="colFormLabelSm" placeholder="Introduzca el codigo" required>
                                </div>
                            </div>
                            {{csrf_field()}}
                            <div class="form-group row mb-4">
                                <label for="colFormLabel" class="col-sm-2 col-form-label">Factura</label>
                                <div class="col-sm-10">
                                    <select class="form-control form-small">
                                        <option selected="selected">orange</option>
                                        <option>white</option>
                                        <option>purple</option>
                                    </select>
                                    <script>
                                        var formSmall = $(".form-small").select2({tags: true});
                                        formSmall.data('select2').$container.addClass('form-control-sm');</script>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Productos</label>
                                <div class="col-sm-10">
                                   <select class="form-control form-small">
                                       <option selected="selected" disabled="disabled">Seleccionar Productos</option>
                                       @if($productos ?? '')
                                       @foreach($productos ?? ''  as $p)
                                       <option value="{{$p->id}}">{{$p->nombre}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                    <script>
                                        var formSmall = $(".form-small").select2({tags: true});
                                        formSmall.data('select2').$container.addClass('form-control-sm');</script>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Proveedor</label>
                                <div class="col-sm-10">
                                    <input type="text" name="stock" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca stock" required>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Rut</label>
                                <div class="col-sm-10">
                                    <input type="text" name="rut" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca el tipo" required>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Marca</label>
                                <div class="col-sm-10">
                                    <input type="text" name="marca" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca la marca" required>
                                </div>
                            </div> <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Unidad de Medida</label>
                                <div class="col-sm-10">
                                    <input type="text" name="unidad_medida" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca la unidad de medida" required>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Precio</label>
                                <div class="col-sm-10">
                                    <input type="number" name="ultimo_precioxunidad"  class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca el precio" required>
                                </div>
                            </div>
                            <input type="submit" name="time" class="mb-4 btn btn-primary" value="Agregar" >
                            <a href="{{url('listar_productos')}}"> <input type="button" name="time" class="mb-4 btn btn-success" value="Cancelar" ></a>
                        </form>
                    </div>
                </div>
            </div>

        </div>

    </div>



    <div class="modal fade" id="eliminarModal" tabindex="-1" role="dialog" aria-labelledby="eliminarModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Eliminar  Producto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="modal-text">Mauris mi tellus, pharetra vel mattis sed, tempus ultrices eros. Phasellus egestas sit amet velit sed luctus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse potenti. Vivamus ultrices sed urna ac pulvinar. Ut sit amet ullamcorper mi. </p>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancelar</button>
                    <button type="button" class="btn btn-primary">Modificar</button>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @section('codigos_especifico')

    <script>


        $(document).ready(function () {
            App.init();

        });
    </script>
    @endsection

