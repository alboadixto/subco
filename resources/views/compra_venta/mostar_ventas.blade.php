@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <h2>Listado de Facturas de Ventas</h2>
                   @if(Auth()->user()->role_id !== 3 && Auth()->user()->role_id !== 4 && Auth()->user()->role_id !== 5 ) <a href="{{url('facturas/create')}}"><button class="btn-success btn" style="float: right;margin-right: 25px">Insertar Facturas</button></a>@endif
                    <button class="btn-success btn" onclick="calcular_balance()" style="float: right;margin-right: 25px">Calcular balance</button>

                    <div class="table-responsive mb-4 mt-4">
                        <table class="table" >
                            <tbody>
                                <tr>
                                    <td>Fecha de Inicio:</td>
                                    <td><input type="date" id="min" class="form-control" name="min" ></td>
                                </tr>
                                <tr>
                                    <td>Fecha Fin:</td>
                                    <td><input type="date" id="max" class="form-control" name="max"></td>
                                </tr>
                            </tbody>
                        </table>
                        <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Folio</th>
                                    <th>Fecha de Emisión</th>
                                    <th>Proveedor</th>
                                    <th>Observaciones</th>
                                    <th>Valor Neto</th>
                                    <th>Estado</th>
                                    <th>Tipo de Tansacción</th>
                                    <th>Fecha de pago</th>
                                    <th>Operacion</th>
                                    <th>Productos</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <script>
        function calcular_balance() {
            $('#balanceModal').modal('show');
            document.getElementById('fecha_b_i').innerHTML=document.getElementById('min').value;
                    document.getElementById('fecha_b_f').innerHTML=document.getElementById('max').value;
            var consulta_m = new XMLHttpRequest();
            if (!window.XMLHttpRequest) {
                consulta_m = new ActiveXObject("Microsoft.XMLHTTP");
            }
            consulta_m.onreadystatechange = function () {
                if (consulta_m.readyState === 4 && consulta_m.status === 200) {
                                     document.getElementById('tabla_balance').innerHTML=consulta_m.responseText;
                                    }
            };
                   
            consulta_m.open("GET", "{{url('/facturas/venta/balance')}}" + '/' + document.getElementById('min').value + '/' + document.getElementById('max').value, true);
            consulta_m.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
            // consulta.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            consulta_m.send();
                    }
                    
                      function imprimir_balance(){
                           
                var text = document.getElementById('b_imprimir').innerHTML;
                var ventana = window.open('', 'solicitud_decraración_recibo');
                ventana.document.write('<style>table { width: 100%;   border: 1px solid #000;}th, td {   width: 25%;   text-align: left;   vertical-align: top;   border: 1px solid #000;}input:focus {outline: 0; outline: none;}</style>');
                ventana.document.write(text);
                ventana.print();
           
        }
    </script>

    <div class="modal fade" id="balanceModal" tabindex="-1" role="dialog" aria-labelledby="balanceModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Balance</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                </div>
                <div class="modal-body" id="b_imprimir">
                    <h3>Balance de Ventas</h3>
                    <p>Desde:<span id="fecha_b_i"><span></p>
                                <p>Hasta:<span id="fecha_b_f"><span></p>
                                            <div id="tabla_balance">
                                                Cargando.....
                                            </div>


                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancelar</button>
                                               <button   onclick="imprimir_balance()" type="button" class="btn btn-primary">Imprimir</button>
                                            </div>
                                            </div>
                                            </div>
                                            </div>

                                            <div class="modal fade" id="eliminarModal" tabindex="-1" role="dialog" aria-labelledby="eliminarModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Eliminar  Factura</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p class="modal-text">Seguro que desea eliminar la Factura:</p>
                                                            <form>
                                                                <input type="number" id="id_producto_eliminar" style="display: none">

                                                                <h2 id="nombre_producto"> </h2>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancelar</button>
                                                            <button id="btn_eliminar" type="button" class="btn btn-primary">Eliminar</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="modal fade" id="rolModal" tabindex="-1" role="dialog" aria-labelledby="rolModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Cambiar  Estado:</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h2 id="nombre_user_rol"> </h2>
                                                            <p class="modal-text">Estado Actual</p>
                                                            <p class="text-danger" id="rol_actual" style="font-size: 20px"></p>
                                                            <form>
                                                                <hr>
                                                                Cambiar a:
                                                                <input type="number" id="id_user_rol" style="display: none">
                                                                <select id="estado" name="estado" class="form-control form-small" required="required">
                                                                    <option value="0" disabled="disabled" selected="selected">Seleccione el estado de la factura</option>
                                                                    <option value="Emitida">Emitida</option>
                                                                    <option value="Registrada">Registrada</option>
                                                                    <option value="Solicitada Anulación">Solicitada Anulación</option>
                                                                    <option value="Anulación Aceptada">Anulación Aceptada</option>
                                                                    <option value="Rechazada">Rechazada</option>
                                                                    <option value="Reenviada">Reenviada</option>
                                                                    <option value="Aceptada">Aceptada</option>
                                                                    <option value="Cobrada">Cobrada</option>
                                                                </select>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancelar</button>
                                                            <button id="btn_cambiar_estado" type="button" class="btn btn-primary">Cambiar</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endsection
                                            @section('codigos_especifico')
                                            <script>
                                                $('#html5-extension').DataTable({
                                                    dom: '<"row"<"col-md-12"<"row"<"col-md-6"l><"col-md-6"f> > >B<"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >',
                                                    buttons: {
                                                        buttons: [
                                                            {extend: 'copy', className: 'btn'},
                                                            {extend: 'csv', className: 'btn'},
                                                            {extend: 'excel', className: 'btn'},
                                                            {extend: 'print', className: 'btn'}
                                                        ]
                                                    },
                                                    "oLanguage": {
                                                        "oPaginate": {"sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'},
                                                        "sInfo": "Mostrada pagina _PAGE_ de _PAGES_",
                                                        "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                                                        "sSearchPlaceholder": "Buscar",
                                                        "sLengthMenu": "Results :  _MENU_",
                                                    },
                                                    "columnDefs": [
                                                        {
                                                            "targets": -1,
                                                            "data": null,
                                                            "searchable": false,
                                                            "defaultContent": '@if(Auth()->user()->role_id !== 3 && Auth()->user()->role_id !== 4 && Auth()->user()->role_id !== 5 )<a id="editar"   title="Editar"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></a><a id="eliminar"  title="Eliminar"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg></a>         \n\
                                                      <a id="cambiar_rol"   title="Rol"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-repeat"><polyline points="17 1 21 5 17 9"></polyline><path d="M3 11V9a4 4 0 0 1 4-4h14"></path><polyline points="7 23 3 19 7 15"></polyline><path d="M21 13v2a4 4 0 0 1-4 4H3"></path></svg></a>       \n\
                                                              @endif     '
                                                        },
                                                        {
                                                            "targets": [0],
                                                            "visible": false,
                                                            "data": 'id'
                                                        },
                                                        {
                                                            "targets": [1],
                                                            "data": 'folio'
                                                        },
                                                        {
                                                            "targets": [2],
                                                            "data": 'fecha'
                                                        },
                                                        {
                                                            "targets": [3],
                                                            "data": 'proveedor'
                                                        },
                                                        {
                                                            "targets": [4],
                                                            "data": 'observaciones'
                                                        },
                                                        {
                                                            "targets": [5],
                                                            "data": 'valor_neto'
                                                        },
                                                        {
                                                            "targets": [6],
                                                            "data": 'estado_pago'
                                                        }, {
                                                            "targets": [7],
                                                            "data": 'tipo_transaccion'
                                                        }, {
                                                            "targets": [8],
                                                            "data": 'fecha_pago'
                                                        }, {
                                                            "targets": [9],
                                                            "data": 'operacion'
                                                        }, {
                                                            "targets": [10],
                                                            "data": 'tipo_producto'
                                                        },
                                                    ],

                                                    "stripeClasses": [],
                                                    "lengthMenu": [10, 20, 50, 100, 1000000],
                                                    "pageLength": 10,
                                                    "processing": true,
                                                    "serverSide": true,
                                                    "ajax": "{{url('/facturas/ventas')}}" + '/' + document.getElementById('min').value + '/' + document.getElementById('max').value
                                                });
                                                $(document).ready(function () {

                                                    App.init();
                                                            @if (Session::has('insertar_usuario'))

                                                    mostrar_notificacion("{{session('insertar_usuario')}}");

                                                    @endif
 var table = $('#html5-extension').DataTable();
                                                    $('#min').change(function () {
                                                        if(document.getElementById('min').value !== '' && document.getElementById('max').value !== ''  ){
                                                        table.ajax.url("{{url('/facturas/ventas')}}" + '/' + document.getElementById('min').value + '/' + document.getElementById('max').value);
                                                        table.draw();}
                                                    });
                                                    $('#max').change(function () {
                                                        if(document.getElementById('min').value !== '' && document.getElementById('max').value !== ''  ){
                                                        table.ajax.url("{{url('/facturas/ventas')}}" + '/' + document.getElementById('min').value + '/' + document.getElementById('max').value);
                                                        table.draw();}
                                                    });
                                                    $('#html5-extension tbody').on('click', '#editar', function () {
                                                        var data = table.row($(this).parents('tr')).data();
                                                        location.href = 'facturas/' + data['id'] + '/edit';
                                                    });
                                                    $('#html5-extension tbody').on('click', '#eliminar', function () {
                                                        var data = table.row($(this).parents('tr')).data();
                                                        document.getElementById('id_producto_eliminar').value = data['id'];
                                                        document.getElementById('nombre_producto').innerHTML = data['folio'];
                                                        $('#eliminarModal').modal('show');
                                                    });
                                                    $('#html5-extension tbody').on('click', '#cambiar_rol', function (e) {
                                                        e.preventDefault();
                                                        var data = table.row($(this).parents('tr')).data();
                                                        document.getElementById('rol_actual').innerHTML = data['estado_pago'];
                                                        document.getElementById('nombre_user_rol').innerHTML = data['folio'];
                                                        document.getElementById('id_user_rol').value = data['id'];
                                                        $('#rolModal').modal('show');
                                                    });
                                                    $('#eliminarModal').on('click', '#btn_eliminar', function (e) {

                                                        e.preventDefault();
                                                        var consulta = new XMLHttpRequest();
                                                        if (!window.XMLHttpRequest) {
                                                            consulta = new ActiveXObject("Microsoft.XMLHTTP");
                                                        }
                                                        consulta.onreadystatechange = function () {
                                                            if (consulta.readyState === 4 && consulta.status === 200) {

                                                                var tabla = $("#html5-extension").DataTable();
                                                                tabla.ajax.reload();
                                                                mostrar_notificacion(consulta.responseText);
                                                                $('#eliminarModal').modal('hide');
                                                            }

                                                        };
                                                        consulta.open("DELETE", 'facturas/' + document.getElementById('id_producto_eliminar').value, true);
                                                        consulta.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                                                        // consulta.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                                        consulta.send();
                                                    });
                                                    $('#rolModal').on('click', '#btn_cambiar_estado', function (e) {


                                                        var consulta_m = new XMLHttpRequest();
                                                        if (!window.XMLHttpRequest) {
                                                            consulta_m = new ActiveXObject("Microsoft.XMLHTTP");
                                                        }
                                                        consulta_m.onreadystatechange = function () {
                                                            if (consulta_m.readyState === 4 && consulta_m.status === 200) {

                                                                var tabla = $("#html5-extension").DataTable();
                                                                tabla.ajax.reload();
                                                                mostrar_notificacion(consulta_m.responseText);
                                                                $('#rolModal').modal('hide');
                                                            }

                                                        };
                                                        var id = document.getElementById('id_user_rol').value;
                                                        var estado = document.getElementById('estado').value;


                                                        var valores = '?id=' + id + '&estado=' + estado;

                                                        consulta_m.open("PUT", 'act_estado/' + document.getElementById('id_user_rol').value + '/' + valores, true);
                                                        consulta_m.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                                                        // consulta.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                                        consulta_m.send();
                                                    });










                                                });




                                            </script>

                                            @endsection

