@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <h2>Empleado:<span id="nombre_empleado">{{$empleado->name}}</span></h2>

                    <form action="{{url('contratos/actualizar')}}" method="POST" >

                        <input type="text" name="id_contrato" value="{{$contrato->id}}" style="display: none">
                        <div class="modal-body" id="todo">
                            <div id="contrato_imprimir">
                            <center><img src="{{asset('img\logo.png')}}" ></center>
                            <p>En Santiago a <input id="fecha_contrato" type="date" name="fecha_contrato" value="{{$contrato->fecha_contrato}}"   required="required">entre la empresa <strong>SERVICIOS DE RECURSOS HUMANOS Y LIMPIEZA SUBCO LIMITADA</strong>, RUT 76.855.721-7, sociedad del giro  comercial, representada por  YENNY ALEJANDRA GUAJARDO GONZALEZ, Factor de Comercio, cédula nacional de identidad Nº 14.159.428-1, ambos domiciliados en Huerfanos Nº 1055 oficina 505, Santiago Centro, en adelante, <strong>"El Empleador"</strong>; por una parte y por la otra, don 
                                <input type="text" name="nombre"  id="nombre" required="required" placeholder="Nombre" value="{{$empleado->name}}">
                                de nacionalidad chilena,  cuya especialidad es AUXILIAR- COORDINADOR DE ASEO, cédula nacional de identidad Numero :
                                <input type="text" name="identidad" onchange="rut(this.value)"  id="identidad_contratador" required="required" placeholder="No. identidad" value="{{$contrato->identidad}}">
                                nacido el día
                                <input id="fecha_nacimiento_contratador" value="{{$contrato->fecha_nacimiento_contratador}}" type="date" name="fecha_nacimiento_contratador"   required="required">
                                ,y en consecuencia, capaz de celebrar contrato de trabajo, domiciliado en 
                                <input id="lugar_trabajo" type="text" name="lugar_trabajo"  value="{{$contrato->lugar_trabajo}}"  required="required" placeholder="Dirección">
                                quien, en adelante, se llamará "El Trabajador", se ha convenido el siguiente contrato laboral
                            </p>
                            <p>  El trabajador se compromete y obliga a ejecutar el trabajo de
                                <input id="nombre_trabajo" type="text" value="{{$contrato->nombre_trabajo}}" name="nombre_trabajo"   required="required" placeholder="Puesto de Trabajo">
                            </p>
                            {{csrf_field()}}
                            <p>Los servicios se prestarán en:
                                <input id="lugar_de_servicios" value="{{$contrato->lugar_de_servicios}}" type="text" name="lugar_de_servicios"   required="required" placeholder="Lugar de Servicio">
                                sin perjuicio de la facultad del empleador de alterar, por causa justificada, la naturaleza de los servicios, o el sitio o recinto en que ellos han de prestarse, con la sola limitación de que se trate de labores similares y que el nuevo sitio o recinto quede dentro de la misma localidad o ciudad.
                            </p>
                            <p>
                                La jornada de trabajo será de 45 horas semanales y se desarrollará en turnos rotativos que irán de lunes a Viernes,  distribuidas en un ciclo de 5 días de trabajo y 2 día de descanso, con una jornada diaria máxima de 9 horas, con media hora de colación a cargo del trabajador y en los siguientes horarios:</p>
                            <textarea id="turnos_trabajo" name="turnos_trabajo" rows="3" cols="130" placeholder="Descripción de horarios y turnos de trabajo">{{$contrato->turnos_trabajo}}</textarea>
                            <p>Cuando por necesidades del funcionamiento de la Empresa, sea necesario pactar trabajo en tiempo extraordinario, el Empleado que lo acuerde desde luego se obligara a cumplir el horario que al efecto determine la Empleadora, dentro de los límites legales. Dicho acuerdo constara por escrito y se firmara por ambas partes, previamente a la realización del trabajo.</p>
                            <p>A falta de acuerdo, queda prohibido expresamente al empleado trabajar sobretiempo o simplemente permanecer en el recinto de la Empresa, después de la hora diaria de salida, salvo en los casos a que se refiere el inciso precedente.</p>
                            <p>El tiempo extraordinario trabajado de acuerdo a las estipulaciones precedentes, se remunerará con el recargo legal correspondiente y se liquidara y pagará conjuntamente con el sueldo del respectivo periodo.</p>
                            <p>El empleador se compromete a remunerar los servicios del trabajador con un sueldo base mensual de: <input value="{{$contrato->sueldo_base}}" id="sueldo_base" type="number" name="sueldo_base"   required="required" placeholder="Sueldo base">Sin perjuicio de lo anterior, la empresa pagará mensualmente el <input id="porciento_gratificacion" value="{{$contrato->porciento_gratificacion}}" type="number" name="porciento_gratificacion"   required="required" placeholder="% gratificación">%de gratificación correspondiente a un monto de:<input id="monto_base" type="number" name="monto_base" value="{{$contrato->monto_base}}"   required="required" placeholder="Monto base"> </p>
                            <p>Además, el empleador se compromete a otorgar a suministrar al trabajador los siguientes beneficios:</p>
                            <p>Asignación de Movilizació:  <input id="movilizacion" value="{{$contrato->movilizacion}}" type="number" name="movilizacion"   required="required" placeholder="Costo de Movilización"> </p>
                            <p>Asignación de Colación:   <input id="colocacion"  value="{{$contrato->colocacion}}" type="number" name="colocacion"   required="required" placeholder="Costo de Colocación">    </p>
                            <p>El trabajador, asimismo, acepta y autoriza al Empleador para que haga las deducciones que establecen las leyes vigentes y, para que le descuente el tiempo no trabajado debido a atrasos, inasistencias o permisos y, además, la rebaja del monto de las multas establecidas en el Reglamento Interno de Orden, Higiene y Seguridad, en caso de que procedieren. Se entenderá por atraso reiterado el llegar después de la hora de ingreso durante 2 días seguidos o dos lunes seguidos, en cada mes calendario. Bastara para acreditar esta situación la constancia en el respectivo Control de Asistencia.</p>
                            <p>Las inasistencias serán sancionadas según lo dispuesto en la causal 3ª del artículo 160 de la ley 19.010.</p>
                            <p>Las deducciones que la Empleadora podrá según los casos – practicar a las remuneraciones, son todas aquellas que dispone el artículo 58 del Código del Trabajo.</p>
                            <p>El trabajador se compromete y obliga expresamente a cumplir las instrucciones que le sean impartidas por su jefe inmediato o por la gerencia de la empresa, en relación con su trabajo. y acatar en todas sus partes las normas del Reglamento Interno de Orden, Higiene y Seguridad, las que declara conocer y que forman parte integrante del presente contrato. El trabajador se compromete y obliga expresamente a cumplir las instrucciones que le sean impartidas por su jefe inmediato o por la gerencia de la empresa, en relación con su trabajo. y acatar en todas sus partes las normas del Reglamento Interno de Orden, Higiene y Seguridad, las que declara conocer y que forman parte integrante del presente contrato. </p>
                            <p><strong> OBLIGACIONES DEL TRABAJADOR.</strong></p>
                            <br>
                            <p>Son obligaciones del TRABAJADOR las que se señalan a continuación, teniendo la calidad de esenciales, dejándose expresa constancia que la inobservancia, por parte de éste, de cualquiera de dichas normas, se considerará “<strong>incumplimiento grave de las obligaciones que impone el Contrato”</strong>, para todos los efectos legales:</p>
                            <p><strong>1.</strong>Presentarse a su lugar de trabajo puntualmente y en óptimas condiciones físicas y de presentación personal.</p>
                            <p><strong>2.</strong>Registrar de manera personal, en el Libro respectivo, su asistencia a sus labores habituales.</p>
                            <p><strong>3.</strong>Desarrollar las tareas concernientes a su trabajo, en la forma más eficaz posible, empleando para ello la mayor diligencia y dedicación.</p>
                            <p><strong>4.</strong>Cumplir todas y cada una de las funciones descritas en el presente contrato de trabajo y en los anexos de contrato, que lo complementen</p>
                            <p><strong>5.</strong>Utilizar el uniforme completo de la Empresa, incluyendo la Tarjeta de Identificación.</p>
                            <p><strong>6.</strong>Dar cuenta inmediata a sus superiores directos o a los directivos o encargados del lugar en que presta servicios, de todo hecho o situación que altere la normalidad y/o seguridad de éste.</p>
                            <p><strong>7.</strong>Mantener una conducta moral intachable y excelente relaciones humanas, tanto con sus compañeros de trabajo como con otras personas que laboren o visiten la instalación.</p>
                            <p><strong>8.</strong>Mantener relaciones sólo profesionales con el personal del EMPLEADOR, como, asimismo, con el personal de la empresa Mandante <strong>SUBSECRETARIA DE SALUD PUBLICA</strong></p>
                            <p><strong>9.</strong>Mantenerse despierto y atento durante toda la jornada de trabajo.</p>
                            <p><strong>10.</strong>Acatar las instrucciones que le impartan sus Superiores directos, como también los directivos de <strong>SERVICIOS DE RECURSOS HUMANOS Y LIMPIEZA SUBCO LIMITADA.</strong></p>
                            <p><strong>11.</strong>Mantener estricta reserva de todos aquellos antecedentes que directa o indirectamente lleguen a su conocimiento sobre negocios u operaciones de la Empresa Principal <strong>SUBSECRETARIA DE SALUD PUBLICA.</strong>, o de su EMPLEADOR, aunque en ellas no intervenga el TRABAJADOR.</p>
                            <p><strong>12.</strong>No ausentarse de sus labores, ni del recinto o área en que se encuentre destinado, sin permiso previo del Supervisor o Jefe de Turno. Asimismo, no podrá abandonar el lugar o recinto donde se desempeña a menos que sea reemplazado por otro <strong>AUXILIAR- COORDIANDOR DE ASEO.</strong> Tampoco, podrá abandonar su puesto de trabajo antes que llegue el relevo correspondiente.</p>
                            <p><strong>13.</strong>Comunicar al EMPLEADOR, por el medio más rápido, personalmente o través de algún familiar, acerca de eventuales atrasos o ausencias a su trabajo, a fin de que el EMPLEADOR pueda disponer su reemplazo en forma oportuna.</p>
                            <p><strong>14.</strong>Comunicar por escrito al EMPLEADOR dentro de las 48 horas de producidos cambios de su domicilio particular, teléfono, estado civil, cargas familiares y/o cualquier otro antecedente curricular y/o personal.</p>
                            <p><strong>15.</strong>15.	En caso de inasistencia al trabajo por enfermedad, el TRABAJADOR, deberá justificarla- únicamente- con el correspondiente Certificado Médico, dentro del plazo de 24 horas desde que dejo de asistir a sus labores.</p>
                            <p><strong>16.</strong>16.	Las partes declaran que el Reglamento Interno de Orden y Seguridad de SERVICIOS DE RECURSOS HUMANOS Y LIMPIEZA SUBCO LIMITADA, forma parte integral, del presente contrato de trabajo en forma íntegra, entendiéndose incorporadas como obligaciones contractuales todas aquellas que para el TRABAJADOR estable dicho Reglamento de Orden, Seguridad e Higiene, las que se dan por expresamente reproducidas, tanto o cuando importen o prohibiciones para el TRABAJADOR. Además, el TRABAJADOR, declara que acatará el Reglamento de Orden, Seguridad e Higiene de la Empresa Principal en que prestará servicios.</p>
                            <br>
                            <p><strong>PROHIBICIONES AL TRABAJADOR.</strong></p>
                            <p>Las partes establecen que serán actos prohibidos para el trabajador, los que se señalan a continuación:</p>
                            <p><strong>1.</strong>No usar en el servicio el vestuario y/o equipo entregado por el EMPLEADOR para el desempeño de sus funciones; modificar o alterar alguna de las prendas del uniforme o no usar los distintivos <strong>SERVICIOS DE RECURSOS HUMANOS Y LIMPIEZA SUBCO LIMITADA.</strong> Asimismo, utilizar el uniforme y/o equipo fuera del recinto donde debe desempeñar sus funciones propias.</p>
                            <p><strong>2.</strong>Cambiar el servicio a otro trabajador, sin contar con la autorización de su EMPLEADOR.</p>
                            <p><strong>3.</strong>Formular reclamos, peticiones o tratar temas de exclusiva competencia <strong>SERVICIOS DE RECURSOS HUMANOS Y LIMPIEZA SUBCO LIMITADA.</strong>, con el mandante <strong>SUBSECRETARIA DE SALUD PUBLICA.</strong>, a objeto éste actúe o sirva de interlocutor ante el EMPLEADOR. Esto además y sin perjuicio de lo establecido precedentemente</p>
                            <p><strong>4.</strong>Permitir el ingreso o la permanencia en el lugar de trabajo de personas no autorizadas expresamente para ello. Esta prohibición comprende los familiares y amistades del TRABAJADOR.</p>
                            <p><strong>5.</strong>Realizar durante su jornada actividades ajenas a sus funciones a la instalación o estación en que presta servicios o asuntos personales y/o desarrollar dentro de la faena actividades sociales, políticas o sindicales.</p>
                            <p><strong>6.</strong>Solicitar a la empresa <strong>SUBSECRETARIA DE SALUD PUBLICA.</strong>, útiles de ASEO u otros elementos para el servicio que deban ser proporcionados por el EMPLEADOR, y/o pedir a éste que los requiera.</p>
                            <p><strong>7.</strong>Solicitar préstamos de dinero o de especies a la empresa <strong>SUBSECRETARIA DE SALUD PUBLICA.</strong>, a su personal y/o a sus contratistas, o aceptarles regalos, donaciones, atenciones y/o beneficios.</p>
                            <p><strong>8.</strong>Ingresar al lugar de trabajo elementos o equipos personales con excepción de aquellos proporcionados por el EMPLEADOR.</p>
                            <p><strong>9.</strong>Ocultar u omitir información y/o encubrir actividades o hechos anormales o irregularidades que sean detectadas en instalaciones de la empresa <strong>SUBSECRETARIA DE SALUD PUBLICA.</strong> o cometidas por personal del EMPLEADOR.</p>
                            <p><strong>10.</strong>Presentarse al trabajo, ingresar o permanecer en las instalaciones del EMPLEADOR o en la instalación de la empresa <strong>SUBSECRETARIA DE SALUD PUBLICA</strong>. donde ésta presta servicios, bajo los efectos del alcohol, drogas o estupefacientes, debido a las condiciones inseguras que se crean. Por ningún motivo el TRABAJADOR podrá trabajar si presenta síntomas de anormalidad por dichas causas. Asimismo, poseer, mantener, consumir, entregar, distribuir, vender o facilitar de cualquier forma a terceros, sean o no TRABAJADORES del EMPLEADOR o del Cliente, cualquier tipo de bebidas alcohólicas, psicofármacos, medicamentos o drogas ilícitas en dependencias o lugares de trabajo. En el caso de las drogas, consumirlas o darlas a consumir en cualquier momento o circunstancia, ya sea antes, durante o después del trabajo, en dependencias o lugares de trabajo. El EMPLEADOR podrá realizar exámenes aleatorios a sus TRABAJADORES, en sus respectivos lugares de trabajo, sin ningún tipo de exclusión de personas, consistentes en detección de uso y/o influencia del alcohol o drogas, que alteren o comprometan la aptitud del TRABAJADOR. Para tales efectos, el TRABAJADOR permitirá que, por parte de los profesionales u otra personal que el EMPLEADOR designe, se le realice cualquier tipo de examen, en especial alcotest, alcoholemia o test de detección de consumo de drogas, debiendo prestar las facilidades que se requieran para tal efecto. En caso que alguno de los referidos exámenes de resultado positivo el TRABAJADOR acepta el término inmediato de su Contrato por incumplimiento grave a las obligaciones que impone el mismo.</p>
                            <p><strong>11.</strong>Ingresar y/o usar  televisores, barajas, naipes, juegos de azar y otras formas de entretención de cualquier clase o especie, en las oficinas o lugares de trabajo.</p>
                            <p><strong>12.</strong>Utilizar un lenguaje inadecuado, o participar en cualquier acción o situación obscena, inmoral o reñida con las buenas costumbres.</p>
                            <p><strong>13.</strong>Fumar durante el desempeño de sus labores. </p>
                            <p><strong>14.</strong>Prestar servicios con una mala presentación personal, entendiéndose por tal el uso de pelo largo, mal  afeitado, uniforme sucio o con hoyos, o portando accesorios que no correspondan al uniforme.</p>
                            <p><strong>15.</strong>Faltar el respeto en forma verbal o escrita o a través de gestos a sus Superiores, personal de la empresa <strong>SUBSECRETARIA DE SALUD PUBLICA.</strong>, compañeros de trabajo y/o a cualquier persona, en los recintos de la empresa mandante o de su EMPLEADOR.</p>
                            <p><strong>16.</strong>16.	Realizar dentro o fuera de la jornada de trabajo, actividades comprendidas dentro del giro del EMPLEADOR, sea que ello se realice a título gratuito o oneroso.
                                La inobservancia de cualquiera de las normas antes indicadas, por parte del TRABAJADOR, se considerará <strong>“incumplimiento grave de las obligaciones que impone el contrato”</strong>, para todos los efectos legales, facultando al EMPLEADOR, para poner término inmediato al presente contrato de trabajo individual. 
                            </p>
                            <br>
                            <p><strong>OCTAVO: DE LAS FUNCIONES DEL TRABAJADOR.</strong></p>
                            <p>Las partes dejan expresa constancia de que la función del <strong>Auxiliar-coordinador de Aseo</strong>  en el <strong>SUBSECRETARIA DE SALUD PUBLICA</strong>, tiene como propósito de mantener las dependencias ubicada en San Diego N° 630 Comuna Santiago Centro, en óptimas condiciones de aseo y limpieza, a lo cual el trabajador declara conocer y aceptar. </p>
                            <p><strong>•</strong>Verificar que el personal a cargo firme el libro de asistencia de manera correcta.</p>
                            <p><strong>•</strong>Mantener comunicación fluida, con los coordinadores del servicio SUBSECRETARIA DE SALUD PUBLICA Y su personal a cargo respecto a las funciones de aseo y limpieza</p>
                            <p><strong>•</strong>Realizar inventarios de insumos y distribución de estos.</p>
                            <p><strong>•</strong>Velar por el uso correcto de todo el personal a su cargo del uniforme exigido por la empresa mandante, usando la credencial a la vista y mantener una excelente presentación personal</p>
                            <p><strong>•</strong>Verificar que el personal se encuentre trabajando con sus elementos de protección personal</p>
                            <p><strong>•</strong>Coordinar relevo en caso de cualquier imprevisto, emergencia y/o ausencias, licencias médicas, que surja con su personal a cargo.</p>
                            <p><strong>•</strong>Notificar de inmediato al empleador en caso de accidente laboral del personal.</p>
                            <p><strong>•</strong>Chequear y velar el buen uso de maquinarias e insumos de limpieza y aseo, como Abrillantadora, barredoras, aspiradoras, etc.</p>
                            <p><strong>•</strong>A cargo de velar que el esquema de trabajo solicitado, por el mandante SUBSECRETARIA DE SALUD PUBLICA, se cumplan a cabalidad, según las actividades detalladas claramente en el contrato de trabajo como primera actividad, diarias, semanal y mensual.</p>
                            <p><strong>•</strong>Planificar la salida a colación de sus auxiliares de aseo</p>
                            <p><strong>•</strong>Dar aviso de inmediato al Jefe Directo de las pérdidas, deterioros y descomposturas que sufran los objetos de trabajo, máquinas o herramientas a su cargo especialmente si pueden ocasionar daño a las personas o a la propiedad</p>
                            <p><strong>•</strong>Velar por el cumplimiento general de las obligaciones, solicitadas por el SUBSECRETARIA DE SALUD PUBLICA.</p>
                            <table class="table-bordered">
                                <tr>
                                    <td >1.- Retirar y cambiar las bolsas en depósitos de
                                        desechos
                                    </td>
                                    <td>Dos veces al
                                        día
                                    </td>
                                    <td>Bolsas de polietileno</td>
                                </tr>
                                <tr>
                                    <td>2.- Mantención de pisos madera</td>
                                    <td>Diario</td>
                                    <td>Escobillón y trapero y enceradora</td>
                                </tr>
                                <tr>
                                    <td>3.- Mantención pisos cerámicos</td>
                                    <td>Diario</td>
                                    <td>Escobillón trapero húmedo o mopa</td>
                                </tr>
                                <tr>
                                    <td>4.- Limpieza de lavaplatos y lavamanos</td>
                                    <td>Diario</td>
                                    <td>Limpiador abrasivo en crema (Cif®), desinfección con desinfectante uso diario</td>
                                </tr> <tr>
                                    <td>5.- Lavado de basureros, baños y cocina</td>
                                    <td>Semanalmente</td>
                                    <td>Detergente multiuso y desinfectante uso común</td>
                                </tr> <tr>
                                    <td>6.- Limpieza de los vidrios de puertas y/o mamparas de acceso a las áreas de la Unidad</td>
                                    <td>Quincenal</td>
                                    <td>Solución limpia vidrios o limpia parabrisas</td>
                                </tr> <tr>
                                    <td>7.-Limpieza de superficie exterior de computadores</td>
                                    <td>Diario</td>
                                    <td>Diario</td>
                                </tr> <tr>
                                    <td>8.-Limpieza de las superficies externas de equipos de refrigeración (Refrigerador, Vitrina y Freezer, aires
                                        acondicionados)
                                    </td>
                                    <td>Semanal</td>
                                    <td>Paños de sacudir, plumeros, paños húmedos con solución liquidas de limpieza</td>
                                </tr> <tr>
                                    <td>9.- Limpieza de coolers de toma de muestras</td>
                                    <td>mensual</td>
                                    <td>Paño húmedo, limpiador líquido, desinfectantes uso común</td>
                                </tr>
                                <tr>
                                    <td>10.-Limpieza de los bordes de muros y marcos alrededor de las ventanas de vidrios y/o puertas separador<>as</td>
                                    <td>Quincenal</td>
                                    <td>Plumeros, y paños limpieza</td>
                                </tr>
                                <tr>
                                    <td>11.-Limpieza de los bordes de muros y marcos alrededor de las ventanas de vidrios y/o puertas separadoras</td>
                                    <td>Quincenal</td>
                                    <td>Agua tibia</td>
                                </tr>
                                <tr>
                                    <td>12.- Deshielo y limpieza interior de los refrigeradores</td>
                                    <td>Según requerimiento del Encargado</td>
                                    <td>Agua tibia con bicarbonato</td>
                                </tr>
                                <tr>
                                    <td>13.-Limpieza de los vidrios de paneles separadores al interior del laboratorio</td>
                                    <td>Según programación entregada por coordinador de Laboratorio</td>
                                    <td>Solución limpiavidrios o limpiaparabrisas</td>
                                </tr>
                                <tr>
                                    <td>14.-Limpieza de los vidrios de ventanas al exterior</td>
                                    <td>Trimestral</td>
                                    <td>Solución limpiavidrios o limpiaparabrisas</td>
                                </tr>
                                <tr>
                                    <td>15.-Limpieza de las persianas en ventanas exteriores</td>
                                    <td>Semestral</td>
                                    <td>Detergente neutro multiuso</td>
                                </tr>
                            </table>
                            <table class="table-bordered">
                                <tr>
                                    <td>1.- Retirar y cambiar las bolsas en depósitos de desechos</td>
                                    <td>Diario</td>
                                    <td>Bolsas de polietileno</td>
                                </tr>
                                <tr>
                                    <td>2.- Mantención de pisos</td>
                                    <td>Diario</td>
                                    <td>Agua</td>
                                </tr>
                                <tr>
                                    <td>3.- Lavado de los pisos</td>
                                    <td>Semanal</td>
                                    <td>Detergente neutro multiuso</td>
                                </tr>
                                <tr>
                                    <td>4.- Limpieza de lavaderos y lavamanos</td>
                                    <td>Diario</td>
                                    <td>Limpiador abrasivo en crema
                                        (Cif®)
                                    </td>
                                </tr>
                                <tr>
                                    <td>5.- Lavado de basureros</td>
                                    <td>Semanalmente o dependiendo de requerimiento coordinado con encargado de área</td>
                                    <td>Detergente neutro multiuso</td>
                                </tr>
                                <tr>
                                    <td>6.- Limpieza de los vidrios de puertas y/o mamparas de
                                        acceso a las áreas del laboratorio
                                    </td>
                                    <td>Semanal</td>
                                    <td>Solución limpia vidrios o
                                        limpia parabrisas
                                    </td>
                                </tr>
                                <tr>
                                    <td>7.-Limpieza de superficie exterior de computadores</td>
                                    <td>Semanal</td>
                                    <td>Detergente Multiuso</td>
                                </tr>
                                <tr>
                                    <td>8.-Limpieza de las superficies externas de equipos de
                                        refrigeración (Refrigerador, Vitrina y Freezer)
                                    </td>
                                    <td>Semanal</td>
                                    <td>Agua tibia</td>
                                </tr>
                                <tr>
                                    <td>9.-Limpieza de los bordes de muros y marcos alrededor de las ventanas de vidrios y/o puertas separadoras</td>
                                    <td>Quincenal</td>
                                    <td>Agua tibia</td>
                                </tr>
                                <tr>
                                    <td>10.- Deshielo y limpieza interior de los refrigeradores</td>
                                    <td>Según requerimiento del Encargado</td>
                                    <td>Agua tibia con bicarbonato</td>
                                </tr>
                                <tr>
                                    <td>11.-Limpieza de los vidrios de paneles separadores al interior del laboratorio</td>
                                    <td>Según programación entregada por coordinador de Laboratorio</td>
                                    <td>Solución limpiavidrios o limpiaparabrisas</td>
                                </tr>

                                <tr>
                                    <td>12.-Limpieza de los vidrios de ventanas al exterior</td>
                                    <td>Trimestral</td>
                                    <td>Solución limpiavidrios o limpiaparabrisas</td>
                                </tr>
                                <tr>
                                    <td>13.-Limpieza de las persianas en ventanas exteriores</td>
                                    <td>Semestral</td>
                                    <td>Detergente neutro multiuso</td>
                                </tr>
                                <tr>
                                    <td>14.-Lavado de Cooler</td>
                                    <td>Semanal y emergencias puntuales</td>
                                    <td>Detergente, escobilla y alcohol</td>
                                </tr>

                            </table>
                            <table class="table-bordered">

                                <tr>
                                    <td>1. Vaciar papeleros y cambiar bolsas</td>
                                    <td>En oficinas                     
                                    </td>
                                    <td>Diario</td>
                                </tr>
                                <tr>
                                    <td>1. Vaciar papeleros y cambiar bolsas</td>
                                    <td>En Servicios sanitarios                    
                                    </td>
                                    <td>Dos veces al día</td>
                                </tr>
                                <tr>
                                    <td>2.- Limpieza de baños</td>
                                    <td>Diario</td>
                                    <td>Diario</td>
                                </tr>
                                <tr>
                                    <td>3.- Limpieza de pisos</td>
                                    <td>Diario</td>

                                </tr>
                                <tr>
                                    <td>4.- Aspirado de alfombras en oficinas y sillas tapizadas</td>
                                    <td>Dos veces a la semana (Preferentemente viernes y martes)</td>

                                </tr>
                                <tr>
                                    <td>5.- Limpieza de computadores</td>
                                    <td>Semanal</td>

                                </tr>
                                <tr>
                                    <td>6.- Limpieza y lustrado de escritorios</td>
                                    <td>Semanal</td>

                                </tr>
                                <tr>
                                    <td>7.- Limpieza de microondas y refrigeradores áreas de casino</td>
                                    <td>Semanal</td>

                                </tr>
                                <tr>
                                    <td>8.- Eliminación del polvo en muebles archivadores de oficinas</td>
                                    <td>Una vez por mes</td>

                                </tr>
                                <tr>
                                    <td>9.- Limpieza de paneles de vidrios, aspirado de marcos y cantos de muros a su alrededor</td>
                                    <td>Semestral</td>

                                </tr>
                                <tr>
                                    <td>10.- Limpieza de paneles separadores de oficina y puertas</td>
                                    <td>Semestral</td>

                                </tr>

                            </table>
                            <br>
                            <p>El presente contrato tendrá vigencia hasta el <input type="date" name="fecha_fin" name="fecha_fin" value="{{$contrato->fecha_fin}}"> 2019 y sólo podrá ponérsele término en conformidad a la legislación vigente.</p>
                            <p>Se deja constancia que el trabajador ingresó al servicio del empleador el <input type="date" name="inicio_trabajo" id="inicio_trabajo" value="{{$contrato->inicio_trabajo}}">.</p>
                            <p>Para todos los efectos derivados del presente contrato las partes fijan domicilio en la ciudad de Santiago, y se someten a la Jurisdicción de sus Tribunales.</p>
                            <p>COTIZACIONES PREVISIONALES Y DE SALUD.Para todos los efectos derivados del presente derivados del presente Contrato, el TRABAJADOR declara, que mantiene sus fondos previsionales en AFP <input type="text" name="apf" id="afp" value="{{$contrato->apf}}"> y de salud en <input name="afp_salud" value="{{$contrato->afp_salud}}"></p>
                            <p>El presente contrato se firma en dos ejemplares, declarando el trabajador haber recibido en este acto un ejemplar de dicho instrumento, que es el fiel reflejo de la relación laboral convenida.</p>

                            <br>
                            <br>
                            </div>
                            <div id="solicitud_anticipo">
                                <p>
                                    <img style="float: left" src="{{asset('img\logo.png')}}" > <br><br><br><br><br>  </p>
                                <center><strong>SOLICITUD DE ANTICIPO</strong></center>
                                <p>Yo,  <input type="text" id="anticipo_nombre" name="anticipo_nombre" placeholder="Nombre" value="{{$contrato->nombre}}"> Rut:<input name="anticipo_rut" placeholder="RUT" id="anticipo_rut" value="{{$empleado_obj->rut}}"> Mediante el presente informo a  SERVICIOS DE RECURSOS HUMANOS Y LIMPIEZA SUBCO LIMITADA Rut:76.855.721-7      que:</p> 
                                <p>SI: <input type="radio" name="anticipo" value="true" @if($contrato->anticipo == true) checked @endif > Solicito Anticipo de Sueldo Mensual	$50.000.-
                                    AUXILIAR -COORDINADOR DE ASEO
                                </p>
                                <p>No: <input type="radio" name="anticipo" value="false"  @if($contrato->anticipo == false) checked @endif> Solicito Anticipo de Sueldo Mensual </p>
                                <p>Declaro estar en conocimiento que este anticipo es mensual y fijo y que será cancelado desde el mes siguiente a la fecha de ser solicitado. Además de ser proporcional al cargo que desempeño en la empresa. También estoy en conocimiento que este anticipo será rebajado en los siguientes casos. Licencias Médicas, Inasistencias Reiteradas, o algún motivo por el cual se vea disminuido el ingreso líquido mensual recibido.</p>
                                <br><br><br><br>
                                <p>Firma Trabajador	: __________________________</p>
                                <p>Nombre:<input type="text" name="anticipo_trabajador" id="anticipo_trabajador" value="{{$contrato->nombre}}"></p>
                                <p>Rut: <input type="text" name="anticipo_rut_trabajador" id="anticipo_rut_trabajador" value="{{$empleado_obj->rut}}"></p>
                            </div>



                            <div id="solicitud_decraración_recibo">
                                <p>
                                    <img style="float: left" src="{{asset('img\logo.png')}}" > <br><br><br><br><br>  </p>
                                <p><center><strong>EMPRESA  SERVICIOS DE RECURSOS HUMANOS Y LIMPIEZA LIMITADA</strong></center></p>
                                <p>DECLARACIÓN DE RECIBO DEL REGLAMENTO INTERNO DE ORDEN E HIGIENE Y SEGURIDAD</p>
                                <p>Ley N° 16.744 & Código del Trabajo) Y CUMPLIMIENTO  D.S.  Nº40 (Deber de Informar)</p>
                                <p style="float: left">Cargo: AUXILIAR-COORDIANDOR DE ASE<br>
                                    Fecha de Entrega: <input type="date" name="fecha_entrega" placeholder="Fecha entrega" value="{{$contrato->fecha_entrega}}"></p>
                                <br><br><br>
                                <p>Yo <input type="text" id="recibo_nombre" name="recibo_nombre" value="{{$empleado->name}}"> Rut: <input type="text" name="recibo_rut" id="recibo_rut" value="{{$empleado_obj->rut}}"> Me doy por informado acerca de los riesgos que entraña mi labor, ya que se me explicó las medidas preventivas y de los métodos de trabajo correctos en las tareas que debo desarrollar (Deber de Informar o Derecho a Saber). Por tanto, Certifico que en conformidad a la Ley de Accidentes del Trabajo n° 16.744 y lo ordenado por el Código del Trabajo, he recibido de <strong>SERVICIOS DE RECURSOS HUMANOS Y LIMPIEZA LIMITADA</strong>. el Reglamento Interno de Orden, Higiene y Seguridad.</p>
                                <p>Comprometiéndome a respetar y llevar a cabo las normas y procedimiento mencionados en él.</p>
                                <p>Este Reglamento Interno de Orden, Higiene y Seguridad, remplaza a los reglamentos que sobre la misma se encuentran vigentes en la Empresa, sus normas y disposiciones regirán a partir de esta fecha en todas las dependencias y en el futuro en aquellos centros de trabajo que la empresa pudiese crear.</p>
                                <p>Constituyendo este Reglamento un todo Orgánico, si debido a disposiciones legales fuese derogado algún acápite de este, la rectificación se limitará a la parte invalidada o modificada, quedando en vigor el resto.
                                    <br><br><br><br><br><br>
                                <p style="float: left">
                                    Firma Trabajador	: __________________________  <br>
                                    Nombre		: <input type="text" name="recibo_nombre_coordinadora" value="{{$empleado->name}}"><br>
                                    Rut		<input type="text" name="recibo_rut_coordinadora" value="{{$empleado_obj->rut}}"><br>
                                    Instalación		:SUBSECRETARIA DE SALUD PUBLICA<br>
                                    CC.: Carpeta Trabajador 

                                </p>
                            </div>
                            <br><br><br><br><br>


                            <div id="temario_charla">
                                <table class="table-bordered">
                                    <tr>
                                        <td> <img style="float: left" src="{{asset('img\logo.png')}}" ></td>
                                        <td><strong>TEMARIO CHARLA DE INDUCCION TRABAJADOR NUEVO Y/O REUBICADO</strong></td>
                                        <td><img style="float: left" src="{{asset('img\logo.png')}}" ></td>
                                    </tr>
                                    <tr>
                                        <td>        Resp : Gerencia, Supervisores,                          
                                            Dpto. Prev. De Riesgos
                                        </td>
                                        <td>DEPARTAMENTO DE PREVENCIÓN DE RIESGOS</td>
                                        <td>+ Seguridad,
                                            + Calidad de Vida
                                        </td></tr>
                                    <tr>
                                        <td> </td>
                                        <td>For :  SSO_02_DAS_TEM_V02</td>
                                        <td>SISTEMA DE GESTION DE SEGURIDAD
                                            Y SALUD OCUPACIONAL VERSION 2.0
                                        </td></tr>
                                </table>
                                <br><br><br>
                                <center>DECLARACION</center>
                                <br>
                                <p>Declaro que la empresa <strong>SERVICIOS DE RECURSOS HUMANOS Y LIMPIEZA SUBCO LIMITADA Rut: 76.855.721-7</strong>   . Cumpliendo con lo establecido en el Decreto Nº 50 incorporado en el título VI del D.S. Nº 40, sobre la obligación de informar de los riesgos laborales (Derecho a saber), me han informado e instruido en los riesgos que conllevan mis labores y las medidas de control correspondientes a los trabajos que desarrollaremos para <strong>SERVICIOS DE RECURSOS HUMANOS Y LIMPIEZA SUBCO LIMITADA</strong>. En las distintas instalaciones que puedan ser designado/a el trabajador/a.</p>
                                <table class="table-bordered">
                                    <tfoot>               TEMAS TRATADOS</tfoot>
                                    <tr><td>l.	Política de la empresa en materias de Prevención de Riesgos.</td></tr>
                                    <tr><td>2.	Ley Nº 16.744 sobre accidentes del trabajo y enfermedades profesionales (Aspectos básicos).</td></tr>
                                    <tr><td>3.	Procedimiento a seguir en caso de accidente del trabajo, accidentes del trayecto y enfermedades profesionales.</td></tr>
                                    <tr><td>4.	Riesgos típicos en la Instalación y dependencias, donde sea ubicado el trabajador.</td></tr>
                                    <tr><td>5.	El respeto, importancia, cuidado y seguimientos de lo indicado por la señalética del lugar.</td></tr>
                                    <tr><td>6.	Capacitación en uso correcto y cuidado de los elementos de protección personal.</td></tr>
                                    <tr><td>7.	Capacitación en uso y manejo correcto de los equipos extintores de incendio.</td></tr>
                                    <tr><td>8.	Conocimiento del Reglamento Interno de Orden, Higiene y Seguridad de  SERVICIOS DE RECURSOS HUMANOS Y LIMPIEZA SUBCO LIMITADA .</td></tr>
                                    <tr><td>9.	Obligación de Informar toda anomalía a la línea de mando (Reportes de accidentes / incidentes u Posible enfermedad Profesional).</td></tr>
                                    <tr><td>10.	Riesgos específicos en las labores diarias.</td></tr>
                                    <tr><td>11.	Cartilla de información de riesgos inherentes al trabajo.</td></tr>
                                    <tr><td>12.	Procedimiento a seguir en caso de emergencia.</td></tr>
                                    <tr><td>13.	Protección solar, Ley Nº  20.096 Radiación ultravioleta (Uso del protector solar).</td></tr>
                                    <tr><td>14.	Decreto Nº 594, condiciones ambientales básicas en los lugares de trabajo (Aspectos básicos).</td></tr>
                                    <tr><td>15.	Manipulación manual de carga y descarga (Ley Nº 20.001 art. 211/ 50 kg.)</td></tr>
                                    <tr><td>16.	Autocuidado.</td></tr>
                                    <tr><td>17.	Observaciones:</td>
                                </table>
                                <p style="float: left">
                                    <strong>
                                        FIRMA DEL TRABAJADOR<br>
                                        NOMBRE DEL TRABAJADOR <input type="text" id="nombre_t" value="{{$empleado->name}}"><br>
                                        RUT :<input type="text" id="rut_t" value="{{$empleado_obj->rut}}"><br>
                                        INSTALACION SUBSECRETARIA DE SALUD PUBLICA<br>
                                        CARGO AUXILIAR- COORDINADOR DE ASEO<br>
                                        INSTRUCCIÓN REALIZADA YENNY GUAJARDO<br>
                                        FECHA DE INSTRUCCIÓN: <input type="date" name="fecha_intruccion" placeholder="Fecha de intrucción" value="{{$contrato->fecha_intruccion}}"><br>
                                    </strong>
                                </p><br><br><br><br><br><br><br><br><br><br>
                                <p><center>EL SIGUIENTE CUADRO SUSCRIBE Y FIRMA CONFIRMA QUE FUE INSTRUIDO EN LOS TEMAS ANTES SEÑALADOS<br>
                                    CADA TRABAJADOR PUEDE CONTAR CON UNA COPIA DE ESTE DOCUMENTO DESPUES DE UNA SEMANA DICTADA LA CHARLA.
                                </center></p>
                                <p>De acuerdo a lo estipulado en la Ley 16.744, Art. 68 inciso tres "Las empresas deberán proporcionar a sus trabajadores, los equipos e implementos de protección necesarios, no pudiendo en caso alguno cobrarles su valor". 
                                    El trabajador se compromete a mantener los elementos de protección personal en buen estado y declara haberlos recibido en forma gratuita.
                                </p>

                                <table class="table-bordered">
                                    <tr>
                                        <td></td>
                                        <td><strong>ENTREGA DE ELEMENTOS DE
                                                PROTECCION PERSONAL
                                            </strong></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>   Resp: Gerencia, Supervisión,     
                                            Dpto. De Prev. De Riesgos
                                        </td>
                                        <td>DEPARTAMENTO DE PREVENCIÓN DE RIESGOS</td>
                                        <td>+ Seguridad,
                                            + Calidad de Vida
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Página 1 de 1</td>
                                        <td>For : SSO_11_ENT_EPP_V03</td>
                                        <td>SISTEMA DE GESTION DE SEGURIDAD 
                                            Y SALUD OCUPACIONAL VERSION 2.0
                                        </td>
                                    </tr>
                                </table>
                                <br>
                                <table class="table-bordered">
                                    <tr>
                                        <td>CANT</td>
                                        <td>ELEMENTO ENTREGADO 
                                            (PRODUCTO, MODELO, MARCA, ETC.) 
                                        </td>
                                        <td>CERTIFICADO
                                            (SI/NO)
                                        </td>
                                        <td>FECHA DE ENTREGA/
                                            DEVOLUCIÓN
                                        </td>
                                        <td>FIRMA RECIBÍ CONFORME</td>
                                    </tr>
                                    <tr style="height: 50px">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr style="height: 50px">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr style="height: 50px">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr style="height: 50px">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr style="height: 50px">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr style="height: 50px">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr style="height: 50px">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr style="height: 50px">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr style="height: 50px">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr style="height: 50px">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr style="height: 50px">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr style="height: 50px">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>
                                <br>
                                <br>
                                <div id="proteccion_personal">
                                    <table class="table-bordered" >
                                        <tr>
                                            <td>NOMBRE DEL TRABAJAODR</td>
                                            <td><input type="text" name="proteccion_nombre" id="proteccion_nombre" value="{{$empleado->name}}"></td>
                                            <td>INTALACIÓN</td>
                                            <td><input type="text" name="proteccion_intalacion" value="{{$contrato->proteccion_intalacion}}"></td>
                                        </tr>
                                        <tr>
                                            <td>RUT DEL TRABAJAODR</td>
                                            <td><input type="text" name="proteccion_rut_trabajador" id="proteccion_rut_trabajador" value="{{$empleado_obj->rut}}"></td>
                                            <td>ELEMENTO ENTREGADO POR (NOMBRE)</td>
                                            <td><input type="text" name="proteccion_entregado_nombre" value="{{$contrato->proteccion_entregado_nombre}}"></td>
                                        </tr>
                                        <tr>
                                            <td>CARGO DEL TRABAJAODR</td>
                                            <td><input type="text" name="proteccion_cargo_trabajador" value="{{$contrato->proteccion_cargo_trabajador}}"></td>
                                            <td>SUPERVISOR A CARGO</td>
                                            <td><input type="text" id="proteccion_supervisor" name="proteccion_supervisor" value="{{$contrato->proteccion_supervisor}}"></td>
                                        </tr>
                                        <tr>
                                            <td>FIRMA DEL TRABAJADOR</td>
                                            <td>_________________________</td>
                                            <td>FIRMA DEL SUPERVISOR</td>
                                            <td>_________________________</td>
                                        </tr>
                                    </table>
                                </div>   



                            </div>
                        </div>
                        <input type="reset" value="Cancelar" class="btn btn-outline-primary mb-2"> 
                       @if(Auth()->user()->role_id !== 7) <input type="submit" value="Modificar" class="btn btn-outline-secondary mb-2" >@endif
                    </form>
                    <button class="btn btn-success mb-2" id="imprimir_todo">Imprimir Todo</button>
                    <button class="btn btn-warning mb-2" id="imprimir_contrato">Imprimir Contrato</button>
                    <button class="btn btn-danger mb-2" id="imprimir_anticipo">Imprimir Anticipo</button>
                    <button class="btn btn-dark mb-2" id="imprimir_recibo">Imprimir Declaración de recibo</button>
                     <button class="btn btn-info mb-2" id="imprimir_temario">Imprimir Temarío Charla</button>



                </div>

            </div>

        </div>



        <script>
            document.getElementById("imprimir_todo").addEventListener("click", function (event) {
                event.preventDefault();
                var text = document.getElementById('todo').innerHTML;
                var ventana = window.open('', 'todo');
                ventana.document.write('<style>table { width: 100%;   border: 1px solid #000;}th, td {   width: 25%;   text-align: left;   vertical-align: top;   border: 1px solid #000;}input:focus {outline: 0; outline: none;}</style>');
                ventana.document.write(text);

                ventana.print();
            });
             document.getElementById("imprimir_contrato").addEventListener("click", function (event) {
                event.preventDefault();
                var text = document.getElementById('contrato_imprimir').innerHTML;
                var ventana = window.open('', 'contrato_imprimir');
                ventana.document.write('<style>table { width: 100%;   border: 1px solid #000;}th, td {   width: 25%;   text-align: left;   vertical-align: top;   border: 1px solid #000;}input:focus {outline: 0; outline: none;}</style>');
                ventana.document.write(text);

                ventana.print();
            });
                document.getElementById("imprimir_anticipo").addEventListener("click", function (event) {
                event.preventDefault();
                var text = document.getElementById('solicitud_anticipo').innerHTML;
                var ventana = window.open('', 'solicitud_anticipo');
                ventana.document.write('<style>table { width: 100%;   border: 1px solid #000;}th, td {   width: 25%;   text-align: left;   vertical-align: top;   border: 1px solid #000;}input:focus {outline: 0; outline: none;}</style>');
                ventana.document.write(text);

                ventana.print();
            });
             document.getElementById("imprimir_recibo").addEventListener("click", function (event) {
                event.preventDefault();
                var text = document.getElementById('solicitud_decraración_recibo').innerHTML;
                var ventana = window.open('', 'solicitud_decraración_recibo');
                ventana.document.write('<style>table { width: 100%;   border: 1px solid #000;}th, td {   width: 25%;   text-align: left;   vertical-align: top;   border: 1px solid #000;}input:focus {outline: 0; outline: none;}</style>');
                ventana.document.write(text);

                ventana.print();
            });
              document.getElementById("imprimir_temario").addEventListener("click", function (event) {
                event.preventDefault();
                var text = document.getElementById('temario_charla').innerHTML;
                var ventana = window.open('', 'temario_charla');
                ventana.document.write('<style>table { width: 100%;   border: 1px solid #000;}th, td {   width: 25%;   text-align: left;   vertical-align: top;   border: 1px solid #000;}input:focus {outline: 0; outline: none;}</style>');
                ventana.document.write(text);

                ventana.print();
            });
        </script>
        @endsection
        @section('codigos_especifico')

        <script>


            $(document).ready(function () {
                App.init();
            });
        </script>
        <script src="{{asset('plugins/select2/custom-select2.js')}}"></script>  

        @endsection

