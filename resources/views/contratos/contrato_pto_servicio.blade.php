@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <h2>Contrato de punto de Servicios</h2>
                    @if($contrato)
                    <form action="{{url('contratos/pto/edit/'.$contrato->id)}}" method="POST">

                        <div class="modal-body" >
                            <div id="para_imprimir">
                                <center><img src="{{asset('img\logo.png')}}" ></center>

                                {{csrf_field()}}
                                <h3> Contrato de Trabajo</h3>
                                <p>Por una parte, <input type="text" value="{{$contrato->sociedad}}" name="sociedad" placeholder="Sociedad" required >, sociedad identificada con rut <input type="text" name="rut_contratista" value="{{$contrato->rut_contratista}}" placeholder="RUT de la sociedad" required>, representada legalmente por <input type="text" value="{{$contrato->representante_legal}}" name="representante_legal" placeholder="Representante Legal" required>, mayor de edad, identificado(a) con cédula de ciudadanía n.° <input type="number" value="{{$contrato->cedula_contratista}}" name="cedula_contratista" required> expedida en la ciudad de Santiago, quien para efectos de este documento se denominará EL CONTRATISTA; y por otro lado, SERVICIOS DE RECURSOS HUMANOS Y LIMPIEZA SUBCO LIMITADA, sociedad legalmente constituida identificada con n° RUT 76.855.721-7, representada legalmente por <input type="text" value="{{$contrato->representante_empresa}}" name="representante_empresa" placeholder="Representante de la Empresa" required>, persona mayor de edad, identificado(a) con cédula de ciudadanía  Nº 14.159.428-1, expedida en la ciudad de Santiago de chile, quien en adelante se denominará EL CONTRATANTE, suscriben un CONTRATO DE PRESTACIÓN DE SERVICIOS GENERALES, el cual se regirá por las siguientes cláusulas:</p>
                                <p><strong>PRIMERA.</strong> OBJETO. EL CONTRATISTA prestará los servicios de aseo y limpieza en las dependencias del CONTRATANTE, las cuales son SERVICIOS DE RECURSOS HUMANOS Y LIMPIEZA SUBCO LIMITADA.</p>
                                <p><strong>SEGUNDA.</strong>PERSONAL. El CONTRATISTA tendrá su propio personal bajo su exclusiva subordinación y dependencia laboral, salarial y de seguridad social, que será personal idóneo para cumplir con el objeto de este contrato.</p>
                                <p><strong>TERCERA.</strong>HERRAMIENTAS DE TRABAJO. Tanto herramientas de trabajo como los productos de aseo <input type="text" name="tipo_producto" value="{{$contrato->tipo_producto}}" placeholder="Tipos de productos" required> estarán a cargo de SERVICIOS DE RECURSOS HUMANOS Y LIMPIEZA SUBCO LIMITADA.</p>
                                <p><strong>CUARTA.</strong>VALOR DEL SERVICIO DE ASEO. El CONTRATANTE cancelará al CONTRATISTA por el servicio de aseo y limpieza la suma de $ <input type="number" name="valor" value="{{$contrato->valor}}" placeholder="Monto del contrato" required> mensualmente, previa presentación de cuenta de cobro.</p>
                                <p><strong>Parágrafo.</strong>: cada año, y de mutuo acuerdo, se fijará el incremento del valor mensual del servicio. No llegar a un acuerdo será causal para la terminación de este contrato, sin que se genere ningún tipo de indemnización entre las partes.</p>
                                <p><strong>QUINTA.</strong>HORARIO Y LUGAR DE LA PRESTACIÓN DEL SERVICIO. El servicio se prestará en las Sucursales <input type="text" value="{{$contrato->sucursales}}" name="sucursales" placeholder="Sucursales involucradas" required>, en la ciudad de Santiago de chile, De lunes a jueves comprende entre las 08:00 hrs. a 17:00hrs, viernes comprende entre las 08:00 hrs. a 16:00hrs. claro está, que de mutuo acuerdo se puede modificar tanto el horario como la ubicación en la que se prestará el servicio de aseo.</p>
                                <p><strong>SEXTA.</strong>OBLIGACIONES DEL CONTRATISTA. EL CONTRATISTA se obliga a: 1. Ejecutar debidamente y bajo el principio de buena fe el servicio de aseo y limpieza que mediante este documento se contrata; 2. Tener a su personal con uniformes idóneos para las labores de aseo y limpieza, al igual que suministrarles los elementos necesarios para la ejecución de sus labores; 3. Mantener a su personal mientras se encuentre en las instalaciones del CONTRATANTE, debidamente identificados y portando siempre su carnet de ARP; 4. Cambiar el personal que tiene designado en las instalaciones del CONTRATANTE, cuando este se lo solicite por encontrarse inconforme, bien sea por una irregular ejecución de la labor o por conductas inadecuadas del personal enviado por parte del CONTRATISTA; 5. Atender las recomendaciones que el CONTRATANTE le haga respecto al servicio desarrollado y las labores de su personal; 6. Informar por escrito y mínimo con un día de antelación el cambio de personal que realizará las labores en las dependencias del CONTRATANTE; 7. Cumplir todos los meses con sus responsabilidades salariales, prestacionales y de seguridad social con sus empleados, por lo que es facultad del CONTRATANTE exigir las constancias de pago a seguridad social, so pena de suspender el pago de cuentas de cobro adeudadas hasta que no demuestre estar a paz y salvo por este concepto.</p>
                                <p><strong>SÉPTIMA.</strong>OBLIGACIONES DEL CONTRATANTE. EL CONTRATANTE se obliga a: 1. Informar al CONTRATISTA inmediatamente tenga conocimiento de cualquier irregularidad con la ejecución de este contrato; 2. Pagar dentro de los cinco (5) días hábiles siguientes a la presentación de la cuenta de cobro y el soporte de pago de la seguridad social de sus empleados por parte del CONTRATISTA. </p>
                                <p><strong>OCTAVA.</strong>DURACIÓN DEL SERVICIO. Este contrato es por un año, pero cualquiera de las partes lo puede dar por terminado por incumplimiento de las obligaciones establecidas para las partes, avisando a la otra con una antelación no menor a diez (10) días.</p>
                                <p><strong>NOVENA.</strong>MODIFICACIÓN O PRÓRROGA. Cualquier modificación o prórroga a este contrato deberá hacerse por escrito.</p>
                                <p><strong>DÉCIMA.</strong>TRIBUNAL DE ARBITRAMENTO. En caso de conflicto entre las partes de este contrato de prestación de servicios generales, relativa a este contrato, su ejecución y liquidación, deberá agotarse una diligencia de conciliación ante cualquier entidad autorizada para efectuarla; si esta fracasa, se llevarán las diferencias ante un tribunal de arbitramento del domicilio del CONTRATANTE, el cual será pagado por partes iguales</p>
                                <p>Se firma en Santiago de Chile, a los <input type="number"  value="{{$contrato->dia}}" name="dia" placeholder="Dia" required> del mes <input type="number" value="{{$contrato->mes}}" name="mes" placeholder="mes" required > de <input type="number" value="{{$contrato->ano}}" name="ano" placeholder="Año" required></p>
                                <br><br><br><br><br><br><br><br><br>
                                <div style="float: left">
                                    <p>YENNY ALEJANDRA GUAJARDO GONZALEZ</p>
                                    <p>Rut 14.159.428-1</p>
                                    <p>Rut 76.855.721-7</p>
                                    <p>SERVICIOS DE RECURSOS HUMANOS Y LIMPIEZA SUBCO LIMITADA</p>

                                </div>
                                <div style="float: right">
                                    <p><input type="text" name="nombrle_completo_contratista" value="{{$contrato->nombrle_completo_contratista}}" placeholder="Nombre del Contratista" required></p>
                                    <p><input type="text" name="no_id_contratista" value="{{$contrato->no_id_contratista}}" placeholder="Id del contratista" required></p>
                                    <p><input type="text" name="rut_empresa_contratista"  value="{{$contrato->rut_empresa_contratista}}" placeholder="Rut empresa contratista" required></p>
                                    <p><input type="text" name="nombre_empresa_contratista"  value="{{$contrato->nombre_empresa_contratista}}" placeholder="Nombre de empresa contratista" required></p>

                                </div>
                                <br>
                                <br><br><br><br><br><br><br><br>
                            </div>
                        </div>
                        <input type="reset" value="Cancelar" class="btn btn-outline-primary mb-2" >
                       @if(Auth()->user()->role_id !== 5) <input type="submit" value="Modificar" class="btn btn-primary mb-2" >@endif
                        <button id="imp" onclick="imprimir()" class="btn btn-success mb-2">Imprimir</button>
                        @else
                        <h3> No se encuentra contrato para este punto de servicio en el sistema</h3>
@endif
                    </form>
                </div>
            </div>

        </div>

    </div>


    <script>


    </script>


    @endsection
    @section('codigos_especifico')

    <script>


        $(document).ready(function () {
        App.init();
                $('#imp').on('click', function(event) {
        event.preventDefault();
                var text = document.getElementById('para_imprimir').innerHTML;
                var ventana = window.open('', 'temario_charla');
                ventana.document.write(text);
                ventana.print();
                })
                @if (Session::has('sin_contrato'))

                mostrar_notificacion("{{session('sin_contrato')}}");
                @endif


        });
    </script>
    <script src="{{asset('plugins/select2/custom-select2.js')}}"></script>  

    @endsection


