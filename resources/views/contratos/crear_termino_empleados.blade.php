@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <h2>Crear Contrato de empleado</h2>
                    Empleado:
                    <form action="{{url('contratos/empleado/termino/crear')}}" method="POST">
                        @if($empleados !== 'NO')
                        <select  id="empleados_select" name="empleados_select" class="form-control  basic" onchange="empleado()" required>
                            <option value="" selected="selected" disabled="">Seleccione un empleado</option>
                            @foreach($empleados as $e)
                            <option  value="{{$e->id}}" >{{$e->name}}</option>
                            @endforeach
                        </select>
                        <script>
                            var ss = $(".basic").select2({
                                tags: true,
                            });
                            function empleado() {
                                var select = document.getElementById('empleados_select');
                                var elemento = select.options[select.selectedIndex].text;
                                document.getElementById('nombre_empleado').value = elemento;
                                document.getElementById('nombre_empleado_').value = elemento;
                                document.getElementById('f_nombre_trabajador').value = elemento;
                                document.getElementById('f_nombre_empleado_').value = elemento;
                                document.getElementById('f_nombre').innerHTML = elemento;
                            }
                            function rut_() {
                                document.getElementById('rut_empleado_').value = document.getElementById('rut_empleado').value;
                                document.getElementById('f_rut_e').value = document.getElementById('rut_empleado').value;
                                document.getElementById('f_rut_empleado_').value = document.getElementById('rut_empleado').value;
                            }
                            function dia() {
                                document.getElementById('fecha_dia_').value = document.getElementById('fecha_dia').value;
                                document.getElementById('f_fecha_dia').value = document.getElementById('fecha_dia').value;
                            }
                            function ano() {
                                document.getElementById('fecha_ano_').value = document.getElementById('fecha_ano').value;
                                document.getElementById('f_fecha_ano').value = document.getElementById('fecha_ano').value;
                            }
                        </script>
                        <div class="modal-body" id="imprimir">
                            <center><img src="{{asset('img\logo.png')}}" ></center>
                            {{csrf_field()}}
                            <center>  <h3>Aviso de Término de Contrato</h3></center>
                            <p style="float: right">Santiago, <input type="number" onchange="dia()" onkeyup="dia()" name="fecha_dia" id="fecha_dia" style="width: 50px" required> de
                                <select name="fecha_mes" id="fecha_mes" >
                                    <option value="Enero">Enero</option>
                                    <option value="Febrero">Febrero</option>
                                    <option value="Marzo">Marzo</option>
                                    <option value="Abril">Abril</option>
                                    <option value="Mayo">Mayo</option>
                                    <option value="Junio">Junio</option>
                                    <option value="Julio">Julio</option>
                                    <option value="Agosto">Agosto</option>
                                    <option value="Octubre">Septiembre</option>
                                    <option value="">Octubre</option>
                                    <option value="Novienbre">Novienbre</option>
                                    <option value="Diciembre">Diciembre</option>
                                </select> del año <input type="number" onchange="ano()" onkeyup="ano()" name="fecha_ano" id="fecha_ano" style="width: 70px" required></p>
                            <h5> SEÑOR(A):<input type="text" name="nombre_empleado" id="nombre_empleado" style="width:250px" required></h5>
                            <h5> RUT: <input type="text" onkeyup="rut_()" id="rut_empleado" name="rut_empleado" placeholder="Rut del empleado" required></h5>
                            <p>Estimado señor(a):</p>    
                            <p>Nos permitimos comunicar que, con esta fecha <input type="number" name="fecha_dia_" id="fecha_dia_" style="width: 50px" required> de <select name="fecha_mes" id="fecha_mes" required >
                                    <option value="Enero">Enero</option>
                                    <option value="Febrero">Febrero</option>
                                    <option value="Marzo">Marzo</option>
                                    <option value="Abril">Abril</option>
                                    <option value="Mayo">Mayo</option>
                                    <option value="Junio">Junio</option>
                                    <option value="Julio">Julio</option>
                                    <option value="Agosto">Agosto</option>
                                    <option value="Octubre">Septiembre</option>
                                    <option value="">Octubre</option>
                                    <option value="Novienbre">Novienbre</option>
                                    <option value="Diciembre">Diciembre</option>
                                </select> del año  <input type="number" name="fecha_ano_" id="fecha_ano_" style="width: 70px" required> se ha resuelto poner término al contrato de trabajo que lo vincula con la empresa desde el <input type="date" name="fecha_vinculo_contrato"  required>, por la causal del artículo 160, número 3, del Código del Trabajo, esto es, No concurrencia del trabajador a sus labores sin causa justificada.
                                Los hechos en que se funda la causal invocada consisten en que: Usted falto a su lugar de trabajo los días <input type="text" name="ausencias" style="width: 200px" placeholder="Dias de Ausencia" required>, sin justificar dichas ausencias.
                                Informo que sus cotizaciones previsionales se encuentran al día. Además, le adjuntamos certificado de cotizaciones (o copia de las planillas de declaración y pago simultáneo) de las entidades de previsión a las que se encuentra afiliado, que dan cuenta que las cotizaciones previsionales, del período trabajado, se encuentran pagadas.
                            </p>
                            <br>
                            <p>Saluda a usted,</p>
                            <br><br><br><br><br><br><br><br><br><br>
                            _____________________________________<br>
                            <input type="text" name="nombre_empleado" id="nombre_empleado_" style="width:250px" required><br>
                            <input type="text" name="rut_empleado" id="rut_empleado_" placeholder="Rut del Empleado" required>
                            <br><br><br><br>
                            ________________________________________<br>
                            <p>SERVICIOS DE RECURSOS HUMANOS Y LIMPIEZA LIMITADA</p>
                            <p>RUT 76.855.721-7</p>
                        </div>   
                        <!----- Fin de aviso inicio finiquito   -->
                        <div class="modal-body" id="f_imprimir">

                            <center><img src="{{asset('img\logo.png')}}" ></center>

                            <center>  <h3>Finiquito de Trabajo</h3></center>
                            <p > En Santiago, a <input type="number" name="f_fecha_dia" id="f_fecha_dia" style="width: 50px" required> de
                                <select name="f_fecha_mes" id="f_fecha_mes"  onchange="mes()">
                                    <option value="Enero">Enero</option>
                                    <option value="Febrero">Febrero</option>
                                    <option value="Marzo">Marzo</option>
                                    <option value="Abril">Abril</option>
                                    <option value="Mayo">Mayo</option>
                                    <option value="Junio">Junio</option>
                                    <option value="Julio">Julio</option>
                                    <option value="Agosto">Agosto</option>
                                    <option value="Octubre">Septiembre</option>
                                    <option value="">Octubre</option>
                                    <option value="Novienbre">Novienbre</option>
                                    <option value="Diciembre">Diciembre</option>
                                </select> del año <input type="number" name="f_fecha_ano" id="f_fecha_ano" style="width: 70px" required>
                                entre SERVICIOS DE RECURSOS HUMANOS Y LIMPIEZA “SUBCO” LIMITADA,  Rut 76.855.721-7, en adelante, también, representado por Don.(a) Yenny Alejandra Guajardo Gonzalez  R.U.T 14.159.428-1 ambos domiciliados en Huerfanos 1055 oficina 505 comuna de Santiago Centro , ciudad de  Santiago , por una parte; y la otra, don(a) <input type="text" id="f_nombre_trabajador" name="f_nombre_trabajador" required="">  , R.U.T <input type="text" id="f_rut_e" onkeydown="rut();" name="f_rut_trabajador" required=""> domiciliado en <input type="text" name="f_direccion_trabajador" style="width: 400px" required>  , en adelante, también, «el trabajador(a)», se deja testimonio y se ha acordado el finiquito que consta de las siguientes cláusulas:
                            </p>
                            <p>PRIMERO: El trabajador prestó servicios al empleador desde el <input type="date" name="f_fecha_desde" required=""> hasta <input type="date" name="f_fecha_hasta" required=""e>, fecha esta última en que su contrato de trabajo ha terminado No concurrencia del trabajador a sus labores sin causa justificada. causal(es) señalada(s) en el Código del Trabajo, artículo(s) 160- numero 3</p>
                            <p>SEGUNDO: Don(a) <span id="f_nombre"></span>  declara recibir en este acto, a su entera satisfacción, de parte de SERVICIOS DE RECURSOS HUMANOS Y LIMPIEZA “SUBCO” LIMITADA la suma señalada a continuación:</p>
                            <p>Total a recibir en el mes de <span id="f_mes"></span>:       <input type="number" name="f_total" required=""></p>
                            <p>Total a recibir:       <input type="number" name="f_total_mes" required=""></p>
                            <br><br><br><br><br><br><br><br><br><br>
                            _____________________________________<br>
                            <input type="text"  id="f_nombre_empleado_" style="width:250px" required><br>
                            <input type="text"  id="f_rut_empleado_" placeholder="Rut del Empleado" required>
                            <br><br><br><br>
                            ________________________________________<br>
                            <p>SERVICIOS DE RECURSOS HUMANOS Y LIMPIEZA LIMITADA</p>
                            <p>RUT 76.855.721-7</p>



                        </div>   
                </div>
                <input type="reset" value="Cancelar" class="btn btn-outline-primary mb-2" >
                <input type="submit" value="Guardar" class="btn  btn-primary mb-2" >
                </form>
            </div>
        </div>
    </div>
</div>
@else
<h2> No existen empleados con contrato actualmente, por lo que no puede cerrar ninguno </h2>
@endif
@endsection
@section('codigos_especifico')
<script>
    $(document).ready(function () {
        App.init();
                @if (Session::has('sin_contrato'))

        mostrar_notificacion("{{session('mensaje')}}");
        @endif
    });</script>
<script src="{{asset('plugins/select2/custom-select2.js')}}"></script>  
@endsection
