@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <h2>Listado de Avisos de Termino de Contrato</h2>

                    <div class="table-responsive mb-4 mt-4">
                        <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Nombre</th>
                                    <th>RUT</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>


    <div class="modal fade" id="eliminarModal" tabindex="-1" role="dialog" aria-labelledby="eliminarModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Eliminar  Punto de Servicio</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="modal-text">Seguro que desea eliminar la constancia de Termino de Contrato de :</p>
                    <form>
                        <input type="number" id="id_producto_eliminar" style="display: none">
                            <input type="number" id="id_punto_servicio" style="display: none">

                                <h2 id="nombre_producto"> </h2>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancelar</button>
                                    <button id="btn_eliminar" type="button" class="btn btn-primary">Eliminar</button>
                                </div>
                                </div>
                                </div>
                                </div>


                                @endsection
                                @section('codigos_especifico')
                                <script>



                                    $('#html5-extension').DataTable({
                                        dom: '<"row"<"col-md-12"<"row"<"col-md-6"l><"col-md-6"f> > >B<"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >',
                                        buttons: {
                                            buttons: [
                                                {extend: 'copy', className: 'btn'},
                                                {extend: 'csv', className: 'btn'},
                                                {extend: 'excel', className: 'btn'},
                                                {extend: 'print', className: 'btn'}
                                            ]
                                        },
                                        "oLanguage": {
                                            "oPaginate": {"sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'},
                                            "sInfo": "Mostrada pagina _PAGE_ de _PAGES_",
                                            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                                            "sSearchPlaceholder": "Buscar",
                                            "sLengthMenu": "Results :  _MENU_",
                                        },
                                        "columnDefs": [
                                            {
                                                "targets": -1,
                                                "data": null,
                                                "searchable": false,
                                                "defaultContent": '@if(Auth()->user()->role_id !== 7)<a id="eliminar"  title="Eliminar"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg></a>  <a id="ver_termino" title="Ver constancia de termino de contrato"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file"><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 9"></polyline></svg></a>@endif'


                                            },
                                            {
                                                "targets": [0],
                                                "visible": false,
                                                "data": 'id'
                                            },
                                            {
                                                "targets": [1],
                                                "visible": true,
                                                "data": 'nombre_empleado'
                                            },

                                            {
                                                "targets": [2],
                                                "data": 'rut_empleado'
                                            }
                                        ],
                                        "stripeClasses": [],
                                        "lengthMenu": [10, 20, 50, 100, 1000000],
                                        "pageLength": 10,
                                        "processing": true,
                                        "serverSide": true,
                                        "ajax": "{{url('/contratos/empleado/termino_json')}}"
                                    });
                                    $(document).ready(function () {

                                        App.init();
                                                @if (Session::has('insertar_usuario'))

                                        mostrar_notificacion("{{session('insertar_usuario')}}");

                                        @endif

                                                var table = $('#html5-extension').DataTable();
                                        $('#html5-extension tbody').on('click', '#eliminar', function () {
                                            var data = table.row($(this).parents('tr')).data();
                                            document.getElementById('id_producto_eliminar').value = data['id'];
                                            document.getElementById('nombre_producto').innerHTML = data['nombre_empleado'];
                                            $('#eliminarModal').modal('show');
                                        });

                                        $('#html5-extension tbody').on('click', '#ver_termino', function () {
                                            var data = table.row($(this).parents('tr')).data();

                                            location.href = 'termino/detalles/' + data['id'];
                                        });
                                        $('#eliminarModal').on('click', '#btn_eliminar', function (e) {

                                            e.preventDefault();
                                            var consulta = new XMLHttpRequest();
                                            if (!window.XMLHttpRequest) {
                                                consulta = new ActiveXObject("Microsoft.XMLHTTP");
                                            }
                                            consulta.onreadystatechange = function () {
                                                if (consulta.readyState === 4 && consulta.status === 200) {

                                                    var tabla = $("#html5-extension").DataTable();
                                                    tabla.ajax.reload();
                                                    mostrar_notificacion(consulta.responseText);
                                                    $('#eliminarModal').modal('hide');
                                                }

                                            };
                                            consulta.open("DELETE", 'termino/eliminar/' + document.getElementById('id_producto_eliminar').value, true);
                                            consulta.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                                            // consulta.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                            consulta.send();
                                        });

                                    }
                                    );

                                </script>

                                @endsection

