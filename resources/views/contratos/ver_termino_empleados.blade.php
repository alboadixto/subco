@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <h2>Constancia de Termino de Contrato : {{$termino->nombre_empleado}}</h2>
                    <div class="modal-body" id="imprimir">
                        <center><img src="{{asset('img\logo.png')}}" ></center>
                        {{csrf_field()}}
                        <center>  <h3>Aviso de Término de Contrato</h3></center>
                        <p style="float: right">Santiago, {{$termino->fecha_dia}} de
                            {{$termino->fecha_mes}} del año {{$termino->fecha_ano}}</p>
                        <h5> SEÑOR(A):{{$termino->nombre_empleado}}</h5>
                        <h5> RUT: {{$termino->rut_empleado}}</h5>
                        <p>Estimado señor(a):</p>    
                        <p>Nos permitimos comunicar que, con esta fecha {{$termino->fecha_dia}} de  {{$termino->fecha_mes}} del año  {{$termino->fecha_ano}} se ha resuelto poner término al contrato de trabajo que lo vincula con la empresa desde el {{$termino->fecha_vinculo_contrato}}, por la causal del artículo 160, número 3, del Código del Trabajo, esto es, No concurrencia del trabajador a sus labores sin causa justificada.
                            Los hechos en que se funda la causal invocada consisten en que: Usted falto a su lugar de trabajo los días {{$termino->ausencias}}, sin justificar dichas ausencias.
                            Informo que sus cotizaciones previsionales se encuentran al día. Además, le adjuntamos certificado de cotizaciones (o copia de las planillas de declaración y pago simultáneo) de las entidades de previsión a las que se encuentra afiliado, que dan cuenta que las cotizaciones previsionales, del período trabajado, se encuentran pagadas.
                        </p>
                        <br>
                        <p>Saluda a usted,</p>
                        <br><br><br><br><br><br><br><br><br><br>
                        _____________________________________<br>
                        {{$termino->nombre_empleado}}<br>
                        {{$termino->rut_empleado}}
                        <br><br><br><br>
                        ________________________________________<br>
                        <p>SERVICIOS DE RECURSOS HUMANOS Y LIMPIEZA LIMITADA</p>
                        <p>RUT 76.855.721-7</p>
                    </div>   
                    <div class="modal-body" id="f_imprimir">
                        <center><img src="{{asset('img\logo.png')}}" ></center>
                        <center>  <h3>Finiquito de Trabajo</h3></center>
                        <p > En Santiago, a {{$finito->fecha_dia}} de
                            {{$finito->fecha_mes}} del año {{$finito->f_fecha_ano}}
                            entre SERVICIOS DE RECURSOS HUMANOS Y LIMPIEZA “SUBCO” LIMITADA,  Rut 76.855.721-7, en adelante, también, representado por Don.(a) Yenny Alejandra Guajardo Gonzalez  R.U.T 14.159.428-1 ambos domiciliados en Huerfanos 1055 oficina 505 comuna de Santiago Centro , ciudad de  Santiago , por una parte; y la otra, don(a) {{$finito->nombre_trabajador}}  , R.U.T {{$finito->rut_trabajador}} domiciliado en {{$finito->direccion_trabajador}}  , en adelante, también, «el trabajador(a)», se deja testimonio y se ha acordado el finiquito que consta de las siguientes cláusulas:
                        </p>
                        <p>PRIMERO: El trabajador prestó servicios al empleador desde el {{$finito->fecha_desde}} hasta {{$finito->fecha_hasta}}, fecha esta última en que su contrato de trabajo ha terminado No concurrencia del trabajador a sus labores sin causa justificada. causal(es) señalada(s) en el Código del Trabajo, artículo(s) 160- numero 3</p>
                        <p>SEGUNDO: Don(a) {{$finito->nombre_trabajador}}  declara recibir en este acto, a su entera satisfacción, de parte de SERVICIOS DE RECURSOS HUMANOS Y LIMPIEZA “SUBCO” LIMITADA la suma señalada a continuación:</p>
                        <p>Total a recibir en el mes de {{$finito->fecha_mes}}:      {{$finito->total_mes}}</p>
                        <p>Total a recibir:       {{$finito->total}}</p>
                        <br><br><br><br><br><br><br><br><br><br>
                        _____________________________________<br>
                        {{$finito->nombre_trabajador}}<br>
                        {{$finito->rut_trabajador}}
                        <br><br><br><br>
                        _______________________________________<br>
                        <p>SERVICIOS DE RECURSOS HUMANOS Y LIMPIEZA LIMITADA</p>
                        <p>RUT 76.855.721-7</p>
                    </div>   
                    <script>
                        function imprimir() {
                            var text = document.getElementById('imprimir').innerHTML;
                            var ventana = window.open('', 'todo');
                            ventana.document.write(text);
                            ventana.print();

                        }
                        function f_imprimir() {
                            var text = document.getElementById('f_imprimir').innerHTML;
                            var ventana = window.open('', 'todo');
                            ventana.document.write(text);
                            ventana.print();
                        }
                    </script>
                </div>
                <button class="btn btn-primary mb-2" onclick="imprimir()">Imprimir Aviso de Término de Contrato</button>
                <button class="btn btn-primary mb-2" onclick="f_imprimir()">Imprimir Finiquito de Trabajo</button>
                <a href="{{url("contratos/empleado/mostar_terminos")}}"><button  class="btn btn-outline-primary mb-2">Cancelar</button></a>
            </div>
        </div>
    </div>
</div>
@endsection
@section('codigos_especifico')
<script>
    $(document).ready(function () {
    App.init();
            @if (Session::has('sin_contrato'))

            mostrar_notificacion("{{session('mensaje')}}");
            @endif
    });</script>
<script src="{{asset('plugins/select2/custom-select2.js')}}"></script>  
@endsection

