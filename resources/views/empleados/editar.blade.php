@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <h2>Editar  Factura</h2>
                    <div class="modal-body">
                        <form  method="POST" action="{{route('facturas.update',$factura->id)}}">
                            <div class="form-group row  mb-4">
                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" >Folio</label>
                                <div class="col-sm-10">
                                    <input value="@if($factura ?? '') {{$factura->folio}} @endif" type="text" name="folio" class="form-control form-control-lg" id="colFormLabelSm" placeholder="Introduzca el Folio" disabled="disabled">
                                    <input type="hidden" name="_method" value="PUT">
                                </div>
                            </div>
                            <div class="form-group row  mb-4">
                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Fecha</label>
                                <div class="col-sm-10">
                                    <input value="@if($factura ?? ''){{$factura->fecha}}@endif" type="date" name="fecha" class="form-control form-control-lg" id="colFormLabelSm" required>
                                </div>
                            </div>
                            {{csrf_field()}}
                          
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Proveedor</label>
                                <div class="col-sm-10">
                                    <select id="proveedor" name="proveedor" class="form-control form-small">
                                       
                                       @if($proveedor ?? '')
                                       @foreach($proveedor ?? ''  as $p)
                                       <option value="{{$p->id}}" @if($p->id == $factura->proveedor) selected @endif>{{$p->name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                    <script>
                                        var formSmall = $(".form-small").select2({tags: true});
                                        formSmall.data('select2').$container.addClass('form-control-sm');</script>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Observaciones</label>
                                <div class="col-sm-10">
                                    <textarea  name="observaciones" class="form-control mb-4" rows="3" id="descripcion">@if($factura ?? ''){{$factura->observaciones}}@endif</textarea>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Valor neto</label>
                                <div class="col-sm-10">
                                    <input value="@if($factura ?? ''){{$factura->valor_neto}}@endif" type="number" name="valor_neto" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca el valor del monto" required>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Estado de Pago</label>
                                <div class="col-sm-10">
                                    <select id="estado" name="estado" class="form-control form-small" required="required">
                                        
                                        <option value="Emitida" @if($factura->estado_pago == 'Emitida') selected @endif>Emitida</option>
                                        <option value="Registrada" @if($factura->estado_pago == 'Registrada') selected @endif>Registrada</option>
                                        <option value="Solicitada Anulación" @if($factura->estado_pago == 'Solicitada Anulación') selected @endif>Solicitada Anulación</option>
                                        <option value="Anulación Aceptada" @if($factura->estado_pago == 'Anulación Aceptada') selected @endif>Anulación Aceptada</option>
                                        <option value="Rechazada" @if($factura->estado_pago == 'Rechazada') selected @endif>Rechazada</option>
                                        <option value="Reenviada" @if($factura->estado_pago == 'Reenviada') selected @endif>Reenviada</option>
                                        <option value="Aceptada" @if($factura->estado_pago == 'Aceptada') selected @endif>Aceptada</option>
                                        <option value="Cobrada" @if($factura->estado_pago == 'Cobrada') selected @endif>Cobrada</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Tipo de Operación</label>
                                <div class="col-sm-10">
                                    <select id="operacion" name="operacion" class="form-control form-small" required="required">
                                        <option disabled="disabled" selected="selected">Seleccione la operación</option>
                                        <option value="Compra" @if($factura->operacion == 'Compra') selected @endif>Compra</option>
                                        <option value="Venta" @if($factura->operacion == 'Venta') selected @endif>Venta</option>
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Tipo de Productos</label>
                                <div class="col-sm-10">
                                    
                                       <select id="tipo_producto" name="tipo_producto" class="form-control form-small" required="required">
                                        <option value="Maquinaria" @if($factura->tipo_producto == 'Maquinaria' ) selected @endif>Maquinaria</option>
                                        <option value="Insumos" @if($factura->tipo_producto == 'Insumos' ) selected @endif>Insumos</option>
                                        
                                    </select>
                                </div>
                            </div>
                            <!-- Inputs especificos de clientes -->
                             <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Tipo de Transacción</label>
                                <div class="col-sm-10">
                                    <select id="transaccion" name="transaccion" class="form-control form-small" required="required">
                                        
                                        <option value="Paypal" @if($factura->tipo_transaccion == 'Paypal' ) selected @endif>Paypal</option>
                                        <option value="Debito" @if($factura->tipo_transaccion == 'Debito' ) selected @endif>Debito</option>
                                        <option value="Transferencia Bancaría" @if($factura->tipo_transaccion == 'Transferencia Bancaría' ) selected @endif>Transferencia Bancaría</option>
                                        <option value="Pagos a través del móvil" @if($factura->tipo_transaccion == 'Pagos a través del móvil' ) selected @endif>Pagos a través del móvil</option>
                                        <option value="Bancos online" @if($factura->tipo_transaccion == 'Bancos online' ) selected @endif>Bancos Online</option>
                                    </select>
                                </div>
                            </div>
                            <div id="input_rubro" class="form-group row mb-4" >
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Fecha de pago</label>
                                <div class="col-sm-10">
                                    <input value="@if($factura ?? ''){{$factura->fecha_pago}}@endif" type="date" name="fecha_pago" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca el rubro" >
                                </div>
                            </div>
                           
                          
                            
                            <input type="submit" class="mb-4 btn btn-primary" value="Modificar" >
                            <a href="{{url('listar_facturas')}}"> <input type="button" name="time" class="mb-4 btn btn-success" value="Cancelar" ></a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
    @endsection
    @section('codigos_especifico')
    <script>
        $(document).ready(function () {
            App.init();

          @if (Session::has('insertar_factura'))

                mostrar_notificacion("{{session('insertar_factura')}}");
      
        @endif



        }
        );
    </script>
    @endsection


