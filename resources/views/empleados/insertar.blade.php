@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <h2>Insertar  Factura</h2>
                    <div class="modal-body">
                        <form action="{{url('facturas')}}" method="POST">
                            <div class="form-group row  mb-4">
                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Folio</label>
                                <div class="col-sm-10">
                                    <input type="text" name="folio" class="form-control form-control-lg" id="colFormLabelSm" placeholder="Introduzca el Folio" required>
                                </div>
                            </div>
                            <div class="form-group row  mb-4">
                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Fecha</label>
                                <div class="col-sm-10">
                                    <input type="date" name="fecha" class="form-control form-control-lg" id="colFormLabelSm" required>
                                </div>
                            </div>
                            {{ csrf_field()}}
                          
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Proveedor</label>
                                <div class="col-sm-10">
                                    <select id="proveedor" name="proveedor" class="form-control form-small">
                                       <option selected="selected" disabled="disabled">Seleccionar Proveedor</option>
                                       @if($proveedor ?? '')
                                       @foreach($proveedor ?? ''  as $p)
                                       <option value="{{$p->id}}">{{$p->name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                    <script>
                                        var formSmall = $(".form-small").select2({tags: true});
                                        formSmall.data('select2').$container.addClass('form-control-sm');</script>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Observaciones</label>
                                <div class="col-sm-10">
                                    <textarea name="observaciones" class="form-control mb-4" rows="3" id="descripcion"></textarea>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Valor neto</label>
                                <div class="col-sm-10">
                                    <input type="number" name="valor_neto" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca el valor del monto" required>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Estado de Pago</label>
                                <div class="col-sm-10">
                                    <select id="estado" name="estado" class="form-control form-small" required="required">
                                        <option disabled="disabled" selected="selected">Seleccione el estado de la factura</option>
                                        <option value="Emitida">Emitida</option>
                                        <option value="Registrada">Registrada</option>
                                        <option value="Solicitada Anulación">Solicitada Anulación</option>
                                        <option value="Anulación Aceptada">Anulación Aceptada</option>
                                        <option value="Rechazada">Rechazada</option>
                                        <option value="Reenviada">Reenviada</option>
                                        <option value="Aceptada">Aceptada</option>
                                        <option value="Cobrada">Cobrada</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Tipo de Operación</label>
                                <div class="col-sm-10">
                                    <select id="operacion" name="operacion" class="form-control form-small" required="required">
                                        <option disabled="disabled" selected="selected">Seleccione la operación</option>
                                        <option value="Compra">Compra</option>
                                        <option value="Venta">Venta</option>
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Tipo de Productos</label>
                                <div class="col-sm-10">
                                    <select id="tipo_producto" name="tipo_producto" class="form-control form-small" required="required">
                                        <option disabled="disabled" selected="selected">Seleccione la operación</option>
                                        <option value="Maquinaria">Maquinaria</option>
                                        <option value="Insumos">Insumos</option>
                                        
                                    </select>
                                </div>
                            </div>
                            <!-- Inputs especificos de clientes -->
                             <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Tipo de Transacción</label>
                                <div class="col-sm-10">
                                    <select id="transaccion" name="transaccion" class="form-control form-small" required="required">
                                        <option disabled="disabled" selected="selected">Seleccione tipo de transacción</option>
                                        <option value="Paypal">Paypal</option>
                                        <option value="Debito">Debito</option>
                                        <option value="Transferencia Bancaría">Transferencia Bancaría</option>
                                        <option value="Pagos a través del móvil">Pagos a través del móvil</option>
                                        <option value="Bancos online">Bancos Online</option>
                                    </select>
                                </div>
                            </div>
                            <div id="input_rubro" class="form-group row mb-4" >
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Fecha de pago</label>
                                <div class="col-sm-10">
                                    <input type="date" name="fecha_pago" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca el rubro" >
                                </div>
                            </div>
                           
                          
                            
                            <input type="submit" name="time" class="mb-4 btn btn-primary" value="Agregar" >
                            <a href="{{url('listar_facturas')}}"> <input type="button" name="time" class="mb-4 btn btn-success" value="Cancelar" ></a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
    @endsection
    @section('codigos_especifico')
    <script>
        $(document).ready(function () {
            App.init();

          @if (Session::has('insertar_factura'))

                mostrar_notificacion("{{session('insertar_factura')}}");
      
        @endif



        }
        );
    </script>
    @endsection


