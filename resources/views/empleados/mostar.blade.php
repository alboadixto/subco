@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <h2>Listado de Empleados</h2>
                  @if(Auth()->user()->role_id !== 5 && Auth()->user()->role_id !== 7)  <a href="{{url('usuarios/create')}}"><button class="btn-success btn" style="float: right;margin-right: 25px">Insertar Usuario</button></a>@endif
                    <div class="table-responsive mb-4 mt-4">
                        <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Nombre</th>
                                    <th>Cargo</th>
                                    <th>Dirección</th>
                                    <th>Teléfono</th>
                                    <th>Correo</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>


    <div class="modal fade" id="nominaModal" tabindex="-1" role="dialog" aria-labelledby="nominaModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nomina</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="max-height: 20px">
                        <svg> ... </svg>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="modal-text">Mauris mi tellus, pharetra vel mattis sed, tempus ultrices eros. Phasellus egestas sit amet velit sed luctus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse potenti. Vivamus ultrices sed urna ac pulvinar. Ut sit amet ullamcorper mi. </p>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancelar</button>
                    <button type="button" class="btn btn-primary">Ver</button>
                </div>
            </div>
        </div>
    </div>



    <div class="modal fade" id="editarModal" tabindex="-1" role="dialog" aria-labelledby="editarModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modificar Usuario</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group row  mb-4">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Nombre: </label>
                        <div class="col-sm-10">
                            <input id="i_id" type="nmber" name="id" class="form-control form-control-sm" id="colFormLabelSm" placeholder="col-form-label-sm" style="display: none">
                            <input id="i_nombre" type="text" name="nombre" class="form-control form-control-lg" id="colFormLabelSm" placeholder="Introduzca el nombre">
                        </div>
                    </div>
                    {{csrf_field()}}
                    <div class="form-group row mb-4">
                        <label for="colFormLabel" class="col-sm-2 col-form-label">Cargo</label>
                        <div class="col-sm-10">
                            <input id="i_cargo" type="text" name="cargo" class="form-control form-control-lg" id="colFormLabel" placeholder="Introduzca el cargo">
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Dirección</label>
                        <div class="col-sm-10">
                            <input id="i_direccion" type="text" name="direccion" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca la dirección">
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Teléfono</label>
                        <div class="col-sm-10">
                            <input id="i_telefono" type="number" name="telefono" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca el teléfono">
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Correo</label>
                        <div class="col-sm-10">
                            <input id="i_correo" type="email" name="correo" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca el correo">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">

                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancelar</button>
                    <button type="button" id="btn_modificar" class="btn btn-primary">Modificar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="asignarModal" tabindex="-1" role="dialog" aria-labelledby="asignarModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Asignar a Punto de Servicio</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                </div>
                <div class="modal-body">
                    <h2 id="nombre_empleado_asignar"> </h2>
                    <p class="modal-text">Asignar a :</p>
                    <form>
                        <input type="number" id="id_empleado_asignar" style="display: none">
                        <select class="form-control  basic" id="pto_select">
                            <option  value="no" selected="selected" disabled="disabled">Seleccione Punto</option>
                            @if($puntos ?? '')
                            @foreach($puntos as $p)
                            <option value="{{$p->id}}">{{$p->name}}</option>
                            @endforeach
                            @endif


                        </select>

                        <script>var ss = $(".basic").select2({
                                tags: true,
                            });</script>                     </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancelar</button>
                    <button id="btn_asignar" type="button" class="btn btn-primary">Asignar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="eliminarModal" tabindex="-1" role="dialog" aria-labelledby="eliminarModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Eliminar  Usuario</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="modal-text">Seguro que desea eliminar el Usuario</p>
                    <form>
                        <input type="number" id="id_producto_eliminar" style="display: none">

                        <h2 id="nombre_producto"> </h2>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancelar</button>
                    <button id="btn_eliminar" type="button" class="btn btn-primary">Eliminar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="hoja_datosModal" tabindex="-1" role="dialog" aria-labelledby="hoja_datosModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="max-width: max-content">
            <div class="modal-content" >
                <div id="hoja_imprimir">
                    <div class="modal-header">
                        <h5 class="modal-title">Hoja de datos</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                        </button>


                    </div></div>

                <div class="modal-body">
                    <div id="hoja_imprimir">
                        <h2>Empleado:<span id="nombre_emple"></span></h2>
                        <div id="cuerpo_detalles">Seguro que desea eliminar el Usuario</div>
                    </div>
                    <form>
                        <input type="number" id="id_producto_eliminar" style="display: none">

                        <h2 id="nombre_producto"> </h2>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancelar</button>
                    <button id="btn_eliminar" onclick="imprimir_hoja();" type="button" class="btn btn-primary">Imprimir</button>
                </div>
                <script>
                    function imprimir_hoja() {
                        var text = document.getElementById('hoja_imprimir').innerHTML;
                        var ventana = window.open('', 'popimp');
                        ventana.document.write(text);
                        ventana.print();
                    }</script>
            </div>
        </div>
    </div>
    @endsection
    @section('codigos_especifico')
    <script>



        $('#html5-extension').DataTable({
            dom: '<"row"<"col-md-12"<"row"<"col-md-6"l><"col-md-6"f> > >B<"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >',
            buttons: {
                buttons: [
                    {extend: 'copy', className: 'btn'},
                    {extend: 'csv', className: 'btn'},
                    {extend: 'excel', className: 'btn'},
                    {extend: 'print', className: 'btn'}
                ]
            },
            "oLanguage": {
                "oPaginate": {"sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'},
                "sInfo": "Mostrada pagina _PAGE_ de _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Buscar",
                "sLengthMenu": "Results :  _MENU_",
            },
            "columnDefs": [
                {
                    "targets": -1,
                    "data": null,
                    "searchable": false,
                    "defaultContent": '@if(Auth()->user()->role_id !== 2 && Auth()->user()->role_id !== 5   )@if(Auth()->user()->role_id !== 7)<a id="editar"   title="Editar"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></a><a id="eliminar"  title="Eliminar"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg></a> @endif <a   id="ver_contrato" title="Ver contrato"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file"><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 9"></polyline></svg></a>@if(Auth()->user()->role_id !== 7)<a title="Asignar a Punto de servicio" id="asignar_P"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-log-in"><path d="M15 3h4a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2h-4"></path><polyline points="10 17 15 12 10 7"></polyline><line x1="15" y1="12" x2="3" y2="12"></line></svg></a>@endif @endif @if( Auth()->user()->role_id !== 5)<a title="Hoja de datos" id="hoja_datos"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-clipboard"><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg></a>@endif'


                },
                {
                    "targets": [0],
                    "visible": false,
                    "data": 'id'
                },
                {
                    "targets": [1],
                    "data": 'name'
                },
                {
                    "targets": [2],
                    "data": 'cargo'
                },
                {
                    "targets": [3],
                    "data": 'direccion'
                },
                {
                    "targets": [4],
                    "data": 'telefono'
                },
                {
                    "targets": [5],
                    "data": 'email'
                },
            ],
            "stripeClasses": [],
            "lengthMenu": [10, 20, 50, 100, 1000000],
            "pageLength": 10,
            "processing": true,
            "serverSide": true,
            "ajax": "{{url('/empleados')}}"
        });
        $(document).ready(function () {

            App.init();
                    @if (Session::has('insertar_usuario'))

            mostrar_notificacion("{{session('insertar_usuario')}}");
            @endif

                    var table = $('#html5-extension').DataTable();
            $('#html5-extension tbody').on('click', '#hoja_datos', function (e) {
                var data = table.row($(this).parents('tr')).data();
                var id = data['id'];
                e.preventDefault();
                var consulta_m = new XMLHttpRequest();
                if (!window.XMLHttpRequest) {
                    consulta_m = new ActiveXObject("Microsoft.XMLHTTP");
                }
                consulta_m.onreadystatechange = function () {
                    if (consulta_m.readyState === 4 && consulta_m.status === 200) {

                        var datos = JSON.parse(consulta_m.responseText);
                        document.getElementById('nombre_emple').innerHTML = data['name'];
                        if (datos.punto) {
                            punto_nombre = datos.punto.name
                        } else {
                            punto_nombre = 'Aun no se encuentra'
                        }
                        document.getElementById('cuerpo_detalles').innerHTML = '<div class="table"><table class="table mb-4"><thead><tr><th class="text-center">Nombre</th><th>Cargo</th><th>Dirección</th><th class="text-center">Correo</th><th class="text-center">Teléfono</th></tr></thead><tbody><tr><td class="text-center">' + data['name'] + '</td><td class="text-center">' + data['cargo'] + '</td><td class="text-center">' + data['direccion'] + '</td><td class="text-center">' + data['email'] + '</td> <td class="text-center">' + data['telefono'] + '</td></tr><thead><tr><th>Sueldo Base:</th><td>' + datos.sueldo_base + '</td> <th>Fecha del contrato:</th><td>' + datos.fecha_contrato + '</td><th>Tipo de Contrato</th></tr><tr><th>Costo de movilización</th><td>' + datos.costo_movilizacion + '</td><th>Costo de Colocación</th> <td>' + datos.costo_colocación + '</td> <td>' + datos.tipo_contrato + '</td> </tr><tr><th>Punto de Servicio</th><td>' + punto_nombre + '</td></tr></thead></tbody></table></div>';
                        $('#hoja_datosModal').modal('show');
                    }

                };
                consulta_m.open("GET", 'empleados/detalles/' + id, true);
                consulta_m.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                consulta_m.send();
            });
            $('#html5-extension tbody').on('click', '#ver_contrato', function () {
                var data = table.row($(this).parents('tr')).data();
                document.getElementById('id_empleado_asignar').value = data['id'];

                window.location = "contratos/ver/" + data['id'];
            });
            $('#html5-extension tbody').on('click', '#asignar_P', function () {
                var data = table.row($(this).parents('tr')).data();
                document.getElementById('id_empleado_asignar').value = data['id'];
                document.getElementById('nombre_empleado_asignar').innerHTML = data['name'];
                $('#asignarModal').modal('show');
            });
            $('#html5-extension tbody').on('click', '#eliminar', function () {
                var data = table.row($(this).parents('tr')).data();
                document.getElementById('id_producto_eliminar').value = data['id'];
                document.getElementById('nombre_producto').innerHTML = data['name'];
                $('#eliminarModal').modal('show');
            });
            $('#html5-extension tbody').on('click', '#editar', function () {
                var data = table.row($(this).parents('tr')).data();
                document.getElementById('i_id').value = data['id'];
                document.getElementById('i_nombre').value = data['name'];
                document.getElementById('i_cargo').value = data['cargo'];
                document.getElementById('i_direccion').value = data['direccion'];
                document.getElementById('i_correo').value = data['email'];
                document.getElementById('i_telefono').value = data['telefono'];
                $('#editarModal').modal('show');
            });
            $('#eliminarModal').on('click', '#btn_eliminar', function (e) {

                e.preventDefault();
                var consulta = new XMLHttpRequest();
                if (!window.XMLHttpRequest) {
                    consulta = new ActiveXObject("Microsoft.XMLHTTP");
                }
                consulta.onreadystatechange = function () {
                    if (consulta.readyState === 4 && consulta.status === 200) {

                        var tabla = $("#html5-extension").DataTable();
                        tabla.ajax.reload();
                        mostrar_notificacion(consulta.responseText);
                        $('#eliminarModal').modal('hide');
                    }

                };
                consulta.open("DELETE", 'usuarios/' + document.getElementById('id_producto_eliminar').value, true);
                consulta.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                // consulta.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                consulta.send();
            });
            $('#editarModal').on('click', '#btn_modificar', function (e) {
                e.preventDefault();
                var consulta_m = new XMLHttpRequest();
                if (!window.XMLHttpRequest) {
                    consulta_m = new ActiveXObject("Microsoft.XMLHTTP");
                }
                consulta_m.onreadystatechange = function () {
                    if (consulta_m.readyState === 4 && consulta_m.status === 200) {

                        var tabla = $("#html5-extension").DataTable();
                        tabla.ajax.reload();
                        mostrar_notificacion(consulta_m.responseText);
                        $('#editarModal').modal('hide');
                    }

                };
                var id = document.getElementById('i_id').value;
                var nombre = document.getElementById('i_nombre').value;
                var cargo = document.getElementById('i_cargo').value;
                var direccion = document.getElementById('i_direccion').value;
                var telefono = document.getElementById('i_telefono').value;
                var correo = document.getElementById('i_correo').value;
                var valores = '?id=' + id + '&nombre=' + nombre + '&cargo=' + cargo + '&direccion=' + direccion + '&telefono=' + telefono + '&correo=' + correo;
                consulta_m.open("PUT", 'usuarios/' + document.getElementById('i_id').value + '/' + valores, true);
                consulta_m.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                // consulta.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                consulta_m.send();
            });
            $('#asignarModal').on('click', '#btn_asignar', function (e) {
                e.preventDefault();
                var consulta_m = new XMLHttpRequest();
                if (!window.XMLHttpRequest) {
                    consulta_m = new ActiveXObject("Microsoft.XMLHTTP");
                }
                consulta_m.onreadystatechange = function () {
                    if (consulta_m.readyState === 4 && consulta_m.status === 200) {

                        var tabla = $("#html5-extension").DataTable();
                        tabla.ajax.reload();
                        mostrar_notificacion(consulta_m.responseText);
                        $('#asignarModal').modal('hide');
                    }

                };
                var id_user = document.getElementById('id_empleado_asignar').value;
                var id_pto = document.getElementById('pto_select').value;

                var valores = '?id_user=' + id_user + '&id_pto=' + id_pto;
                consulta_m.open("PUT", 'empleados/asignar/' + id_user + '/' + id_pto, true);
                consulta_m.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                // consulta.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                consulta_m.send();
            });
            $('#html5-extension tbody').on('click', '#nomina', function () {
                var data = table.row($(this).parents('tr')).data();

                $('#nominaModal').modal('show');
            });
        }
        );
    </script>

    @endsection

