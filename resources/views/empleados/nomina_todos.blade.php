@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <h2>Listado de Empleados</h2>
                    @if(Auth()->user()->role_id !== 7) <a href="{{url('usuarios/create')}}"><button class="btn-success btn" style="float: right;margin-right: 25px">Insertar Usuario</button></a>@endif
                    <div class="table-responsive mb-4 mt-4">
                        <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Nombre</th>
                                    <th>Cargo</th>
                                    <th>Dirección</th>
                                    <th>Teléfono</th>
                                    <th>Correo</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="verNomina" style="overflow-y: scroll;" tabindex="-1" role="dialog" aria-labelledby="verNominaLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Nómina:</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                </div>
                <div class="modal-body" id="cuerpo_modal_nomina">
                    CARGANDO.......
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancelar</button>
                    <button id="imprimir_nomina" onclick="imprimir()" type="button" class="btn btn-primary">Imprimir</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        function imprimir() {
            var text = document.getElementById('cuerpo_modal_nomina').innerHTML;
            var ventana = window.open('', 'popimp');
            ventana.document.write(text);
            ventana.print();

        }</script>
    <div class="modal fade" id="rangoFecha" tabindex="-1" role="dialog" aria-labelledby="rangoFechaModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Nómina:</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                </div>
                <div class="modal-body">
                    <input id="empleado_id" type="number" style="display: none">
                        <p class="modal-text">Seleccione el rango de fecha para calcular el pago:</p>
                        Fecha Inicio
                        <input id="inicio"  value="2020-09-04" class="form-control flatpickr flatpickr-input active" type="date" placeholder="Seleccione el dia"  >
                            Fecha Fin:
                            <input id="fin" value="2020-09-04" class="form-control flatpickr flatpickr-input active" type="date" placeholder="Seleccione el dia" >
                                @if(Auth()->user()->role_id !=7 )   Provida:
                                <input id="provida"  value="0" class="form-control flatpickr flatpickr-input active" type="number" placeholder="Introduzca la provida" >
                                    Descuento de Anticipo:
                                    <input id="anticipo" value="0" class="form-control flatpickr flatpickr-input active" type="number" placeholder="Introduzca el descuento de anticipo" >
                                        Bono de Estimulación:
                                        <input id="bono" value="0"  class="form-control flatpickr flatpickr-input active" type="number" placeholder="Introduzca bono por estimulo" >
                                            Otro Pago:
                                            <input id="otro_pago" value="0"  class="form-control flatpickr flatpickr-input active" type="number" placeholder="Introduzca monto por otro pago" >
                                                Bono de Estimulación:
                                                <input id="otro_descuento" value="0"  class="form-control flatpickr flatpickr-input active" type="number" placeholder="Introduzca monto por otro descuento" >
                                                    @endif
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancelar</button>
                                                        <button id="btn_ver_nomina" type="button" class="btn btn-primary">Ver</button>
                                                    </div>
                                                    </div>
                                                    </div>
                                                    </div>
                                                    @endsection
                                                    @section('codigos_especifico')
                                                    <script>
                                                        $('#html5-extension').DataTable({
                                                            dom: '<"row"<"col-md-12"<"row"<"col-md-6"l><"col-md-6"f> > >B<"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >',
                                                            buttons: {
                                                                buttons: [
                                                                    {extend: 'copy', className: 'btn'},
                                                                    {extend: 'csv', className: 'btn'},
                                                                    {extend: 'excel', className: 'btn'},
                                                                    {extend: 'print', className: 'btn'}
                                                                ]
                                                            },
                                                            "oLanguage": {
                                                                "oPaginate": {"sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'},
                                                                "sInfo": "Mostrada pagina _PAGE_ de _PAGES_",
                                                                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                                                                "sSearchPlaceholder": "Buscar",
                                                                "sLengthMenu": "Results :  _MENU_",
                                                            },
                                                            "columnDefs": [
                                                                {
                                                                    "targets": -1,
                                                                    "data": null,
                                                                    "searchable": false,
                                                                    "defaultContent": '<a title="Ver Nómina" id="nomina"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-clipboard"><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg></a>'
                                                                },
                                                                {
                                                                    "targets": [0],
                                                                    "visible": false,
                                                                    "data": 'id'
                                                                },
                                                                {
                                                                    "targets": [1],
                                                                    "data": 'name'
                                                                },
                                                                {
                                                                    "targets": [2],
                                                                    "data": 'cargo'
                                                                },
                                                                {
                                                                    "targets": [3],
                                                                    "data": 'direccion'
                                                                },
                                                                {
                                                                    "targets": [4],
                                                                    "data": 'telefono'
                                                                },
                                                                {
                                                                    "targets": [5],
                                                                    "data": 'email'
                                                                },
                                                            ],
                                                            "stripeClasses": [],
                                                            "lengthMenu": [10, 20, 50, 100, 1000000],
                                                            "pageLength": 10,
                                                            "processing": true,
                                                            "serverSide": true,
                                                            "ajax": "{{url('/empleados')}}"
                                                        });
                                                        $(document).ready(function () {

                                                        App.init();
                                                                @if (Session::has('insertar_usuario'))

                                                                mostrar_notificacion("{{session('insertar_usuario')}}");
                                                                @endif
                                                                $('#btn_ver_nomina').on('click', function (e) {
                                                        $('#rangoFecha').modal('hide');
                                                                //codigo de consulta para ver el pago del trabajador

                                                                var consulta_m = new XMLHttpRequest();
                                                                if (!window.XMLHttpRequest) {
                                                        consulta_m = new ActiveXObject("Microsoft.XMLHTTP");
                                                        }
                                                        consulta_m.onreadystatechange = function () {
                                                        if (consulta_m.readyState === 4 && consulta_m.status === 200) {

                                                        $('#verNomina').modal('show');
                                                                document.getElementById('cuerpo_modal_nomina').innerHTML = consulta_m.responseText;
                                                        }
                                                        }; var id_empleado = document.getElementById('empleado_id').value;
                                                                var inicio = document.getElementById('inicio').value;
                                                                var fin = document.getElementById('fin').value;
                                                                var provida = document.getElementById('provida').value;
                                                                var anticipo = document.getElementById('anticipo').value;
                                                                var bono = document.getElementById('bono').value;
                                                                var otro_pago = document.getElementById('otro_pago').value;
                                                                var otro_descuento = document.getElementById('otro_descuento').value;
                                                                consulta_m.open("GET", '{{url("nomina")}}/' + id_empleado + '/' + inicio + '/' + fin + '/' + provida + '/' + anticipo + '/' + bono + '/' + otro_pago + '/' + otro_descuento, true);
                                                                consulta_m.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                                                                consulta_m.send();
                                                                $('#rangoFecha').modal('hide');
                                                        });
                                                                $('#html5-extension tbody').on('click', '#nomina', function (e) {
                                                        var table = $('#html5-extension').DataTable();
                                                                var data = table.row($(this).parents('tr')).data();
                                                                document.getElementById('empleado_id').value = data['id']

                                                                $('#rangoFecha').modal('show');
                                                        });
                                                        }
                                                        );
                                                    </script>
                                                    @endsection
