@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <h2>Listado de Perdida Pendientes</h2>


                    <div class="table-responsive mb-4 mt-4">
                        <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>id_producto</th>
                                    <th>id_punto</th>
                                    <th>Nombre del Punto</th>
                                    <th>Nombre del Producto</th>
                                    <th>Cantidad de Productos</th>
                                    <th>Precio</th>
                                    <th>Informa</th>
                                    <th>Descripción</th>
                                    <th>Estado</th>
                                     <th>Fecha</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>



    <div class="modal fade" id="informarModal" tabindex="-1" role="dialog" aria-labelledby="informarModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Informar Perdida Pendientes:</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="modal-text">Producto:</p>
                    <h3 id="prod_info"></h3>
                    <form>
                        <input type="number" id="id_prod_informar" style="display: none">

                        <h2 id="nombre_producto"> </h2>
                        <p>Motivo, Razón o Comentario(Opcional)</p>
                        <textarea class="form-control" id="descripcion_informar" rows="3"></textarea>      
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancelar</button>
                    <button id="btn_informar" type="button" class="btn btn-primary">Informar</button>
                </div>
            </div>
        </div>
    </div>


    @endsection
    @section('codigos_especifico')
    <script>

        function mostrar_notificacion(mensaje) {
            Snackbar.show({
                showAction: false,
                text: mensaje,
            });
        }

        $('#html5-extension').DataTable({
            dom: '<"row"<"col-md-12"<"row"<"col-md-6"l><"col-md-6"f> > >B<"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >',
            buttons: {
                buttons: [
                    {extend: 'copy', className: 'btn'},
                    {extend: 'csv', className: 'btn'},
                    {extend: 'excel', className: 'btn'},
                    {extend: 'print', className: 'btn'}
                ]
            },
            "oLanguage": {
                "oPaginate": {"sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'},
                "sInfo": "Mostrada pagina _PAGE_ de _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Buscar",
                "sLengthMenu": "Results :  _MENU_",
            },
            "columnDefs": [
                {
                    "targets": -1,
                    "data": null,
                    "searchable": false,
                    "defaultContent": '<a id="confirmar" title="Confirmar Perdida"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-circle"><path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path><polyline points="22 4 12 14.01 9 11.01"></polyline></svg></a><a title="Eliminar" id="eliminar"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg> </a>'
                },
                {
                    "targets": [0],
                    "visible": false,
                    "data": 'id',
                },
                {
                    "targets": [1],
                    "visible": false,
                    "data": 'id_producto'
                },
                {
                    "targets": [2],
                    "visible": false,
                    "data": 'id_punto'
                },
                {
                    "targets": [3],
                    "data": 'nombre_punto'
                },
                {
                    "targets": [4],
                    "data": 'nombre_producto'
                },
                {
                    "targets": [5],
                    "data": 'cant_producto'
                },
                {
                    "targets": [6],
                    "data": 'precio'
                },
                {
                    "targets": [7],
                    "data": 'informa'
                },
                {
                    "targets": [8],
                    "data": 'descrip'
                },
                {
                    "targets": [9],
                    "data": 'estado'
                },
                {
                    "targets": [10],
                    "data": 'fecha'
                },
            ],
            "stripeClasses": [],
            "lengthMenu": [10, 20, 50, 100, 1000000],
            "pageLength": 10,
            "processing": true,
            "serverSide": true,
            "ajax": "{{url('/perdidas/json')}}"
        });
        $(document).ready(function () {

            App.init();
                    @if (Session::has('insertar_productos'))

            mostrar_notificacion("{{session('insertar_productos')}}");

            @endif

                    var table = $('#html5-extension').DataTable();

            $('#html5-extension tbody').on('click', '#confirmar', function (e) {
                var data = table.row($(this).parents('tr')).data();
                document.getElementById('id_prod_informar').value = data['id'];
                e.preventDefault();
                var consulta = new XMLHttpRequest();
                if (!window.XMLHttpRequest) {
                    consulta = new ActiveXObject("Microsoft.XMLHTTP");
                }
                consulta.onreadystatechange = function () {
                    if (consulta.readyState === 4 && consulta.status === 200) {

                        mostrar_notificacion(consulta.responseText);
                        table.draw();
                    }
                };
                consulta.open("POST", "{{url('/perdidas/confirmar')}}" + '/' + data['id'], true);
                consulta.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                consulta.send();








            });
            $('#html5-extension tbody').on('click', '#eliminar', function (e) {
                var data = table.row($(this).parents('tr')).data();
                document.getElementById('id_prod_informar').value = data['id'];
                e.preventDefault();
                var consulta = new XMLHttpRequest();
                if (!window.XMLHttpRequest) {
                    consulta = new ActiveXObject("Microsoft.XMLHTTP");
                }
                consulta.onreadystatechange = function () {
                    if (consulta.readyState === 4 && consulta.status === 200) {

                        mostrar_notificacion(consulta.responseText);
                        table.draw();
                    }
                };
                consulta.open("POST", "{{url('/perdidas/eliminar')}}" + '/' + data['id'], true);
                consulta.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                consulta.send();








            });


        }
        );
    </script>

    @endsection

