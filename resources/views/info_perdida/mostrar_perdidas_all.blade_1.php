@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <h2>Listado de Perdida</h2>
                    <input type="number" id="min">
                    <input type="number" id="max">

                    <div class="table-responsive mb-4 mt-4">
                        <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                            <thead>
                                <tr>
                                   
                                    <th>Nombre del Punto</th>
                                    <th>Nombre del Producto</th>
                                    <th>Cantidad de Productos</th>
                                    <th>Precio</th>
                                    <th>Informa</th>
                                    <th>Descripción</th>
                                    <th>Estado</th>
                                    <th>Fecha</th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach($perdidas as $p)
                                <tr>
                                    <td>$p->nombre_punto</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>



    <div class="modal fade" id="informarModal" tabindex="-1" role="dialog" aria-labelledby="informarModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Informar Perdida:</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="modal-text">Producto:</p>
                    <h3 id="prod_info"></h3>
                    <form>
                        <input type="number" id="id_prod_informar" style="display: none">

                        <h2 id="nombre_producto"> </h2>
                        <p>Motivo, Razón o Comentario(Opcional)</p>
                        <textarea class="form-control" id="descripcion_informar" rows="3"></textarea>      
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancelar</button>
                    <button id="btn_informar" type="button" class="btn btn-primary">Informar</button>
                </div>
            </div>
        </div>
    </div>


    @endsection
    @section('codigos_especifico')
    <script>

   $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
                var min = parseInt( $('#min').val(), 10 );
                var max = parseInt( $('#max').val(), 10 );
                alert('sdsa');
                var age = parseFloat( data[6] ) || 0; // use data for the age column
         
                if ( ( isNaN( min ) && isNaN( max ) ) ||
                     ( isNaN( min ) && age <= max ) ||
                     ( min <= age   && isNaN( max ) ) ||
                     ( min <= age   && age <= max ) )
                {
                    return true;
                }
                return false;
            }
        );         


        $(document).ready(function () {

            App.init();
                    @if (Session::has('insertar_productos'))

            mostrar_notificacion("{{session('insertar_productos')}}");

            @endif





        $('#html5-extension').DataTable({
            dom: '<"row"<"col-md-12"<"row"<"col-md-6"l><"col-md-6"f> > >B<"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >',
            buttons: {
                buttons: [
                    {extend: 'copy', className: 'btn'},
                    {extend: 'csv', className: 'btn'},
                    {extend: 'excel', className: 'btn'},
                    {extend: 'print', className: 'btn'}
                ]
            },
            "oLanguage": {
                "oPaginate": {"sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'},
                "sInfo": "Mostrada pagina _PAGE_ de _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Buscar",
                "sLengthMenu": "Results :  _MENU_",
            },
            "columnDefs": [

                {
                    "targets": [0],
                    //"visible": false,
                    "data": 'id',
                },
                {
                    "targets": [1],
                    "visible": false,
                    "data": 'id_producto'
                },
                {
                    "targets": [2],
                    "visible": false,
                    "data": 'id_punto'
                },
                {
                    "targets": [3],
                    "data": 'nombre_punto'
                },
                {
                    "targets": [4],
                    "data": 'nombre_producto'
                },
                {
                    "targets": [5],
                    "data": 'cant_producto'
                },
                {
                    "targets": [6],
                    "data": 'precio'
                },
                {
                    "targets": [7],
                    "data": 'informa'
                },
                {
                    "targets": [8],
                    "data": 'descrip'
                },
                {
                    "targets": [9],
                    "data": 'estado'
                },
                {
                    "targets": [10],
                    type: 'datetime',
                    "data": 'fecha'
                },
            ],
            "stripeClasses": [],
            "lengthMenu": [10, 20, 50, 100, 1000000],
            "pageLength": 10,
            "processing": true,
            "serverSide": true,
            "ajax": "{{url('perdidas/listar_perdidas_json_all')}}"
        });


                    var table = $('#html5-extension').DataTable();
 $('#min, #max').change( function() { table.draw(); } );

            $('#html5-extension tbody').on('click', '#informar', function () {
                var data = table.row($(this).parents('tr')).data();
                document.getElementById('id_prod_informar').value = data['id'];
                document.getElementById('prod_info').innerHTML = data['nombre'];
                $('#informarModal').modal('show');
            });

            $('#informarModal').on('click', '#btn_informar', function (e) {
                e.preventDefault();
                var consulta = new XMLHttpRequest();
                if (!window.XMLHttpRequest) {
                    consulta = new ActiveXObject("Microsoft.XMLHTTP");
                }
                consulta.onreadystatechange = function () {
                    if (consulta.readyState === 4 && consulta.status === 200) {

                        var tabla = $("#html5-extension").DataTable();
                        tabla.ajax.reload();
                        mostrar_notificacion(consulta.responseText);
                        $('#informarModal').modal('hide');
                    }

                };
                consulta.open("PUT", "{{url('/notificacion/crear')}}" + '?relacion_1=' + document.getElementById('id_prod_informar').value + '&relacion_2=' + '{{ Auth::user()->name }}' + '&tipo=Perdida&descrip=' + document.getElementById('descripcion_informar').value + '&titulo=Informe de Perdida', true);
                consulta.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                consulta.send();
            });

        }
        );




    </script>

    @endsection

