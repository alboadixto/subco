@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <h2>Informe Totales</h2>
                    <button class="btn-success btn" onclick="calcular()" style="float: right;margin-right: 25px">Calcular balance</button>
                    <div class="table-responsive mb-4 mt-4">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>Fechass de Issnicio:</td>
                                    <td><input type="date" id="min" class="form-control" name="min"></td>
                                </tr>
                                <tr>
                                    <td>Fecha Fin:</td>
                                    <td><input type="date" id="max" class="form-control" name="max" ></td>
                                </tr>
                            </tbody>
                        </table>
                        <div>
                            <center><h3>Balance</h3></center>
                            <center><img src="{{asset('img/logo.png')}}"></center>
                            <p>Desde:<span id="fecha_b_i"><span></p>
                                        <p>Hasta:<span id="fecha_b_f"><span></p>
                                                    <div id="compras_div">
                                                    </div>
                                                    <div id="ventas_div">
                                                    </div>
                                                    <div id="perdidas_div">
                                                    </div>
                                                    <div id="uniforme_div">
                                                    </div>
                                                    <div id="empleados_div">
                                                    </div>
                                                    </div>
                                                    </div>
                                                    </div>
                                                    </div>
                                                    </div>
                                                    </div>
                                                    <script>
                                                        function calcular() {
                                                         
                                                        calcular_balance_compra();
                                                        calcular_balance_ventas();
                                                        calcular_balance_perdidas();
                                                        calcular_balance_uniforme();
                                                        calcular_balance_empleados();
                                                        }
                                                         function calcular_balance_empleados() {
                                                        document.getElementById('fecha_b_i').innerHTML = document.getElementById('min').value;
                                                        document.getElementById('fecha_b_f').innerHTML = document.getElementById('max').value;
                                                        var consulta_m = new XMLHttpRequest();
                                                        if (!window.XMLHttpRequest) {
                                                        consulta_m = new ActiveXObject("Microsoft.XMLHTTP");
                                                        }
                                                        consulta_m.onreadystatechange = function () {
                                                        if (consulta_m.readyState === 4 && consulta_m.status === 200) {
                                                            alert(consulta_m.responseText);
                                                        document.getElementById('empleados_div').innerHTML = '<center><h3>Pagos a Empleados</h3></center>' + consulta_m.responseText;
                                                        }
                                                        };
                                                        alert('sadsad');
                                                        consulta_m.open("GET", "{{url('informe/calculo/empleados')}}", true);
                                                        consulta_m.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                                                        // consulta.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                                        consulta_m.send();
                                                        }
                                                        function calcular_balance_ventas() {
                                                        document.getElementById('fecha_b_i').innerHTML = document.getElementById('min').value;
                                                        document.getElementById('fecha_b_f').innerHTML = document.getElementById('max').value;
                                                        var consulta_m = new XMLHttpRequest();
                                                        if (!window.XMLHttpRequest) {
                                                        consulta_m = new ActiveXObject("Microsoft.XMLHTTP");
                                                        }
                                                        consulta_m.onreadystatechange = function () {
                                                        if (consulta_m.readyState === 4 && consulta_m.status === 200) {
                                                        document.getElementById('compras_div').innerHTML = '<center><h3>Ventas</h3></center>' + consulta_m.responseText;
                                                        }
                                                        };
                                                        consulta_m.open("GET", "{{url('/facturas/venta/balance')}}" + '/' + document.getElementById('min').value + '/' + document.getElementById('max').value, true);
                                                        consulta_m.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                                                        // consulta.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                                        consulta_m.send();
                                                        }
                                                        function calcular_balance_compra() {
                                                        document.getElementById('fecha_b_i').innerHTML = document.getElementById('min').value;
                                                        document.getElementById('fecha_b_f').innerHTML = document.getElementById('max').value;
                                                        var consulta_m = new XMLHttpRequest();
                                                        if (!window.XMLHttpRequest) {
                                                        consulta_m = new ActiveXObject("Microsoft.XMLHTTP");
                                                        }
                                                        consulta_m.onreadystatechange = function () {
                                                        if (consulta_m.readyState === 4 && consulta_m.status === 200) {
                                                        document.getElementById('ventas_div').innerHTML = '<center><h3>Compras</h3></center>' + consulta_m.responseText;
                                                        }
                                                        };
                                                        consulta_m.open("GET", "{{url('/facturas/compra/balance')}}" + '/' + document.getElementById('min').value + '/' + document.getElementById('max').value, true);
                                                        consulta_m.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                                                        // consulta.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                                        consulta_m.send();
                                                        }
                                                        function calcular_balance_perdidas() {
                                                        document.getElementById('fecha_b_i').innerHTML = document.getElementById('min').value;
                                                        document.getElementById('fecha_b_f').innerHTML = document.getElementById('max').value;
                                                        var consulta_m = new XMLHttpRequest();
                                                        if (!window.XMLHttpRequest) {
                                                        consulta_m = new ActiveXObject("Microsoft.XMLHTTP");
                                                        }
                                                        consulta_m.onreadystatechange = function () {
                                                        if (consulta_m.readyState === 4 && consulta_m.status === 200) {
                                                        document.getElementById('perdidas_div').innerHTML = '<center><h3>Perdidas</h3></center>' + consulta_m.responseText;
                                                        }
                                                        };
                                                        consulta_m.open("GET", "{{url('perdidas/calcular')}}" + '/' + document.getElementById('min').value + '/' + document.getElementById('max').value, true);
                                                        consulta_m.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                                                        // consulta.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                                        consulta_m.send();
                                                        }
                                                        function calcular_balance_uniforme() {
                                                        document.getElementById('fecha_b_i').innerHTML = document.getElementById('min').value;
                                                        document.getElementById('fecha_b_f').innerHTML = document.getElementById('max').value;
                                                        var consulta_m = new XMLHttpRequest();
                                                        if (!window.XMLHttpRequest) {
                                                        consulta_m = new ActiveXObject("Microsoft.XMLHTTP");
                                                        }
                                                        consulta_m.onreadystatechange = function () {
                                                        if (consulta_m.readyState === 4 && consulta_m.status === 200) {
                                                        document.getElementById('uniforme_div').innerHTML = '<center><h3>Uniformes</h3></center>' + consulta_m.responseText;
                                                        }
                                                        };
                                                        consulta_m.open("GET", "{{url('/uniforme/gastos/calcular')}}" + '/' + document.getElementById('min').value + '/' + document.getElementById('max').value, true);
                                                        consulta_m.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                                                        // consulta.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                                        consulta_m.send();
                                                        }
                                                                                                             function imprimir_balance() {

                                                        var text = document.getElementById('b_imprimir').innerHTML;
                                                        var ventana = window.open('', 'solicitud_decraración_recibo');
                                                        ventana.document.write('<style>table { width: 100%;   border: 1px solid #000;}th, td {   width: 25%;   text-align: left;   vertical-align: top;   border: 1px solid #000;}input:focus {outline: 0; outline: none;}</style>');
                                                        ventana.document.write(text);
                                                        ventana.print();
                                                        }
                                                    </script>
                                                    @endsection
                                                    @section('codigos_especifico')
                                                    <script>

                                                        $(document).ready(function () {

                                                        App.init();
                                                        @if (Session::has('insertar_usuario'))

                                                                mostrar_notificacion("{{session('insertar_usuario')}}");
                                                        @endif

                                                                $('#min').change(function () {
                                                        //   alert(document.getElementById('min').value);
                                                        });
                                                        //  $('#max').change(function () {
                                                        //      location.reload();
                                                        //  });
                                                        }
                                                        );

                                                    </script>
                                                    @endsection

