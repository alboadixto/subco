@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <h2>Insertar  Productos</h2>
                    <div class="modal-body">
                        <form action="{{url('productos')}}" method="POST">
                            <div class="form-group row  mb-4">
                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Codigo</label>
                                <div class="col-sm-10">
                                    <input type="text" name="codigo" class="form-control form-control-lg" id="colFormLabelSm" placeholder="Introduzca el codigo" required>
                                </div>
                            </div>
                            {{csrf_field()}}
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Proveedor</label>
                                <div class="col-sm-10">
                                    <select id="proveedor" name="proveedor" class="form-control form-small" required="">
                                        <option selected="selected" disabled="disabled">Seleccionar Proveedor</option>
                                        @if($proveedor ?? '')
                                        @foreach($proveedor ?? ''  as $p)
                                        <option value="{{$p->id}}">{{$p->name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                    <script>
                                        var formSmall = $(".form-small").select2({tags: true});
                                        formSmall.data('select2').$container.addClass('form-control-sm');</script>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Nombre</label>
                                <div class="col-sm-10">
                                    <input type="text" name="nombre" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca el nombre" required="">
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Stock</label>
                                <div class="col-sm-10">
                                    <input type="number" name="stock" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca stock" required>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Tipo</label>
                                <div class="col-sm-10">
                                    <select id="tipo" name="tipo" class="form-control form-small" required="required">
                                        <option selected="selected" disabled="disabled">Seleccionar el tipo</option>
                                        <option value="Insumo">Insumo</option>
                                        <option value="Maquinaria">Maquinaría</option>
                                    </select>

                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Marca</label>
                                <div class="col-sm-10">
                                    <input type="text" name="marca" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca la marca" required>
                                </div>
                            </div> <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Unidad de Medida</label>
                                <div class="col-sm-10">
                                    <input type="text" name="unidad_medida" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca la unidad de medida" required>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Precio</label>
                                <div class="col-sm-10">
                                    <input type="number" name="ultimo_precioxunidad"  class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca el precio" required>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Estado</label>
                                <div class="col-sm-10">
                                    <select id="estado" name="estado" class="form-control form-small" required="required">
                                        <option selected="selected" disabled="disabled">Seleccionar el Estado</option>
                                        <option value="Consumible">Insumo</option>
                                        <option value="Menos 1 Año">Menos 1 Años</option>
                                        <option value="Mas de 1 Año">Mas de 1 Año</option>
                                        <option value="Mas de 2 Años">Mas de 2 Años</option>
                                        <option value="Mas de 3 Años">Mas de 3 Años</option>
                                        <option value="Mas de 4 Años">Mas de 4 Años</option>
                                        <option value="Mas de 5 Años">Mas de 5 Años</option>
                                        <option value="Mas de 6 Años">Mas de 6 Años</option>
                                        <option value="Mas de 7 Años">Mas de 7 Años</option>
                                         <option value="Mas de 8 Años">Mas de 8 Años</option>
                                        <option value="Mas de 9 Años">Mas de 9 Años</option>
                                        <option value="Mas de 10 Años">Mas de 10 Años</option>
                                        <option value="Funcional">Funcional</option>
                                        <option value="Buen Estado">Buen Estado</option>
                                        <option value="Termino medio">Termino medio</option>
                                        <option value="Obsoleto">Obsoleto</option>
                                        <option value="Mal Estado">Mal Estado</option>
                                    </select>
                                </div>
                            </div>
                            <input type="submit" name="time" class="mb-4 btn btn-primary" value="Agregar" >
                            <a href="{{url('listar_productos')}}"> <input type="button" name="time" class="mb-4 btn btn-success" value="Cancelar" ></a>
                        </form>
                    </div>
                </div>
            </div>

        </div>

    </div>



    <div class="modal fade" id="eliminarModal" tabindex="-1" role="dialog" aria-labelledby="eliminarModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Eliminar  Producto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="modal-text">Mauris mi tellus, pharetra vel mattis sed, tempus ultrices eros. Phasellus egestas sit amet velit sed luctus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse potenti. Vivamus ultrices sed urna ac pulvinar. Ut sit amet ullamcorper mi. </p>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancelar</button>
                    <button type="button" class="btn btn-primary">Modificar</button>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @section('codigos_especifico')

    <script>


        $(document).ready(function () {
            App.init();

        });
    </script>
    @endsection

