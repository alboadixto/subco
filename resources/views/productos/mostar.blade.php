@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <h2>Listado de Productos</h2>
                   @if(Auth()->user()->role_id !== 5  && Auth()->user()->role_id !== 6)<a href="{{url('productos/create')}}"><button class="btn-success btn" style="float: right;margin-right: 25px">Insertar Producto</button></a>@endif
                    <div class="table-responsive mb-4 mt-4">
                        <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>codigo</th>
                                    <th>proveedor</th>
                                    <th>nombre</th>
                                    <th>stock</th>
                                    <th>tipo</th>
                                    <th>marca</th>
                                    <th>unidad de medida</th>
                                    <th>Precio</th>
                                    <th>Estado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <div class="card component-card_3" >
            <div class="card-body" id="body_impirmir">
                <img src="{{asset('img/logo.png')}}" class="img-preview"  >
                <h3>Balance de productos en Stock</h3>
                <h5 class="card-user_name">Cantidad de Maquinarías: <span id="cant_perdidas">{{$cant_maquinaria}}</span></h5>
                <h5 class="card-user_name">Monto Total Maquinarías: <span id="monto_perdidas">{{$precio_maquinaria}}</span></h5>
                <h5 class="card-user_name">Cantidad de Insumos: <span id="monto_perdidas">{{$cant_insumo}}</span></h5>
                <h5 class="card-user_name">Monto Total insumos: <span id="monto_perdidas">{{$precio_insumo}}</span></h5>
                <h5 class="card-user_name">Monto total: <span id="monto_perdidas">{{$precio_total}}</span></h5>
             



            </div>
            <center><button onclick="imprimir()" id="imprimir" class="btn btn-success mb-2" >Imprimir</button></center>
            <center><button onclick="location.reload()" id="imprimir" class="btn btn-primary mb-2" >Actualizar</button></center>
        </div>


        <script>
            function imprimir()
            {
                var text = document.getElementById('body_impirmir').innerHTML;
                var ventana = window.open('', 'todo');
                ventana.document.write(text);
                ventana.print();
            }

        </script>
    </div>

    <div class="modal fade" id="editarModal" tabindex="-1" role="dialog" aria-labelledby="editarModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modificar Producto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group row  mb-4">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Codigo</label>
                        <div class="col-sm-10">
                            <input id="i_id" type="nmber" name="id" class="form-control form-control-sm" id="colFormLabelSm" placeholder="col-form-label-sm" style="display: none">
                            <input id="i_codigo" type="text" name="codigo" class="form-control form-control-lg" id="colFormLabelSm" placeholder="col-form-label-sm">
                        </div>
                    </div>
                    {{csrf_field()}}

                    <div class="form-group row mb-4">
                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Nombre</label>
                        <div class="col-sm-10">
                            <input id="i_nombre" type="text" name="nombre" class="form-control form-control-lg" id="colFormLabelLg" placeholder="col-form-label-lg">
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Stock</label>
                        <div class="col-sm-10">
                            <input id="i_stock" type="number" name="stock" class="form-control form-control-lg" id="colFormLabelLg" placeholder="col-form-label-lg">
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Tipo</label>
                        <div class="col-sm-10">
                            <input id="i_tipo" type="text" name="tipo" class="form-control form-control-lg" id="colFormLabelLg" placeholder="col-form-label-lg">
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Marca</label>
                        <div class="col-sm-10">
                            <input id="i_marca" type="text" name="marca" class="form-control form-control-lg" id="colFormLabelLg" placeholder="col-form-label-lg">
                        </div>
                    </div> <div class="form-group row mb-4">
                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Unidad de Medida</label>
                        <div class="col-sm-10">
                            <input id="i_medida" type="text" name="medida" class="form-control form-control-lg" id="colFormLabelLg" placeholder="col-form-label-lg">
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Precio</label>
                        <div class="col-sm-10">
                            <input id="i_precio" type="number" name="precio" class="form-control form-control-lg" id="colFormLabelLg" placeholder="col-form-label-lg">
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Precio</label>
                        <div class="col-sm-10">
                            <select id="i_estado" name="estado" class="form-control form-small" required="required">
                                <option selected="selected" disabled="disabled">Seleccionar el Estado</option>
                                <option value="Consumible">Insumo</option>
                                <option value="Menos 1 Año">Menos 1 Años</option>
                                <option value="Mas de 1 Año">Mas de 1 Año</option>
                                <option value="Mas de 2 Años">Mas de 2 Años</option>
                                <option value="Mas de 3 Años">Mas de 3 Años</option>
                                <option value="Mas de 4 Años">Mas de 4 Años</option>
                                <option value="Mas de 5 Años">Mas de 5 Años</option>
                                <option value="Mas de 6 Años">Mas de 6 Años</option>
                                <option value="Mas de 7 Años">Mas de 7 Años</option>
                                <option value="Mas de 8 Años">Mas de 8 Años</option>
                                <option value="Mas de 9 Años">Mas de 9 Años</option>
                                <option value="Mas de 10 Años">Mas de 10 Años</option>
                                <option value="Funcional">Funcional</option>
                                <option value="Buen Estado">Buen Estado</option>
                                <option value="Termino medio">Termino medio</option>
                                <option value="Obsoleto">Obsoleto</option>
                                <option value="Mal Estado">Mal Estado</option>
                            </select>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancelar</button>
                    <button  id="btn_modificar" type="button" class="btn btn-primary">Modificar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="eliminarModal" tabindex="-1" role="dialog" aria-labelledby="eliminarModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Eliminar  Producto:</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="modal-text">Seguro que desea eliminar el productos</p>
                    <form>
                        <input type="number" id="id_producto_eliminar" style="display: none">

                        <h2 id="nombre_producto"> </h2>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancelar</button>
                    <button id="btn_eliminar" type="button" class="btn btn-primary">Eliminar</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="enviarModal" tabindex="-1" role="dialog" aria-labelledby="enviarModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Enviar a   Punto de Servicio:</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                </div>
                <input type="number" id="id_producto_enviar" style="display: none">
                <div class="modal-body">
                    <h3>Total:<span id="cantidad_total"></span></h3>

                    <div class="form-group row mb-4">
                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Enviar:</label>
                        <div class="col-sm-10">
                            <input id="canti_enviar" type="number" name="canti_enviar" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca la cantidad a enviar">
                        </div>
                    </div>
                    <select id="punto_a_enviar" class="form-control  basic">
                        <option  value="no" selected="selected">Seleccione el Punto</option>
                        @if($puntos ?? '')
                        @foreach($puntos as $p)
                        @if($p->role_id == 5)
                        <option value="{{$p->id}}">{{$p->name}}</option>
                        @endif
                        @endforeach
                        @endif


                    </select>
                    <form>


                        <h2 id="nombre_producto"> </h2>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancelar</button>
                    <button id="btn_enviar" type="button" class="btn btn-primary">Enviar</button>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @section('codigos_especifico')
    <script>

        function mostrar_notificacion(mensaje) {
            Snackbar.show({
                showAction: false,
                text: mensaje,
            });
        }

        $('#html5-extension').DataTable({
            dom: '<"row"<"col-md-12"<"row"<"col-md-6"l><"col-md-6"f> > >B<"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >',
            buttons: {
                buttons: [
                    {extend: 'copy', className: 'btn'},
                    {extend: 'csv', className: 'btn'},
                    {extend: 'excel', className: 'btn'},
                    {extend: 'print', className: 'btn'}
                ]
            },
            "oLanguage": {
                "oPaginate": {"sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'},
                "sInfo": "Mostrada pagina _PAGE_ de _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Buscar",
                "sLengthMenu": "Results :  _MENU_",
            },
            "columnDefs": [
                {
                    "targets": -1,
                    "data": null,
                    "searchable": false,
                    "defaultContent": '@if(Auth()->user()->role_id !== 5 && Auth()->user()->role_id !== 6)<a id="editar"   title="Editar"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></a><a id="eliminar"  title="Eliminar"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg></a><a id="asignar" title="Enviar a Punto de servicio"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-share"><path d="M4 12v8a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-8"></path><polyline points="16 6 12 2 8 6"></polyline><line x1="12" y1="2" x2="12" y2="15"></line></svg></a>@endif'
                },
                {
                    "targets": [0],
                    "visible": false,
                    "data": 'id',
                },
                {
                    "targets": [1],
                    "data": 'codigo'
                },
                {
                    "targets": [2],
                    "data": 'proveedor'
                },
                {
                    "targets": [3],
                    "data": 'nombre'
                },
                {
                    "targets": [4],
                    "data": 'stock'
                },
                {
                    "targets": [5],
                    "data": 'tipo'
                },
                {
                    "targets": [6],
                    "data": 'marca'
                },
                {
                    "targets": [7],
                    "data": 'unidad_medida'
                },
                {
                    "targets": [8],
                    "data": 'ultimo_precioxunidad'
                },
                {
                    "targets": [9],
                    "data": 'estado'
                },
            ],
            "stripeClasses": [],
            "lengthMenu": [10, 20, 50, 100, 1000000],
            "pageLength": 10,
            "processing": true,
            "serverSide": true,
            "ajax": "{{url('/productos')}}"
        });
        $(document).ready(function () {

            App.init();
                    @if (Session::has('insertar_productos'))

            mostrar_notificacion("{{session('insertar_productos')}}");

            @endif

                    var table = $('#html5-extension').DataTable();
            $('#html5-extension tbody').on('click', '#eliminar', function () {
                var data = table.row($(this).parents('tr')).data();
                document.getElementById('id_producto_eliminar').value = data['id'];
                document.getElementById('nombre_producto').innerHTML = data['nombre'];
                $('#eliminarModal').modal('show');
            });
            $('#html5-extension tbody').on('click', '#asignar', function () {
                var data = table.row($(this).parents('tr')).data();
                document.getElementById('cantidad_total').innerHTML = data['stock'];
                document.getElementById('id_producto_enviar').value = data['id'];

                $('#enviarModal').modal('show');
            });
            $('#html5-extension tbody').on('click', '#editar', function () {
                var data = table.row($(this).parents('tr')).data();
                document.getElementById('i_id').value = data['id'];
                document.getElementById('i_codigo').value = data['codigo'];

                document.getElementById('i_nombre').value = data['nombre'];
                document.getElementById('i_stock').value = data['stock'];
                document.getElementById('i_tipo').value = data['tipo'];
                document.getElementById('i_marca').value = data['marca'];
                document.getElementById('i_medida').value = data['unidad_medida'];
                document.getElementById('i_precio').value = data['ultimo_precioxunidad'];

                $('#editarModal').modal('show');
            });
            $('#editarModal').on('click', '#btn_modificar', function (e) {


                var consulta_m = new XMLHttpRequest();
                if (!window.XMLHttpRequest) {
                    consulta_m = new ActiveXObject("Microsoft.XMLHTTP");
                }
                consulta_m.onreadystatechange = function () {
                    if (consulta_m.readyState === 4 && consulta_m.status === 200) {

                        var tabla = $("#html5-extension").DataTable();
                        tabla.ajax.reload();
                        mostrar_notificacion(consulta_m.responseText);
                        $('#editarModal').modal('hide');
                    }

                };
                var id = document.getElementById('i_id').value;
                var codigo = document.getElementById('i_codigo').value;
                var estado = document.getElementById('i_estado').value;
                var nombre = document.getElementById('i_nombre').value;
                var stock = document.getElementById('i_stock').value;
                var tipo = document.getElementById('i_tipo').value;
                var marca = document.getElementById('i_marca').value;
                var unidad_medida = document.getElementById('i_medida').value;
                var ultimo_precioxunidad = document.getElementById('i_precio').value;
                var valores = '?id=' + id +'&estado='+estado+ '&codigo=' + codigo + '&nombre=' + nombre + '&stock=' + stock + '&tipo=' + tipo + '&marca=' + marca + '&unidad_medida=' + unidad_medida + '&ultimo_precioxunidad=' + ultimo_precioxunidad;

                consulta_m.open("PUT", 'productos/' + document.getElementById('i_id').value + '/' + valores, true);
                consulta_m.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                // consulta.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                consulta_m.send();
            });
            $('#eliminarModal').on('click', '#btn_eliminar', function (e) {
                e.preventDefault();
                var consulta = new XMLHttpRequest();
                if (!window.XMLHttpRequest) {
                    consulta = new ActiveXObject("Microsoft.XMLHTTP");
                }
                consulta.onreadystatechange = function () {
                    if (consulta.readyState === 4 && consulta.status === 200) {

                        var tabla = $("#html5-extension").DataTable();
                        tabla.ajax.reload();
                        mostrar_notificacion(consulta.responseText);
                        $('#eliminarModal').modal('hide');
                    }

                };
                consulta.open("DELETE", 'productos/' + document.getElementById('id_producto_eliminar').value, true);
                consulta.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                // consulta.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                consulta.send();
            });
            $('#enviarModal').on('click', '#btn_enviar', function (e) {
                e.preventDefault();
                if (document.getElementById('punto_a_enviar').value !== 'no') {
                    if (document.getElementById('canti_enviar').value > 0) {


                        if (document.getElementById('canti_enviar').value < parseInt(document.getElementById('cantidad_total').innerHTML)) {
                            var tabla = $("#html5-extension").DataTable();
                            var consulta = new XMLHttpRequest();
                            if (!window.XMLHttpRequest) {
                                consulta = new ActiveXObject("Microsoft.XMLHTTP");
                            }
                            consulta.onreadystatechange = function () {
                                if (consulta.readyState === 4 && consulta.status === 200) {

                                    var tabla = $("#html5-extension").DataTable();
                                    tabla.ajax.reload();
                                    mostrar_notificacion(consulta.responseText);
                                    $('#enviarModal').modal('hide');
                                }
                            };
                            consulta.open("PUT", 'productos/enviar/' + document.getElementById('id_producto_enviar').value + '/?cant=' + document.getElementById('canti_enviar').value + '&punto=' + document.getElementById('punto_a_enviar').value, true);
                            consulta.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                            // consulta.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                            consulta.send();
                        } else {
                            alert('No puede enviar mas de lo que dispone en el STOCK');
                        }
                    } else {
                        alert('La cantidad a enviar debe ser mayor que 0');
                    }
                } else {
                    alert('Debe seleccionar un punto de servicio para el envio');
                }
            });
        }
        );
    </script>

    @endsection

