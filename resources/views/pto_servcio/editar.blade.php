@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <h2>Editar  Punto de servcio</h2>
                    <div class="modal-body">
                        <form  method="POST" action="{{route('puntos.update',$usuario->id)}}">
                             {{csrf_field()}}
                            <div class="form-group row  mb-4">
                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" >Nombre</label>
                                <div class="col-sm-10">
                                    <input value="@if($usuario ?? '') {{$usuario->name}} @endif" type="text" name="nombre" class="form-control form-control-lg" id="colFormLabelSm" placeholder="Introduzca el Nombre" >
                                    <input type="hidden" name="_method" value="PUT">
                                </div>
                            </div>
                             <div class="form-group row  mb-4">
                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" >Dirección</label>
                                <div class="col-sm-10">
                                    <input value="@if($usuario ?? '') {{$usuario->direccion}} @endif" type="text" name="direccion" class="form-control form-control-lg" id="colFormLabelSm" placeholder="Introduzca la dirección" >
                                    
                                </div>
                            </div>
                             <div class="form-group row  mb-4">
                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" >Teléfono</label>
                                <div class="col-sm-10">
                                    <input value="@if($usuario ?? '') {{$usuario->telefono}} @endif" type="phone" name="telefono" class="form-control form-control-lg" id="colFormLabelSm" placeholder="Introduzca el teléfono" >
                                    
                                </div>
                            </div>
                                  <div class="form-group row  mb-4">
                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" >Correo</label>
                                <div class="col-sm-10">
                                    <input value="@if($usuario ?? '') {{$usuario->email}} @endif" type="email" name="correo" class="form-control form-control-lg" id="colFormLabelSm" placeholder="Introduzca el correo" >
                                    
                                </div>
                            </div>
                            
                             <div class="form-group row  mb-4">
                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" >Folio</label>
                                <div class="col-sm-10">
                                    <input value="@if($usuario ?? '') {{$punto->folio}} @endif" type="text" name="folio" class="form-control form-control-lg" id="colFormLabelSm" placeholder="Introduzca el folio"  >
                                                                        
                                </div>
                            </div>
                                <div class="form-group row  mb-4">
                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" >Nombre del Encargado</label>
                                <div class="col-sm-10">
                                    <input value="@if($punto ?? '') {{$punto->nombre_encargado}} @endif" type="text" name="encargado" class="form-control form-control-lg" id="colFormLabelSm" placeholder="Introduzca el nombre del encargado" >
                                    
                                </div>
                            </div>
                             <div class="form-group row  mb-4">
                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" >Cantidad de Perdonas</label>
                                <div class="col-sm-10">
                                    <input value="@if($punto ?? '') {{$punto->cant_persona}} @endif" type="text" name="cant_persona" class="form-control form-control-lg" id="colFormLabelSm" placeholder="Introduzca la cantidad de personas" >
                                    
                                </div>
                            </div>
                            <div class="form-group row  mb-4">
                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" >Fecha</label>
                                <div class="col-sm-10">
                                    <input value="@if($punto ?? '') {{$punto->fecha_desde_antes}} @endif" type="data" name="fecha_antes" class="form-control form-control-lg" id="colFormLabelSm" placeholder="Introduzca la cantidad de personas" >
                                    
                                </div>
                            </div>
                                       <div class="form-group row  mb-4">
                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" >Supervisor</label>
                                <div class="col-sm-10">
                                    <input value="@if($punto ?? '') {{$punto->supervisor_id}} @endif" type="data" name="supervisor" class="form-control form-control-lg" id="colFormLabelSm" placeholder="Introduzca la cantidad de personas" >
                                    
                                </div>
                            </div>
                                <div class="form-group row  mb-4">
                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" >Precio Mensual</label>
                                <div class="col-sm-10">
                                    <input value="@if($punto ?? '') {{$punto->precio_mensual}} @endif" type="data" name="precio" class="form-control form-control-lg" id="colFormLabelSm" placeholder="Introduzca la cantidad de personas" >
                                    
                                </div>
                            </div>  <div class="form-group row  mb-4">
                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" >Descipción</label>
                                <div class="col-sm-10">
                                  
                                    <textarea name="descripcion" class="form-control mb-4" rows="3" id="descripcion">@if($punto ?? '') {{$punto->descripcion}} @endif</textarea>
                                </div>
                            </div>
                              
                          
                            
                            <input type="submit" class="mb-4 btn btn-primary" value="Modificar" >
                            <a href="{{url('listar_puntos')}}"> <input type="button" name="time" class="mb-4 btn btn-success" value="Cancelar" ></a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
    @endsection
    @section('codigos_especifico')
    <script>
        $(document).ready(function () {
            App.init();

          @if (Session::has('insertar_factura'))

                mostrar_notificacion("{{session('insertar_factura')}}");
      
        @endif



        }
        );
    </script>
    @endsection


