@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <h2>Listado de puntos de servicio</h2>
                   @if(Auth()->user()->role_id !== 5 ) <a href="{{url('usuarios/create')}}"><button class="btn-success btn" style="float: right;margin-right: 25px">Insertar Punto</button></a>@endif
                    <div class="table-responsive mb-4 mt-4">
                        <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>id_pto</th>
                                    <th>Nombre</th>
                                    <th>Dirección</th>
                                    <th>Teléfono</th>
                                    <th>Correo</th>
                                    <th>Folio</th>
                                    <th>Encargado</th>
                                    <th>Precio mensual</th>
                                    <th>Cantidad de Personas</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <div class="modal fade" id="editarModal" tabindex="-1" role="dialog" aria-labelledby="editarModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modificar Punto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group row  mb-4">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Nombre: </label>
                        <div class="col-sm-10">
                            <input id="i_id" type="nmber" name="id" class="form-control form-control-sm" id="colFormLabelSm" placeholder="col-form-label-sm" style="display: none">
                            <input id="i_nombre" type="text" name="nombre" class="form-control form-control-lg" id="colFormLabelSm" placeholder="Introduzca el nombre">
                        </div>
                    </div>
                    {{csrf_field()}}
                    <div class="form-group row mb-4">
                        <label for="colFormLabel" class="col-sm-2 col-form-label">Encargado </label>
                        <div class="col-sm-10">
                            <input id="i_encargado" type="text" name="cargo" class="form-control form-control-lg" id="colFormLabel" placeholder="Introduzca el cargo">
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Dirección</label>
                        <div class="col-sm-10">
                            <input id="i_direccion" type="text" name="direccion" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca la dirección">
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Teléfono</label>
                        <div class="col-sm-10">
                            <input id="i_telefono" type="number" name="telefono" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca el teléfono">
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Correo</label>
                        <div class="col-sm-10">
                            <input id="i_correo" type="email" name="correo" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca el correo">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">

                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancelar</button>
                    <button type="button" id="btn_modificar" class="btn btn-primary">Modificar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="eliminarModal" tabindex="-1" role="dialog" aria-labelledby="eliminarModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Eliminar  Punto de Servicio</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="modal-text">Seguro que desea eliminar el Punto de servicio:</p>
                    <form>
                        <input type="number" id="id_producto_eliminar" style="display: none">
                        <input type="number" id="id_punto_servicio" style="display: none">

                        <h2 id="nombre_producto"> </h2>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancelar</button>
                    <button id="btn_eliminar" type="button" class="btn btn-primary">Eliminar</button>
                </div>
            </div>
        </div>
    </div>


    @endsection
    @section('codigos_especifico')
    <script>



        $('#html5-extension').DataTable({
            dom: '<"row"<"col-md-12"<"row"<"col-md-6"l><"col-md-6"f> > >B<"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >',
            buttons: {
                buttons: [
                    {extend: 'copy', className: 'btn'},
                    {extend: 'csv', className: 'btn'},
                    {extend: 'excel', className: 'btn'},
                    {extend: 'print', className: 'btn'}
                ]
            },
            "oLanguage": {
                "oPaginate": {"sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'},
                "sInfo": "Mostrada pagina _PAGE_ de _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Buscar",
                "sLengthMenu": "Results :  _MENU_",
            },
            "columnDefs": [
                {
                    "targets": -1,
                    "data": null,
                    "searchable": false,
                    "defaultContent": '@if(Auth()->user()->role_id !== 5 )<a id="editar"   title="Editar"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></a><a id="eliminar"  title="Eliminar"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg></a> @endif <a id="ver_contrato" title="Ver contrato"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file"><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 9"></polyline></svg></a><a id="inventario" title="Ver Inventario"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg></a>'


                },
                {
                    "targets": [0],
                    "visible": false,
                    "data": 'id'
                },
                {
                    "targets": [1],
                    "visible": false,
                    "data": 'id_pto'
                },

                {
                    "targets": [2],
                    "data": 'nombre'
                },

                {
                    "targets": [3],
                    "data": 'direccion'
                },
                {
                    "targets": [4],
                    "data": 'telefono'
                },
                {
                    "targets": [5],
                    "data": 'correo'
                },
                {
                    "targets": [6],
                    "data": 'folio'
                }, {
                    "targets": [7],
                    "data": 'encargado'
                },
                {
                    "targets": [8],
                    "data": 'precio'
                },
                {
                    "targets": [9],
                    "data": 'cant_personas'
                },
            ],
            "stripeClasses": [],
            "lengthMenu": [10, 20, 50, 100, 1000000],
            "pageLength": 10,
            "processing": true,
            "serverSide": true,
            "ajax": "{{url('/puntos')}}"
        });
        $(document).ready(function () {

            App.init();
                    @if (Session::has('insertar_usuario'))

            mostrar_notificacion("{{session('insertar_usuario')}}");

            @endif

                    var table = $('#html5-extension').DataTable();
            $('#html5-extension tbody').on('click', '#eliminar', function () {
                var data = table.row($(this).parents('tr')).data();
                document.getElementById('id_producto_eliminar').value = data['id'];
                document.getElementById('id_punto_servicio').value = data['id_pto'];
                document.getElementById('nombre_producto').innerHTML = data['nombre'];
                $('#eliminarModal').modal('show');
            });

            $('#html5-extension tbody').on('click', '#editar', function () {
                var data = table.row($(this).parents('tr')).data();
                
                            location.href = 'puntos/' + data['id'] + '/edit';
            });
$('#html5-extension tbody').on('click', '#inventario', function () {
                var data = table.row($(this).parents('tr')).data();
                
                            location.href = 'inventario_punto/' + data['id'];
            });
            $('#html5-extension tbody').on('click', '#ver_contrato', function () {
                var data = table.row($(this).parents('tr')).data();
                
                            location.href = 'contratos/pto/ver/' + data['id'];
            });
            $('#eliminarModal').on('click', '#btn_eliminar', function (e) {

                e.preventDefault();
                var consulta = new XMLHttpRequest();
                if (!window.XMLHttpRequest) {
                    consulta = new ActiveXObject("Microsoft.XMLHTTP");
                }
                consulta.onreadystatechange = function () {
                    if (consulta.readyState === 4 && consulta.status === 200) {

                        var tabla = $("#html5-extension").DataTable();
                        tabla.ajax.reload();
                        mostrar_notificacion(consulta.responseText);
                        $('#eliminarModal').modal('hide');
                    }

                };
                consulta.open("DELETE", 'puntos/' + document.getElementById('id_producto_eliminar').value, true);
                consulta.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                // consulta.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                consulta.send();
            });







        }
        );

        function detalles_punto(id) {
            var consulta_m = new XMLHttpRequest();
            if (!window.XMLHttpRequest) {
                consulta_m = new ActiveXObject("Microsoft.XMLHTTP");
            }
            consulta_m.onreadystatechange = function () {
                if (consulta_m.readyState === 4 && consulta_m.status === 200) {


                }

            };






            consulta_m.open("GET", 'puntos/' + document.getElementById('i_id').value);
            consulta_m.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
            // consulta.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            consulta_m.send();

        }
    </script>

    @endsection

