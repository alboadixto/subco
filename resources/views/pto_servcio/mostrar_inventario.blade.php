@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">

            <div class="widget-content widget-content-area br-6">


                <div id="mini_balance" style="border-bottom-color: #000000;background-color: lavenderblush">
                    <h2>Inventario de Productos</h2>
                    <h2 style="align-self: center">Punto: {{$punto->name}}</h2>
                    <hr>
                    <p>Total de Tranferencias de Productos:<span id="t_productos">{{$cantidad}}</span>         <span style="margin-left: 100px">Precio total de productos:</span ><span id="td_productos">{{$total_precio_todos}}</span></p>
                    <p>Total de Insumos:<span id="t_insumos">{{$cant_insumo}}</span>           <span style="margin-left: 230px">Precio total de insumos:</span><span id="td_insumos">{{$total_precio_insumo}}</span></p>
                    <p>Total de maquinarías:<span id="t_maquinaria">{{$cant_maquinaria}}</span>       <span style="margin-left: 200px">Precio total de maquinarías:</span><span id="td_maquinaria">{{$total_precio_maquinaria}}</span></p>
                </div>
                <input type="button" onclick="imprimir_mini()" name="imprimir" class="mt-4 btn btn-primary" title="imprimir" value="Imprimir Mini Balance" style="width:180px;height: 40px">
                <a href="{{url('listar_puntos')}}"> <input type="button"  name="imprimir" class="mt-4 btn btn-success" title="Atras" value="Atras" style="width:180px;height: 40px"></a>
                <script>
                    function imprimir_mini() {
                        var text = document.getElementById('mini_balance').innerHTML;
                        var ventana = window.open('', 'popimp');
                        ventana.document.write(text);
                        ventana.print();

                    }</script>
                <a href="{{url('usuarios/create')}}"><button class="btn-success btn" style="float: right;margin-right: 25px">Insertar Punto</button></a>
                <div class="table-responsive mb-4 mt-4">
                    <table id="html5-extension" class="table table-hover non-hover" >
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>Cantidad</th>
                                <th>Codigo</th>
                                <th>Nombre</th>
                                <th>Tipo</th>
                                <th>Marca</th>
                                <th>Precio</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="row layout-top-spacing" id="cancel-row">

        </div>

    </div>

    <div class="modal fade" id="editarModal" tabindex="-1" role="dialog" aria-labelledby="editarModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cambiar cantidad</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="number" id="id_producto_mod" style="display: none">
                    {{csrf_field()}}
                    <div class="form-group row mb-4">
                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Cantidad</label>
                        <div class="col-sm-10">
                            <input id="cant_mod" type="number" name="cant_mod" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca la cantidad">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">

                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancelar</button>
                    <button type="button" id="btn_modificar" class="btn btn-primary">Modificar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="eliminarModal" tabindex="-1" role="dialog" aria-labelledby="eliminarModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Eliminar  Punto de Servicio</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="modal-text">Seguro que desea eliminar esta producto del punto:</p>
                    <form>
                        <input type="number" id="id_producto_eliminar" style="display: none">
                        <input type="number" id="id_punto_servicio" style="display: none">

                        <h2 id="nombre_producto"> </h2>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancelar</button>
                    <button id="btn_eliminar" type="button" class="btn btn-primary">Eliminar</button>
                </div>
            </div>
        </div>
    </div>


    @endsection
    @section('codigos_especifico')
    <script>



        $('#html5-extension').DataTable({
            dom: '<"row"<"col-md-12"<"row"<"col-md-6"l><"col-md-6"f> > >B<"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >',
            buttons: {
                buttons: [
                    {extend: 'copy', className: 'btn'},
                    {extend: 'csv', className: 'btn'},
                    {extend: 'excel', className: 'btn'},
                    {extend: 'print', className: 'btn'}
                ]
            },
            "oLanguage": {
                "oPaginate": {"sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'},
                "sInfo": "Mostrada pagina _PAGE_ de _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Buscar",
                "sLengthMenu": "Results :  _MENU_",
            },
            "columnDefs": [
                {
                    "targets": -1,
                    "data": null,
                    "searchable": false,
                    "defaultContent": '@if(Auth()->user()->role_id !== 5 )<a id="editar"   title="Editar"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></a><a id="eliminar"  title="Eliminar"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg></a>@endif'


                },
                {
                    "targets": [0],
                    "visible": false,
                    "data": 'id'
                },

                {
                    "targets": [1],
                    "data": 'cantidad'
                },

                {
                    "targets": [2],
                    "data": 'codigo'
                },
                {
                    "targets": [3],
                    "data": 'nombre'
                },
                {
                    "targets": [4],
                    "data": 'tipo'
                },
                {
                    "targets": [5],
                    "data": 'marca'
                },
                {
                    "targets": [6],
                    "data": 'precio'
                },
                {
                    "targets": [7],
                    "data": 'estado'
                }
            ],
            "stripeClasses": [],
            "lengthMenu": [10, 20, 50, 100, 1000000],
            "pageLength": 10,
            "processing": true,
            "serverSide": true,
            "ajax": "{{url('inventario/')}}/{{$punto->id}}"
        });
        $(document).ready(function () {

            App.init();
                    @if (Session::has('insertar_usuario'))

            mostrar_notificacion("{{session('insertar_usuario')}}");

            @endif
                    var table = $('#html5-extension').DataTable();
            $('#html5-extension tbody').on('click', '#eliminar', function () {
                var data = table.row($(this).parents('tr')).data();
                document.getElementById('id_producto_eliminar').value = data['id'];
                document.getElementById('id_punto_servicio').value = data['id_pto'];
                document.getElementById('nombre_producto').innerHTML = data['nombre'];
                $('#eliminarModal').modal('show');
            });

            $('#html5-extension tbody').on('click', '#editar', function () {
                var data = table.row($(this).parents('tr')).data();
                document.getElementById('id_producto_mod').value = data['id'];
                document.getElementById('cant_mod').value = data['cantidad'];
                $('#editarModal').modal('show');
            });

            $('#editarModal').on('click', '#btn_modificar', function (e) {

                e.preventDefault();
                var consulta = new XMLHttpRequest();
                if (!window.XMLHttpRequest) {
                    consulta = new ActiveXObject("Microsoft.XMLHTTP");
                }
                consulta.onreadystatechange = function () {
                    if (consulta.readyState === 4 && consulta.status === 200) {

                        var tabla = $("#html5-extension").DataTable();
                        tabla.ajax.reload();
                        mostrar_notificacion(consulta.responseText);
                        $('#editarModal').modal('hide');
                    }

                };
                consulta.open("PUT", '../inventario/mod_cant/' + document.getElementById('id_producto_mod').value + '/?cant=' + document.getElementById('cant_mod').value, true);
                consulta.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                // consulta.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                consulta.send();
            });

            $('#eliminarModal').on('click', '#btn_eliminar', function (e) {

                e.preventDefault();
                var consulta = new XMLHttpRequest();
                if (!window.XMLHttpRequest) {
                    consulta = new ActiveXObject("Microsoft.XMLHTTP");
                }
                consulta.onreadystatechange = function () {
                    if (consulta.readyState === 4 && consulta.status === 200) {

                        var tabla = $("#html5-extension").DataTable();
                        tabla.ajax.reload();
                        mostrar_notificacion(consulta.responseText);
                        $('#eliminarModal').modal('hide');
                    }

                };
                consulta.open("DELETE", '../inventario/quitar_prod_inv/' + document.getElementById('id_producto_eliminar').value, true);
                consulta.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                // consulta.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                consulta.send();
            });

        }
        );

        function detalles_punto(id) {
            var consulta_m = new XMLHttpRequest();
            if (!window.XMLHttpRequest) {
                consulta_m = new ActiveXObject("Microsoft.XMLHTTP");
            }
            consulta_m.onreadystatechange = function () {
                if (consulta_m.readyState === 4 && consulta_m.status === 200) {


                }

            };






            consulta_m.open("GET", 'puntos/' + document.getElementById('i_id').value);
            consulta_m.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
            // consulta.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            consulta_m.send();

        }
    </script>

    @endsection

