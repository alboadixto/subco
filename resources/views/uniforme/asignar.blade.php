@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <h2>Asignar uniforme a : {{$user->name}}</h2>
                    <div class="modal-body">
                        <form action="{{url('/uniforme/empleado/asignar')}}" method="POST">
                            <div class="form-group row  mb-4">
                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Fecha de Asignación</label>
                                <div class="col-sm-10">
                                    <input type="number" value="{{$user->id}}" name="empleado_id" class="form-control form-control-lg" id="colFormLabelSm" style="display: none" placeholder="Introduzca la talla de pantalón" required>
                                    <input type="date" name="fecha" class="form-control form-control-lg" id="colFormLabelSm" placeholder="Introduzca la talla de pantalón" required>
                                </div>
                            </div>
                            <div class="form-group row  mb-4">
                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Cantidad de Pantalones</label>
                                <div class="col-sm-10">
                                    <input onchange="total_monto();" onkeyup="total_monto()" type="number" name="pantalon_cant" id="pantalon_cant" class="form-control form-control-lg" id="colFormLabelSm" placeholder="Introduzca la cantidad de pantalones" required>
                                </div>
                            </div>
                            {{csrf_field()}}



                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Precio de Pantalones</label>
                                <div class="col-sm-10">
                                    <input onchange="total_monto()" onkeyup="total_monto()" type="number" name="pantalon_precio" id="pantalon_precio" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca el precio del Pantalón" required>
                                </div>
                            </div>


                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Cantidad de Poleras</label>
                                <div class="col-sm-10">
                                    <input onchange="total_monto()" onkeyup="total_monto()" type="number" name="polera_cant" id="polera_cant" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca la cantidad de poleras" required>
                                </div>
                            </div>
                            
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Precio de Poleras</label>
                                <div class="col-sm-10">
                                    <input onchange="total_monto()" onkeyup="total_monto()" type="number" name="polera_precio" id="polera_precio" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca la cantidad de poleras" required>
                                </div>
                            </div>


                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Cantidad de delantares</label>
                                <div class="col-sm-10">
                                    <input onchange="total_monto()" onkeyup="total_monto()" type="number" name="delantar_cant"  id="delantar_cant" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca la cantidad de delantares" required>
                                </div>
                            </div>

                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Precio de delantares</label>
                                <div class="col-sm-10">
                                    <input onchange="total_monto()" onkeyup="total_monto()" type="number" name="delantal_precio" id="delantal_precio" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca el precio de los delantares" required>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Cantidad de zapatatos</label>
                                <div class="col-sm-10">
                                    <input onchange="total_monto()" onkeyup="total_monto()" type="number" name="zapato_cant" id="zapato_cant" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca la cantidad de zapatos" required>
                                </div>
                            </div>

                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Precio de zapatos</label>
                                <div class="col-sm-10">
                                    <input onchange="total_monto()" onkeyup="total_monto()" type="number" name="zapato_precio" id="zapato_precio" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca el precio de los zapatos" required>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Cantidad de polares</label>
                                <div class="col-sm-10">
                                    <input onchange="total_monto()" onkeyup="total_monto()" type="number" name="polar_cant" id="polar_cant" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca la cantidad de polares" required>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Precio de polares</label>
                                <div class="col-sm-10">
                                    <input onchange="total_monto()" onkeyup="total_monto()" type="number" name="polar_precio" id="polar_precio" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca el precio de los polares" required>
                                </div>
                            </div>

                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Monto total</label>
                                <div class="col-sm-10">
                                    <input onchange="total_monto()" onkeyup="total_monto()" type="number" name="total" id="total" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca el monto total" required>
                                </div>
                            </div>
                            <input type="submit"  class="mb-4 btn btn-primary" value="Agregar" >
                            <a href="{{url('listar_facturas')}}"> <input type="button" name="time" class="mb-4 btn btn-success" value="Cancelar" ></a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
         
        function  total_monto()
        {
            var pantalon_cant = document.getElementById('pantalon_cant').value;
               var pantalon_precio = document.getElementById('pantalon_precio').value;
              var polera_cant = document.getElementById('polera_cant').value;
               var polera_precio = document.getElementById('polera_precio').value;
                var delantar_cant = document.getElementById('delantar_cant').value;
               var delantar_precio = document.getElementById('delantal_precio').value;
               var zapato_cant = document.getElementById('zapato_cant').value;
              var zapato_precio = document.getElementById('zapato_precio').value;
              var polar_cant = document.getElementById('polar_cant').value;
             var polar_precio = document.getElementById('polar_precio').value;
             var t_total = (pantalon_cant*pantalon_precio)+(polar_cant*polar_precio)+(delantar_cant*delantar_precio)+(zapato_cant*zapato_precio)+(polera_cant*polera_precio);
             var total = document.getElementById('total').value=t_total;


        }
    </script>


    @endsection
    @section('codigos_especifico')
    <script>
        $(document).ready(function () {
        App.init();
                @if (Session::has('mensaje'))
                mostrar_notificacion("{{session('mensaje')}}");
                @endif
        });
    </script>
    @endsection


