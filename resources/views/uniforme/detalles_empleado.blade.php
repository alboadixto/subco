@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <h2>Detalles de vestuario</h2>
                    <h5>Empleado : {{$user->name}}</h5>
                    @if($tallas ?? '')
                    <h6>Talla de Pantalón: {{ $tallas->pantalon }}</h6>
                    <h6>Talla de Polera: {{ $tallas->polera }}</h6>
                    <h6>Talla de Delantal: {{ $tallas->delantal }}</h6>
                    <h6>Talla de Zapatos: {{ $tallas->zapato }}</h6>
                    <h6>Talla de Polar: {{ $tallas->polar }}</h6>
                    @else
                    <h4 style="color: red">Aun no define sus tallas</h4>       
                    @endif           
                    <div class="table-responsive mb-4 mt-4">
                        <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                            <thead>
                                <tr>
                                    <th>fecha</th>
                                    <th>Pantalón(Cantidad)</th>
                                    <th>Pantalón(Precio)</th>
                                    <th>Polera(Cantidad)</th>
                                    <th>Poleta(Precio)</th>
                                    <th>Delantar(Cantidad)</th>
                                    <th>Delantar(Precio)</th>
                                    <th>Zapato(Cantidad)</th>
                                    <th>Zapato(Precio)</th>
                                    <th>Polar(Cantidad)</th>
                                    <th>Polar(Precio)</th>
                                    <th>Total</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($uniforme as $u)
                                <tr><td>{{$u->fecha}}</td>
                                    <td>{{$u->pantalon_cant}}</td>
                                    <td>{{$u->pantalon_precio}}</td>
                                    <td>{{$u->polera_cant}}</td>
                                    <td>{{$u->polera_precio}}</td>
                                    <td>{{$u->delantar_cant}}</td>
                                    <td>{{$u->delantal_precio}}</td>
                                    <td>{{$u->zapato_cant}}</td>
                                    <td>{{$u->zapato_precio}}</td>
                                    <td>{{$u->polar_cant}}</td>
                                    <td>{{$u->polar_precio}}</td>
                                    <td>{{$u->total}}</td>
                                    <td>@if(Auth()->user()->role_id !== 2 && Auth()->user()->role_id !== 5 )<a href="{{url('/uniforme/empleado/eliminar/'.$u->id)}}" id="eliminar" title="Eliminar uniforme"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg></a>@endif</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @section('codigos_especifico')
    <script>

        function mostrar_notificacion(mensaje) {
            Snackbar.show({
                showAction: false,
                text: mensaje,
            });
        }

        $('#html5-extension').DataTable({
            dom: '<"row"<"col-md-12"<"row"<"col-md-6"l><"col-md-6"f> > >B<"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >',
            buttons: {
                buttons: [
                    {extend: 'copy', className: 'btn'},
                    {extend: 'csv', className: 'btn'},
                    {extend: 'excel', className: 'btn'},
                    {extend: 'print', className: 'btn'}
                ]
            },
            "oLanguage": {
                "oPaginate": {"sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'},
                "sInfo": "Mostrada pagina _PAGE_ de _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Buscar",
                "sLengthMenu": "Results :  _MENU_",
            },

            "stripeClasses": [],
            "lengthMenu": [10, 20, 50, 100, 1000000],
            "pageLength": 10,

        });
        $(document).ready(function () {

            App.init();
                    @if (Session::has('mensaje'))

            mostrar_notificacion("{{session('mensaje')}}");

            @endif

                    var table = $('#html5-extension').DataTable();
            $('#html5-extension tbody').on('click', '#eliminar', function () {
                var data = table.row($(this).parents('tr')).data();
                document.getElementById('id_producto_eliminar').value = data['id'];
                location.href = "{{url('')}}"
            });

        }
        );
    </script>

    @endsection

