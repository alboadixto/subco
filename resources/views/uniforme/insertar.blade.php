@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <h2>Informar Tallas de Uniforme</h2>
                    <div class="modal-body">
                        <form action="{{url('uniforme')}}" method="POST">
                            <div class="form-group row  mb-4">
                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Talla de Pantalón</label>
                                <div class="col-sm-10">
                                    <input type="text" name="pantalon" class="form-control form-control-lg" id="colFormLabelSm" placeholder="Introduzca la talla de pantalón" required>
                                </div>
                            </div>
                            <div class="form-group row  mb-4">
                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Talla de Polera</label>
                                <div class="col-sm-10">
                                    <input type="text" name="polera" class="form-control form-control-lg" id="colFormLabelSm" placeholder="Introduzca la talla de polera" required>
                                </div>
                            </div>
                            {{csrf_field()}}
                          
                       
                            
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Talla de Delantal</label>
                                <div class="col-sm-10">
                                    <input type="text" name="delantal" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca la talla del delantal" required>
                                </div>
                            </div>
                            
                            
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Número de Zapatos</label>
                                <div class="col-sm-10">
                                    <input type="text" name="zapato" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca el número de zapatos" required>
                                </div>
                            </div>
                                                
                          
                        <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Talla de Polar</label>
                                <div class="col-sm-10">
                                    <input type="text" name="polar" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca la talla de el polar" required>
                                </div>
                            </div>
                           
                          
                            
                            <input type="submit" name="time" class="mb-4 btn btn-primary" value="Agregar" >
                            <a href="{{url('listar_facturas')}}"> <input type="button" name="time" class="mb-4 btn btn-success" value="Cancelar" ></a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
    @endsection
    @section('codigos_especifico')
    <script>
        $(document).ready(function () {
            App.init();

          @if (Session::has('mensaje'))

                mostrar_notificacion("{{session('mensaje')}}");
      
        @endif



        }
        );
    </script>
    @endsection


