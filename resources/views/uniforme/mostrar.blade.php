@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <h2>Listado de Empleados y Tallas de Ropa</h2>
                    
                    <div class="table-responsive mb-4 mt-4">
                        <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>nombre</th>
                                    <th>pantalon</th>
                                    <th>polera</th>
                                    <th>delantal</th>
                                    <th>zapato</th>
                                    <th>polar</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>


    @endsection
    @section('codigos_especifico')
    <script>

        function mostrar_notificacion(mensaje) {
            Snackbar.show({
                showAction: false,
                text: mensaje,
            });
        }

        $('#html5-extension').DataTable({
            dom: '<"row"<"col-md-12"<"row"<"col-md-6"l><"col-md-6"f> > >B<"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >',
            buttons: {
                buttons: [
                    {extend: 'copy', className: 'btn'},
                    {extend: 'csv', className: 'btn'},
                    {extend: 'excel', className: 'btn'},
                    {extend: 'print', className: 'btn'}
                ]
            },
            "oLanguage": {
                "oPaginate": {"sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'},
                "sInfo": "Mostrada pagina _PAGE_ de _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Buscar",
                "sLengthMenu": "Results :  _MENU_",
            },
            "columnDefs": [
                {
                    "targets": -1,
                    "data": null,
                    "searchable": false,
                    "defaultContent": '@if(Auth()->user()->role_id !== 5)<a id="ver" title="Detalles"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg></a>@if(Auth()->user()->role_id !== 2 && Auth()->user()->role_id !== 5 )<a id="asignar_uniforme" title="Asignar Uniforme"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg></a>@endif @endif'
                },
                {
                    "targets": [0],
                    "visible": false,
                    "data": 'id_empleado',
                },
                {
                    "targets": [1],
                    "data": 'nombre'
                },
                {
                    "targets": [2],
                    "data": 'pantalon'
                },
                {
                    "targets": [3],
                    "data": 'polera'
                },
                {
                    "targets": [4],
                    "data": 'delantal'
                },
                {
                    "targets": [5],
                    "data": 'zapato'
                },
                {
                    "targets": [6],
                    "data": 'polar'
                },
            ],
            "stripeClasses": [],
            "lengthMenu": [10, 20, 50, 100, 1000000],
            "pageLength": 10,
            "processing": true,
            "serverSide": true,
            "ajax": "{{url('/uniforme/ver/empleados_tallas_json')}}"
        });
        $(document).ready(function () {

            App.init();
                    @if (Session::has('mensaje'))

            mostrar_notificacion("{{session('mensaje')}}");

            @endif

                    var table = $('#html5-extension').DataTable();
            $('#html5-extension tbody').on('click', '#ver', function () {
                var data = table.row($(this).parents('tr')).data();
             
                location.href = "{{url('uniforme/')}}" + '/' + data['id_empleado'];
            });
              $('#html5-extension tbody').on('click', '#asignar_uniforme', function () {
                var data = table.row($(this).parents('tr')).data();
             
                location.href = "{{url('/uniforme/asignar/')}}" + '/' + data['id_empleado'];
            });

        }
        );
    </script>

    @endsection

