@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <h2>Listado de gastos de uniforme</h2>

                    <div class="table-responsive mb-4 mt-4">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>Fecha Inicial:</td>
                                    <td><input type="date" id="min" class="form-control" name="min"  ></td>
                                </tr>
                                <tr>
                                    <td>Fecha Final:</td>
                                    <td><input type="date" id="max" class="form-control" name="max" ></td>
                                </tr>
                            </tbody>
                        </table>
                        <table id="range-search" class="display table table-hover" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Fecha</th>
                                    <th>Empleado</th>
                                    <th>Pantalones</th>
                                    <th>Precio pantalones</th>
                                    <th>Poleras</th>
                                    <th>Precio polera</th>
                                    <th>Delantares</th>
                                    <th>Precio Delantar</th>
                                    <th>Zapatos</th>
                                    <th>Precio Zapato</th>
                                    <th>Polares</th>
                                    <th>Precio Polar</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($out as $out_)
                                <tr>
                                    <td>{{$out_->fecha}}</td>
                                    <td>{{$out_->empleado_id}}</td>
                                    <td>{{$out_->pantalon_cant}}</td>
                                    <td>{{$out_->pantalon_precio}}</td>
                                    <td>{{$out_->polera_cant}}</td>
                                    <td>{{$out_->polera_precio}}</td>
                                    <td>{{$out_->delantar_cant}}</td>
                                    <td>{{$out_->delantal_precio}}</td>
                                    <td>{{$out_->zapato_cant}}</td>
                                    <td>{{$out_->zapato_precio}}</td>
                                    <td>{{$out_->polar_cant}}</td>
                                    <td>{{$out_->polar_precio}}</td>
                                    <td>{{$out_->total}}</td>

                                </tr>
                                @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
                <div class="card component-card_3" >
                    <div class="card-body" id="body_impirmir">
                        <img src="{{asset('img/logo.png')}}" class="img-preview"  >
                        <h5 class="card-user_name">Cantidad de Perdidas: <span id="cant_perdidas"></span></h5>
                        <h5 class="card-user_name">Monto Total Perdidas: <span id="monto_perdidas"></span></h5>
                        <h5 class="card-user_name">Promedio Perdidas: <span id="promedio"></span></h5>
                        <div class="card-star_rating">
                            <p>Fecha inicio:<span id="fecha_inicio"></span></p>
                            <p>Fecha fin:<span id="fecha_fin"></span></p>

                        </div>

                    </div>
                    <center><button onclick="imprimir()" id="imprimir" class="btn btn-success mb-2" >Imprimir</button></center>
                </div>
            </div>

        </div>

    </div>

    <script>
        function imprimir()
        {
            var text = document.getElementById('body_impirmir').innerHTML;
            var ventana = window.open('', 'todo');
            ventana.document.write(text);
            ventana.print();
        }

    </script>

    <div class="modal fade" id="informarModal" tabindex="-1" role="dialog" aria-labelledby="informarModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Informar Perdida:</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="modal-text">Producto:</p>
                    <h3 id="prod_info"></h3>
                    <form>
                        <input type="number" id="id_prod_informar" style="display: none">

                        <h2 id="nombre_producto"> </h2>
                        <p>Motivo, Razón o Comentario(Opcional)</p>
                        <textarea class="form-control" id="descripcion_informar" rows="3"></textarea>      
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancelar</button>
                    <button id="btn_informar" type="button" class="btn btn-primary">Informar</button>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @section('codigos_especifico')
    <script>
        $(document).ready(function () {
            App.init();
        });
    </script>


    <script>
        $.fn.dataTable.Api.register('column().data().sum()', function () {
            return this.reduce(function (a, b) {
                var x = parseFloat(a) || 0;
                var y = parseFloat(b) || 0;
                return x + y;
            });
        });
        /* Custom filtering function which will search data in column four between two values */
       $.fn.dataTableExt.afnFiltering.push(
	function( oSettings, aData, iDataIndex ) {
		var iFini = document.getElementById('min').value;
		var iFfin = document.getElementById('max').value;
		var iStartDateCol = 0;
		var iEndDateCol = 0;

		iFini=iFini.substring(6,10) + iFini.substring(3,5)+ iFini.substring(0,2);
		iFfin=iFfin.substring(6,10) + iFfin.substring(3,5)+ iFfin.substring(0,2);

		var datofini=aData[iStartDateCol].substring(6,10) + aData[iStartDateCol].substring(3,5)+ aData[iStartDateCol].substring(0,2);
		var datoffin=aData[iEndDateCol].substring(6,10) + aData[iEndDateCol].substring(3,5)+ aData[iEndDateCol].substring(0,2);

		if ( iFini === "" && iFfin === "" )
		{
			return true;
		}
		else if ( iFini <= datofini && iFfin === "")
		{
			return true;
		}
		else if ( iFfin >= datoffin && iFini === "")
		{
			return true;
		}
		else if (iFini <= datofini && iFfin >= datoffin)
		{
			return true;
		}
		return false;
	}
);
        $(document).ready(function () {
            var table = $('#range-search').DataTable({
                "oLanguage": {
                    "oPaginate": {"sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'},
                    "sInfo": "Showing page _PAGE_ of _PAGES_",
                    "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                    "sSearchPlaceholder": "Search...",
                    "sLengthMenu": "Results :  _MENU_",
                },
                "stripeClasses": [],
                "lengthMenu": [7, 10, 20, 50],
                "pageLength": 7,

            });
            // Event listener to the two range filtering inputs to redraw on input
            $('#min, #max').change(function () {
                var table = $('#range-search').DataTable();
table.draw();
              document.getElementById('monto_perdidas').innerHTML = table.column(12, {page: 'current'}).data().sum();
               document.getElementById('cant_perdidas').innerHTML = table.column(12, {page: 'current'}).data().count();
               document.getElementById('fecha_inicio').innerHTML = document.getElementById('min').value;
               document.getElementById('fecha_fin').innerHTML = document.getElementById('max').value;
               document.getElementById('promedio').innerHTML = parseFloat(document.getElementById('monto_perdidas').innerHTML) / parseFloat(document.getElementById('cant_perdidas').innerHTML);
                
            });
        });
        
         
    </script>

    @endsection

