@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <h2>Insertar  Usuarios</h2>
                    <div class="modal-body">
                        <form action="{{url('usuarios')}}" method="POST">
                            <div class="form-group row  mb-4">
                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Nombre</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control form-control-lg" id="colFormLabelSm" placeholder="Introduzca el nombre" required>
                                </div>
                            </div>
                            {{csrf_field()}}
                            <div class="form-group row mb-4">
                                <label for="colFormLabel" class="col-sm-2 col-form-label">Rol</label>
                                <div class="col-sm-10">
                                    <select name="role_id" class="placeholder js-states form-control" required="required" onchange="mostrar_input(this.value)">
                                        <option   disabled="disable" selected >Seleccionar Rol</option>
                                        <option value="1">Administrador</option>
                                        <option value="2">Supervisor</option>
                                        <option value="3">Cliente</option>
                                        <option value="4">Contador</option>
                                        <option value="5">Punto de Servicio</option>
                                        <option value="6">Proveedor</option>
                                        <option value="7">Empleado</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Cargo</label>
                                <div class="col-sm-10">
                                    <input type="text" name="cargo" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca el cargo" required="">
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">RUT</label>
                                <div class="col-sm-10">
                                    <input type="text" name="rut" class="form-control form-control-lg" id="rut" placeholder="Introduzca el rut" required="">
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Dirección</label>
                                <div class="col-sm-10">
                                    <input type="text" name="direccion" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca dirección" required>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Teléfono</label>
                                <div class="col-sm-10">
                                    <input type="tel" name="telefono" class="form-control form-control-lg" id="telefono" placeholder="Introduzca el teléfono" required>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Correo</label>
                                <div class="col-sm-10">
                                    <input type="email" name="email" class="form-control form-control-lg" id="email" placeholder="Introduzca la correo" required>
                                </div>
                            </div>
                            <!-- Inputs especificos de clientes -->
                            <div id="input_actividad" class="form-group row mb-4" style="display: none">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Actividad</label>
                                <div class="col-sm-10">
                                    <input type="text" name="actividad" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca la actividad" >
                                </div>
                            </div>
                            <div id="input_rubro" class="form-group row mb-4" style="display: none">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Rubro</label>
                                <div class="col-sm-10">
                                    <input type="text" name="rubro" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca el rubro" >
                                </div>
                            </div>
                            <!-- Inputs especificos de Proveedor -->
                            <div id="input_contacto" class="form-group row mb-4" style="display: none">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Contacto</label>
                                <div class="col-sm-10">
                                    <input type="text" name="contacto" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca el Contacto" >
                                </div>
                            </div>


                            <!-- Inputs especificos de punto de sercvicio -->
                            <div id="input_folio" class="form-group row mb-4" style="display: none">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Folio</label>
                                <div class="col-sm-10">
                                    <input type="text" name="folio" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca el folio" >
                                </div>
                            </div>
                            <div id="input_encargado" class="form-group row mb-4" style="display: none">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Encargado</label>
                                <div class="col-sm-10">
                                    <input type="text" name="encargado" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca el nombre del encargado" >
                                </div>
                            </div>
                            <div id="input_cant_personas" class="form-group row mb-4" style="display: none">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Cantidad de Personas</label>
                                <div class="col-sm-10">
                                    <input type="number" name="cant_personas" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca la cantidad de personas"  >
                                </div>
                            </div>
                            <div id="input_fecha_antes" class="form-group row mb-4" style="display: none">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Fecha desde antes</label>
                                <div class="col-sm-10">
                                    <input type="date" name="fecha_antes" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca la fecha"  >
                                </div>
                            </div>
                            <div id="input_supervisor" class="form-group row mb-4" style="display: none">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Supervisor</label>
                                <div class="col-sm-10">
                                    <input type="number" name="supervisor" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca el supervisor"  >
                                </div>
                            </div>
                            <div id="input_descripcion" class="form-group row mb-4" style="display: none">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Descripcion</label>
                                <div class="col-sm-10">
                                    <textarea name="descripcion" class="form-control mb-4" rows="3" id="descripcion"></textarea>
                                </div>
                            </div>
                            <div id="input_precio" class="form-group row mb-4" style="display: none">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Precio Mensual</label>
                                <div class="col-sm-10">
                                    <input type="number" name="precio" class="form-control form-control-lg" id="precio_mensual" placeholder="Introduzca el precio mensual"  >
                                </div>
                            </div>
                            <!-- Inputs especificos de contraseña -->
                            <div class="form-group row mb-4">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Contraseña</label>
                                <div class="col-sm-10">
                                    <input type="password" name="password" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca la contraseña" required>
                                </div>
                            </div>

                            <!-- Inputs especificos de empleado -->

                            <div id="input_sueldo_base" class="form-group row mb-4" style="display: none">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Sueldo base</label>
                                <div class="col-sm-10">
                                    <input type="number" name="empleado_sueldo_base" class="form-control form-control-lg" id="empleado_sueldo_base" placeholder="Introduzca el sueldo base"  >
                                </div>
                            </div>
                            <div id="input_fecha_contrato" class="form-group row mb-4" style="display: none">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Fecha del contrato</label>
                                <div class="col-sm-10">
                                    <input type="date" name="empleado_fecha_contrato" class="form-control form-control-lg" id="empleado_fecha_contrato" placeholder="Introduzca el precio mensual"  >
                                </div>
                            </div>
                            <div id="input_tipo_contrato" class="form-group row mb-4" style="display: none">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Tipo de Contrato</label>
                                <div class="col-sm-10">
                                    <select name="empleado_tipo_contrato" class="placeholder js-states form-control" required="required" >
                                        <option   value="Fijo" selected >Fijo</option>
                                        <option value="Temporal">Temporal</option>
                                        <option value="Suplencia">Suplencia</option>

                                    </select>

                                </div>
                            </div>
                            <div id="input_costo_movilizacion" class="form-group row mb-4" style="display: none">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Costo de Movilizacion</label>
                                <div class="col-sm-10">
                                    <input type="number" name="empleado_costo_movilizacion" class="form-control form-control-lg" id="empleado_costo_movilizacion" placeholder="Introduzca el precio de movilización"  >
                                </div>
                            </div>
                            <div id="input_costo_colocación" class="form-group row mb-4" style="display: none">
                                <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Costo Colocación</label>
                                <div class="col-sm-10">
                                    <input type="number" name="costo_colocación" class="form-control form-control-lg" id="empleado_costo_colocación" placeholder="Introduzca el precio de movilización"  >
                                </div>
                            </div>
                            <input type="submit" name="time" class="mb-4 btn btn-primary" value="Agregar" onclick="return correo_valid()" >
                            <a href="{{url('listar_usuarios')}}"> <input type="button" name="time" class="mb-4 btn btn-success" value="Cancelar" ></a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function  mostrar_input(id) {
            document.getElementById('input_actividad').style.display = 'none';
            document.getElementById('input_rubro').style.display = 'none';
            document.getElementById('input_contacto').style.display = 'none';
            document.getElementById('input_folio').style.display = 'none';
            document.getElementById('input_encargado').style.display = 'none';
            document.getElementById('input_cant_personas').style.display = 'none';
            document.getElementById('input_fecha_antes').style.display = 'none';
            document.getElementById('input_supervisor').style.display = 'none';
            document.getElementById('input_descripcion').style.display = 'none';
            document.getElementById('input_precio').style.display = 'none';

            document.getElementById('input_sueldo_base').style.display = 'none';
            document.getElementById('input_fecha_contrato').style.display = 'none';
            document.getElementById('input_tipo_contrato').style.display = 'none';
            document.getElementById('input_costo_colocación').style.display = 'none';
            document.getElementById('input_costo_movilizacion').style.display = 'none';
            if (id == 7) {
                //mostrar campos de empleados

                document.getElementById('input_sueldo_base').style.display = '';
                document.getElementById('input_fecha_contrato').style.display = '';
                document.getElementById('input_tipo_contrato').style.display = '';
                document.getElementById('input_costo_colocación').style.display = '';
                document.getElementById('input_costo_movilizacion').style.display = '';
            } else
            if (id == 3) {
                //mostrar campos de clientes
                document.getElementById('input_actividad').style.display = '';
                document.getElementById('input_rubro').style.display = '';
            } else
            if (id == 6)
            {// mostrar campos de proveedor
                document.getElementById('input_contacto').style.display = '';

            } else
            if (id == 5) {
                //mostrar campos de punto de servicio
                document.getElementById('input_folio').style.display = '';
                document.getElementById('input_encargado').style.display = '';
                document.getElementById('input_cant_personas').style.display = '';
                document.getElementById('input_fecha_antes').style.display = '';
                document.getElementById('input_supervisor').style.display = '';
                document.getElementById('input_descripcion').style.display = '';
                document.getElementById('input_precio').style.display = '';
            }
        }

    </script>
    @endsection
    @section('codigos_especifico')
    <script>
        function correo_valid() {

            if (document.getElementById('email').value.substr(-10) !== '@gmail.com') {
                mostrar_notificacion('El correo debe ser de gmail para ingresar el usuario al sistema');
                return false;
            } else {
                return true;
            }

        }

        $(document).ready(function () {
        App.init();
                $('#telefono').inputmask("+56999999999");
                $('#rut').inputmask("999999999-0|k");
                @if (Session::has('insertar_usuario'))

                mostrar_notificacion("{{session('insertar_usuario')}}");
                @endif



        }
        );
    </script>
    @endsection


