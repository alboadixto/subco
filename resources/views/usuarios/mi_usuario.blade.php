@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <h2>Mi usuario: {{Auth::user()->name}}</h2>
                    <hr>
                    <table>
                        <tr>
                            <td style="padding-left: 10px;padding-right: 60px"><p style="font-size: 20px">Cargo: <span style="color: blue">{{Auth::user()->cargo}}</span></p></td>
                            <td style="padding-left: 10px;padding-right:60px"><p style="font-size: 20px">Dirección: <span style="color: blue">{{Auth::user()->direccion}}</span></p></td>
                            <td style="padding-left: 10px;padding-right:60px"><p style="font-size: 20px">Telefono: <span style="color: blue">{{Auth::user()->telefono}}</span></p></td>
                        </tr>
                        <tr>
                            <td style="padding-left: 10px;padding-right: 60px"><p style="font-size: 20px">Correo: <span style="color: blue">{{Auth::user()->email}}</span></p></td>
                            <td style="padding-left: 10px;padding-right: 60px"><p style="font-size: 20px">Rol: <span style="color: blue">{{Auth::user()->role->nombre}}</span></p></td>
                        </tr>
                    </table>
                    <button class="btn btn-primary mb-2" onclick="$('#passModal').modal('show');")>Cambiar Contraseña</button>





                </div>
            </div>

        </div>

    </div>




    <div class="modal fade" id="passModal" tabindex="-1" role="dialog" aria-labelledby="passModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cambiar Contraseña</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="modal-text" style="font-size: 20px">Usuario:<span id="nombre_usuario_pass"></span></p>
                    <form>
                        <input type="number" id="id_usuario_pass" style="display: none" value="{{Auth::user()->id}}">
                        Nueva Contraseña:
                        <input type="password" id="pass_new" class="form-control form-control-lg" placeholder="Introduzca la nueva contraseña">

                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancelar</button>
                            <button id="btn_pass" type="button"  onclick="cambiar_pass()" class="btn btn-primary">Cambiar</button>
                        </div>
                </div>
            </div>
        </div>

        <script>function cambiar_pass() {
                var consulta = new XMLHttpRequest();
                if (!window.XMLHttpRequest) {
                    consulta = new ActiveXObject("Microsoft.XMLHTTP");
                }
                consulta.onreadystatechange = function () {
                    if (consulta.readyState === 4 && consulta.status === 200) {

                        var tabla = $("#html5-extension").DataTable();
                        tabla.ajax.reload();
                        mostrar_notificacion(consulta.responseText);
                        $('#passModal').modal('hide');
                        
                    }

                };
                consulta.open("PUT", 'usuarios_pass/' + document.getElementById('id_usuario_pass').value + '/?pass=' + document.getElementById('pass_new').value, true);
                consulta.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                // consulta.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                consulta.send();


            }</script>








        @endsection
        @section('codigos_especifico')
        <script>




            $(document).ready(function () {

            App.init();
                    @if (Session::has('insertar_usuario'))

                    mostrar_notificacion("{{session('insertar_usuario')}}");
                    @endif





            });




        </script>

        @endsection

