@extends('layouts.base')
@section('contenido')
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="row layout-top-spacing" id="cancel-row">
            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                <div class="widget-content widget-content-area br-6">
                    <h2>Listado de Usuarios</h2>
                    <a href="{{url('usuarios/create')}}"><button class="btn-success btn" style="float: right;margin-right: 25px">Insertar Usuario</button></a>
                    <div class="table-responsive mb-4 mt-4">
                        <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Rol</th>
                                    <th>Nombre</th>
                                    <th>Cargo</th>
                                    <th>Dirección</th>
                                    <th>Teléfono</th>
                                    <th>Correo</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editarModal" tabindex="-1" role="dialog" aria-labelledby="editarModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modificar Usuario</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group row  mb-4">
                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Nombre: </label>
                        <div class="col-sm-10">
                            <input id="i_id" type="nmber" name="id" class="form-control form-control-sm" id="colFormLabelSm" placeholder="col-form-label-sm" style="display: none">
                                <input id="i_nombre" type="text" name="nombre" class="form-control form-control-lg" id="colFormLabelSm" placeholder="Introduzca el nombre">
                                    </div>
                                    </div>
                                    {{csrf_field()}}
                                    <div class="form-group row mb-4">
                                        <label for="colFormLabel" class="col-sm-2 col-form-label">Cargo</label>
                                        <div class="col-sm-10">
                                            <input id="i_cargo" type="text" name="cargo" class="form-control form-control-lg" id="colFormLabel" placeholder="Introduzca el cargo">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Dirección</label>
                                        <div class="col-sm-10">
                                            <input id="i_direccion" type="text" name="direccion" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca la dirección">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Teléfono</label>
                                        <div class="col-sm-10">
                                            <input id="i_telefono" type="tel" name="telefono" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca el teléfono">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Correo</label>
                                        <div class="col-sm-10">
                                            <input id="i_correo" type="email" name="correo" class="form-control form-control-lg" id="colFormLabelLg" placeholder="Introduzca el correo">
                                        </div>
                                    </div>
                                    </div>
                                    <div class="modal-footer">

                                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancelar</button>
                                        <button type="button" id="btn_modificar" class="btn btn-primary">Modificar</button>
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                                    <div class="modal fade" id="passModal" tabindex="-1" role="dialog" aria-labelledby="passModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Cambiar Contraseña</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <p class="modal-text" style="font-size: 20px">Usuario:<span id="nombre_usuario_pass"></span></p>
                                                    <form>
                                                        <input type="number" id="id_usuario_pass" style="display: none">
                                                            Nueva Contraseña:
                                                            <input type="password" id="pass_new" class="form-control form-control-lg" placeholder="Introduzca la nueva contraseña">

                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancelar</button>
                                                                    <button id="btn_pass" type="button" class="btn btn-primary">Cambiar</button>
                                                                </div>
                                                                </div>
                                                                </div>
                                                                </div>
                                                                <div class="modal fade" id="eliminarModal" tabindex="-1" role="dialog" aria-labelledby="eliminarModalLabel" aria-hidden="true">
                                                                    <div class="modal-dialog" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h5 class="modal-title">Eliminar  Usuario</h5>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <p class="modal-text">Seguro que desea eliminar el Usuario</p>
                                                                                <form>
                                                                                    <input type="number" id="id_producto_eliminar" style="display: none">

                                                                                        <h2 id="nombre_producto"> </h2>
                                                                                        </div>
                                                                                        <div class="modal-footer">
                                                                                            <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancelar</button>
                                                                                            <button id="btn_eliminar" type="button" class="btn btn-primary">Eliminar</button>
                                                                                        </div>
                                                                                        </div>
                                                                                        </div>
                                                                                        </div>
                                                                                        <div class="modal fade" id="rolModal" tabindex="-1" role="dialog" aria-labelledby="rolModalLabel" aria-hidden="true">
                                                                                            <div class="modal-dialog" role="document">
                                                                                                <div class="modal-content">
                                                                                                    <div class="modal-header">
                                                                                                        <h5 class="modal-title">Cambiar  Rol:</h5>
                                                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                                            <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                                                                                        </button>
                                                                                                    </div>
                                                                                                    <div class="modal-body">
                                                                                                        <h2 id="nombre_user_rol"> </h2>
                                                                                                        <p class="modal-text">Rol Actual</p>
                                                                                                        <p class="text-danger" id="rol_actual" style="font-size: 20px"></p>
                                                                                                        <form>
                                                                                                            <hr>
                                                                                                                Cambiar a:
                                                                                                                <input type="number" id="id_user_rol" style="display: none">
                                                                                                                    <select class="form-control  basic" id="roles_select" onchange="mostrar_input(this.value)">
                                                                                                                        <option  value="0" selected="selected">Seleccionar Rol</option>
                                                                                                                        <option value="1">Administrador</option>
                                                                                                                        <option value="2">Supervisor</option>
                                                                                                                        <option value="3">Cliente</option>
                                                                                                                        <option value="4">Contador</option>
                                                                                                                        <option value="5" disabled="disabled">Punto de Servicio</option>
                                                                                                                        <option value="6">Proveedor</option>
                                                                                                                        <option value="7">Empleado</option>
                                                                                                                        <option value="8">Usuario</option>
                                                                                                                    </select>
                                                                                                                    <div id="input_actividad" class="form-group row mb-4" style="display: none">
                                                                                                                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Actividad</label>
                                                                                                                        <div class="col-sm-10">
                                                                                                                            <input type="text" name="actividad" class="form-control form-control-lg" id="id_actividad" placeholder="Introduzca la actividad" >
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div id="input_rubro" class="form-group row mb-4" style="display: none">
                                                                                                                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Rubro</label>
                                                                                                                        <div class="col-sm-10">
                                                                                                                            <input type="text" name="rubro" class="form-control form-control-lg" id="id_rubro" placeholder="Introduzca el rubro" >
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <!-- Inputs especificos de Proveedor -->
                                                                                                                    <div id="input_contacto" class="form-group row mb-4" style="display: none">
                                                                                                                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Contacto</label>
                                                                                                                        <div class="col-sm-10">
                                                                                                                            <input type="text" name="contacto" class="form-control form-control-lg" id="id_contacto" placeholder="Introduzca el Contacto" >
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div id="input_rut" class="form-group row mb-4" style="display: none">
                                                                                                                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Rut</label>
                                                                                                                        <div class="col-sm-10">
                                                                                                                            <input type="text" name="rut" class="form-control form-control-lg" id="id_rut" placeholder="Introduzca el Contacto" >
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <!-- Inputs especificos de empleado -->
                                                                                                                    <div id="input_e_rut" class="form-group row mb-4" style="display: none">
                                                                                                                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Rut</label>
                                                                                                                        <div class="col-sm-10">
                                                                                                                            <input type="text" name="e_rut" class="form-control form-control-lg" id="e_rut" placeholder="Introduzca el rut del empleado" >
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div id="input_e_sueldo" class="form-group row mb-4" style="display: none">
                                                                                                                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Sueldo base</label>
                                                                                                                        <div class="col-sm-10">
                                                                                                                            <input type="number" name="e_sueldo" class="form-control form-control-lg" id="e_sueldo" placeholder="Introduzca el sueldo base " >
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div id="input_e_fecha_contrato" class="form-group row mb-4" style="display: none">
                                                                                                                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Fecha del contrato</label>
                                                                                                                        <div class="col-sm-10">
                                                                                                                            <input type="date" name="e_fecha_contrato" class="form-control form-control-lg" id="e_fecha_contrato" >
                                                                                                                        </div>
                                                                                                                    </div>

                                                                                                                    <div id="input_e_tipo_contrato" class="form-group row mb-4" style="display: none">
                                                                                                                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Tipo Contrato</label>
                                                                                                                        <div class="col-sm-10">
                                                                                                                            <select name="e_tipo_contrato" id="e_tipo_contrato" class="placeholder js-states form-control" required="required" >
                                                                                                                                <option   value="Fijo" selected >Fijo</option>
                                                                                                                                <option value="Temporal">Temporal</option>
                                                                                                                                <option value="Suplencia">Suplencia</option>

                                                                                                                            </select>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div id="input_e_movilizacion" class="form-group row mb-4" style="display: none">
                                                                                                                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Costo de Movilización</label>
                                                                                                                        <div class="col-sm-10">
                                                                                                                            <input type="number" name="e_movilizacion" class="form-control form-control-lg" id="e_movilizacion" >
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div id="input_e_colocacion" class="form-group row mb-4" style="display: none">
                                                                                                                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Costo de Colocación</label>
                                                                                                                        <div class="col-sm-10">
                                                                                                                            <input type="number" name="e_colocacion" class="form-control form-control-lg" id="e_colocacion" >
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <!-- Inputs especificos de punto de sercvicio -->
                                                                                                                    <div id="input_folio" class="form-group row mb-4" style="display: none">
                                                                                                                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Folio</label>
                                                                                                                        <div class="col-sm-10">
                                                                                                                            <input type="text" name="folio" class="form-control form-control-lg" id="id_folio" placeholder="Introduzca el folio" >
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div id="input_encargado" class="form-group row mb-4" style="display: none">
                                                                                                                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Encargado</label>
                                                                                                                        <div class="col-sm-10">
                                                                                                                            <input type="text" name="encargado" class="form-control form-control-lg" id="id_encargado" placeholder="Introduzca el nombre del encargado" >
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div id="input_cant_personas" class="form-group row mb-4" style="display: none">
                                                                                                                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Cantidad de Personas</label>
                                                                                                                        <div class="col-sm-10">
                                                                                                                            <input type="number" name="cant_personas" class="form-control form-control-lg" id="id_cant_personas" placeholder="Introduzca la cantidad de personas"  >
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div id="input_fecha_antes" class="form-group row mb-4" style="display: none">
                                                                                                                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Fecha desde antes</label>
                                                                                                                        <div class="col-sm-10">
                                                                                                                            <input type="date" name="fecha_antes" class="form-control form-control-lg" id="id_fecha_antes" placeholder="Introduzca la fecha"  >
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div id="input_supervisor" class="form-group row mb-4" style="display: none">
                                                                                                                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Supervisor</label>
                                                                                                                        <div class="col-sm-10">
                                                                                                                            <input type="number" name="supervisor" class="form-control form-control-lg" id="id_supervisor" placeholder="Introduzca el supervisor"  >
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div id="input_descripcion" class="form-group row mb-4" style="display: none">
                                                                                                                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Descripcion</label>
                                                                                                                        <div class="col-sm-10">
                                                                                                                            <textarea name="descripcion" class="form-control mb-4" rows="3" id="id_descripcion"></textarea>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div id="input_precio" class="form-group row mb-4" style="display: none">
                                                                                                                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Precio Mensual</label>
                                                                                                                        <div class="col-sm-10">
                                                                                                                            <input type="number" name="precio" class="form-control form-control-lg" id="id_precio" placeholder="Introduzca el precio mensual"  >
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    </div>
                                                                                                                    <div class="modal-footer">
                                                                                                                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Cancelar</button>
                                                                                                                        <button id="btn_cambiar_rol" type="button" class="btn btn-primary">Cambiar</button>
                                                                                                                    </div>
                                                                                                                    </div>
                                                                                                                    </div>
                                                                                                                    </div>

                                                                                                                    <script>
                                                                                                                        function  mostrar_input(id) {
                                                                                                                            document.getElementById('input_actividad').style.display = 'none';
                                                                                                                            document.getElementById('input_rubro').style.display = 'none';
                                                                                                                            document.getElementById('input_contacto').style.display = 'none';
                                                                                                                            document.getElementById('input_folio').style.display = 'none';
                                                                                                                            document.getElementById('input_encargado').style.display = 'none';
                                                                                                                            document.getElementById('input_cant_personas').style.display = 'none';
                                                                                                                            document.getElementById('input_fecha_antes').style.display = 'none';
                                                                                                                            document.getElementById('input_supervisor').style.display = 'none';
                                                                                                                            document.getElementById('input_descripcion').style.display = 'none';
                                                                                                                            document.getElementById('input_precio').style.display = 'none';
                                                                                                                            document.getElementById('input_rut').style.display = 'none';
                                                                                                                            document.getElementById('input_e_colocacion').style.display = 'none';
                                                                                                                            document.getElementById('input_e_fecha_contrato').style.display = 'none';
                                                                                                                            document.getElementById('input_e_movilizacion').style.display = 'none';
                                                                                                                            document.getElementById('input_e_rut').style.display = 'none';
                                                                                                                            document.getElementById('input_e_sueldo').style.display = 'none';
                                                                                                                            document.getElementById('input_e_tipo_contrato').style.display = 'none';
                                                                                                                            if (id == 3) {
                                                                                                                                //mostrar campos de clientes
                                                                                                                                document.getElementById('input_actividad').style.display = '';
                                                                                                                                document.getElementById('input_rubro').style.display = '';
                                                                                                                            } else
                                                                                                                            if (id == 6)
                                                                                                                            {// mostrar campos de proveedor
                                                                                                                                document.getElementById('input_contacto').style.display = '';
                                                                                                                                document.getElementById('input_rut').style.display = '';
                                                                                                                            } else
                                                                                                                            if (id == 5) {
                                                                                                                                //mostrar campos de punto de servicio
                                                                                                                                document.getElementById('input_folio').style.display = '';
                                                                                                                                document.getElementById('input_encargado').style.display = '';
                                                                                                                                document.getElementById('input_cant_personas').style.display = '';
                                                                                                                                document.getElementById('input_fecha_antes').style.display = '';
                                                                                                                                document.getElementById('input_supervisor').style.display = '';
                                                                                                                                document.getElementById('input_descripcion').style.display = '';
                                                                                                                                document.getElementById('input_precio').style.display = '';
                                                                                                                            } else
                                                                                                                            if (id == 7) {
                                                                                                                                document.getElementById('input_e_colocacion').style.display = '';
                                                                                                                                document.getElementById('input_e_fecha_contrato').style.display = '';
                                                                                                                                document.getElementById('input_e_movilizacion').style.display = '';
                                                                                                                                document.getElementById('input_e_rut').style.display = '';
                                                                                                                                document.getElementById('input_e_sueldo').style.display = '';
                                                                                                                                document.getElementById('input_e_tipo_contrato').style.display = '';

                                                                                                                            }
                                                                                                                        }

                                                                                                                    </script>


                                                                                                                    @endsection
                                                                                                                    @section('codigos_especifico')
                                                                                                                    <script>



                                                                                                                        $('#html5-extension').DataTable({
                                                                                                                            dom: '<"row"<"col-md-12"<"row"<"col-md-6"l><"col-md-6"f> > >B<"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >',
                                                                                                                            buttons: {
                                                                                                                                buttons: [
                                                                                                                                    {extend: 'copy', className: 'btn'},
                                                                                                                                    {extend: 'csv', className: 'btn'},
                                                                                                                                    {extend: 'excel', className: 'btn'},
                                                                                                                                    {extend: 'print', className: 'btn'}
                                                                                                                                ]
                                                                                                                            },
                                                                                                                            "oLanguage": {
                                                                                                                                "oPaginate": {"sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'},
                                                                                                                                "sInfo": "Mostrada pagina _PAGE_ de _PAGES_",
                                                                                                                                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                                                                                                                                "sSearchPlaceholder": "Buscar",
                                                                                                                                "sLengthMenu": "Results :  _MENU_",
                                                                                                                            },
                                                                                                                            "columnDefs": [
                                                                                                                                {
                                                                                                                                    "targets": -1,
                                                                                                                                    "data": null,
                                                                                                                                    "searchable": false,
                                                                                                                                    "defaultContent": '<a id="editar"   title="Editar"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></a><a id="eliminar"  title="Eliminar"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg></a>         \n\
                                                                                                                              <a id="cambiar_rol"   title="Rol"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg></a>       \n\
                                                                                                                                           <a id="cambiar_pass"   title="Cambiar Contraseña"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-lock"><rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect><path d="M7 11V7a5 5 0 0 1 10 0v4"></path></svg></a>'
                                                                                                                                },
                                                                                                                                {
                                                                                                                                    "targets": [0],
                                                                                                                                    "visible": false,
                                                                                                                                    "data": 'id'
                                                                                                                                },
                                                                                                                                {
                                                                                                                                    "targets": [1],
                                                                                                                                    "data": 'rol'
                                                                                                                                },
                                                                                                                                {
                                                                                                                                    "targets": [2],
                                                                                                                                    "data": 'nombre'
                                                                                                                                },
                                                                                                                                {
                                                                                                                                    "targets": [3],
                                                                                                                                    "data": 'cargo'
                                                                                                                                },
                                                                                                                                {
                                                                                                                                    "targets": [4],
                                                                                                                                    "data": 'direccion'
                                                                                                                                },
                                                                                                                                {
                                                                                                                                    "targets": [5],
                                                                                                                                    "data": 'telefono'
                                                                                                                                },
                                                                                                                                {
                                                                                                                                    "targets": [6],
                                                                                                                                    "data": 'email'
                                                                                                                                },
                                                                                                                            ],
                                                                                                                            "stripeClasses": [],
                                                                                                                            "lengthMenu": [10, 20, 50, 100, 1000000],
                                                                                                                            "pageLength": 10,
                                                                                                                            "processing": true,
                                                                                                                            "serverSide": true,
                                                                                                                            "ajax": "{{url('/usuarios')}}"
                                                                                                                        });
                                                                                                                        function correo_valid() {

                                                                                                                            if (document.getElementById('i_correo').value.substr(-10) !== '@gmail.com') {
                                                                                                                                mostrar_notificacion('El correo debe ser de gmail para ingresar el usuario al sistema');
                                                                                                                                return false;
                                                                                                                            } else {
                                                                                                                                return true;
                                                                                                                            }

                                                                                                                        }
                                                                                                                        function telefono_valid() {

                                                                                                                            if (document.getElementById('i_telefono').value.substr(0, 4) !== '+569') {
                                                                                                                                // mostrar_notificacion(document.getElementById('i_telefono').value.substr(0, 4));
                                                                                                                               mostrar_notificacion('El numero de teléfono debe comenzar con 569 ');
                                                                                                                                return false;
                                                                                                                            } else {
                                                                                                                                return true;
                                                                                                                            }

                                                                                                                        }
                                                                                                                        $(document).ready(function () {
                                                                                                                            $('#e_rut').inputmask("999999999-9");

                                                                                                                            App.init();
                                                                                                                                    @if (Session::has('insertar_usuario'))

                                                                                                                            mostrar_notificacion("{{session('insertar_usuario')}}");

                                                                                                                            @endif

                                                                                                                                    var table = $('#html5-extension').DataTable();
                                                                                                                            $('#html5-extension tbody').on('click', '#eliminar', function () {
                                                                                                                                var data = table.row($(this).parents('tr')).data();
                                                                                                                                document.getElementById('id_producto_eliminar').value = data['id'];
                                                                                                                                document.getElementById('nombre_producto').innerHTML = data['nombre'];
                                                                                                                                $('#eliminarModal').modal('show');
                                                                                                                            });
                                                                                                                            $('#html5-extension tbody').on('click', '#cambiar_rol', function (e) {
                                                                                                                                e.preventDefault();
                                                                                                                                var data = table.row($(this).parents('tr')).data();
                                                                                                                                document.getElementById('rol_actual').innerHTML = data['rol'];
                                                                                                                                document.getElementById('nombre_user_rol').innerHTML = data['nombre'];
                                                                                                                                document.getElementById('id_user_rol').value = data['id'];

                                                                                                                                $('#rolModal').modal('show');

                                                                                                                            });
                                                                                                                            $('#html5-extension tbody').on('click', '#editar', function () {
                                                                                                                                var data = table.row($(this).parents('tr')).data();
                                                                                                                                document.getElementById('i_id').value = data['id'];
                                                                                                                                document.getElementById('i_nombre').value = data['nombre'];
                                                                                                                                document.getElementById('i_cargo').value = data['cargo'];
                                                                                                                                document.getElementById('i_direccion').value = data['direccion'];
                                                                                                                                document.getElementById('i_correo').value = data['email'];
                                                                                                                                document.getElementById('i_telefono').value = data['telefono'];

                                                                                                                                $('#editarModal').modal('show');
                                                                                                                            });
                                                                                                                            $('#html5-extension tbody').on('click', '#cambiar_pass', function () {
                                                                                                                                var data = table.row($(this).parents('tr')).data();
                                                                                                                                document.getElementById('id_usuario_pass').value = data['id'];
                                                                                                                                document.getElementById('nombre_usuario_pass').innerHTML = data['nombre'];


                                                                                                                                $('#passModal').modal('show');
                                                                                                                            });
                                                                                                                            $('#eliminarModal').on('click', '#btn_eliminar', function (e) {

                                                                                                                                e.preventDefault();
                                                                                                                                var consulta = new XMLHttpRequest();
                                                                                                                                if (!window.XMLHttpRequest) {
                                                                                                                                    consulta = new ActiveXObject("Microsoft.XMLHTTP");
                                                                                                                                }
                                                                                                                                consulta.onreadystatechange = function () {
                                                                                                                                    if (consulta.readyState === 4 && consulta.status === 200) {

                                                                                                                                        var tabla = $("#html5-extension").DataTable();
                                                                                                                                        tabla.ajax.reload();
                                                                                                                                        mostrar_notificacion(consulta.responseText);
                                                                                                                                        $('#eliminarModal').modal('hide');
                                                                                                                                    }

                                                                                                                                };
                                                                                                                                consulta.open("DELETE", 'usuarios/' + document.getElementById('id_producto_eliminar').value, true);
                                                                                                                                consulta.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                                                                                                                                // consulta.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                                                                                                                consulta.send();
                                                                                                                            });
                                                                                                                            $('#editarModal').on('click', '#btn_modificar', function (e) {
                                                                                                                                e.preventDefault();
                                                                                                                                var consulta_m = new XMLHttpRequest();
                                                                                                                                if (!window.XMLHttpRequest) {
                                                                                                                                    consulta_m = new ActiveXObject("Microsoft.XMLHTTP");
                                                                                                                                }
                                                                                                                                consulta_m.onreadystatechange = function () {
                                                                                                                                    if (consulta_m.readyState === 4 && consulta_m.status === 200) {

                                                                                                                                        var tabla = $("#html5-extension").DataTable();
                                                                                                                                        tabla.ajax.reload();
                                                                                                                                        mostrar_notificacion(consulta_m.responseText);
                                                                                                                                        $('#editarModal').modal('hide');
                                                                                                                                    }

                                                                                                                                };
                                                                                                                                var id = document.getElementById('i_id').value;
                                                                                                                                var nombre = document.getElementById('i_nombre').value;
                                                                                                                                var cargo = document.getElementById('i_cargo').value;
                                                                                                                                var direccion = document.getElementById('i_direccion').value;
                                                                                                                                var telefono = document.getElementById('i_telefono').value;
                                                                                                                                var correo = document.getElementById('i_correo').value;
                                                                                                                                var valores = '?id=' + id + '&nombre=' + nombre + '&cargo=' + cargo + '&direccion=' + direccion + '&telefono=' +telefono + '&correo=' + correo;
                                                                                                                                consulta_m.open("PUT", 'usuarios/' + document.getElementById('i_id').value + '/' + valores, true);
                                                                                                                                consulta_m.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                                                                                                                                // consulta.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                                                                                                                if (telefono_valid() && correo_valid()) {
                                                                                                                                    consulta_m.send();
                                                                                                                                }
                                                                                                                            });
                                                                                                                            $('#rolModal').on('click', '#btn_cambiar_rol', function (e) {
                                                                                                                                e.preventDefault();

                                                                                                                                if (document.getElementById('roles_select').value !== '0') {

                                                                                                                                    var consulta_m = new XMLHttpRequest();
                                                                                                                                    if (!window.XMLHttpRequest) {
                                                                                                                                        consulta_m = new ActiveXObject("Microsoft.XMLHTTP");
                                                                                                                                    }
                                                                                                                                    consulta_m.onreadystatechange = function () {
                                                                                                                                        if (consulta_m.readyState === 4 && consulta_m.status === 200) {

                                                                                                                                            var tabla = $("#html5-extension").DataTable();
                                                                                                                                            tabla.ajax.reload();
                                                                                                                                            mostrar_notificacion(consulta_m.responseText);
                                                                                                                                            $('#rolModal').modal('hide');
                                                                                                                                        }
                                                                                                                                    };
                                                                                                                                    //si el cambio de rol es para cliente
                                                                                                                                    if (document.getElementById('roles_select').value == '3') {
                                                                                                                                        if (document.getElementById('id_actividad').value !== '' && document.getElementById('id_rubro').value !== '') {
                                                                                                                                            var id = document.getElementById('id_user_rol').value;
                                                                                                                                            var actividad = document.getElementById('id_actividad').value;
                                                                                                                                            var rubro = document.getElementById('id_rubro').value;
                                                                                                                                            var valores = '?id=' + id + '&actividad=' + actividad + '&rubro=' + rubro + '&rol=3';
                                                                                                                                            consulta_m.open("PUT", 'usuarios/' + document.getElementById('id_user_rol').value + '/' + valores, true);
                                                                                                                                            consulta_m.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                                                                                                                                            consulta_m.send();
                                                                                                                                        } else {
                                                                                                                                            alert('Los valores de los campos no puedes estas vacios');
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                    //si el cambio de rol es para proveedor
                                                                                                                                    if (document.getElementById('roles_select').value == '6') {
                                                                                                                                        if (document.getElementById('id_contacto').value !== '') {
                                                                                                                                            var id = document.getElementById('id_user_rol').value;
                                                                                                                                            var contacto = document.getElementById('id_contacto').value;
                                                                                                                                            var rut = document.getElementById('id_rut').value;
                                                                                                                                            var valores = '?id=' + id + '&contacto=' + contacto + '&rol=6' + '&rut=' + rut;
                                                                                                                                            consulta_m.open("PUT", 'usuarios/' + document.getElementById('id_user_rol').value + '/' + valores, true);
                                                                                                                                            consulta_m.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                                                                                                                                            consulta_m.send();
                                                                                                                                        } else {
                                                                                                                                            alert('Los valores de los campos no puedes estas vacios');
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                    if (document.getElementById('roles_select').value == '7') {
                                                                                                                                        if (document.getElementById('e_rut').value !== '' && document.getElementById('e_colocacion') !== '0' && document.getElementById('e_movilizacion').value !== 0 && document.getElementById('e_tipo_contrato').value !== '' && document.getElementById('e_sueldo').value !== '' && document.getElementById('input_e_tipo_contrato').value !== '') {
                                                                                                                                            var id = document.getElementById('id_user_rol').value;
                                                                                                                                            var rut = document.getElementById('e_rut').value;
                                                                                                                                            var colocacion = document.getElementById('e_colocacion').value;
                                                                                                                                            var fecha_contrato = document.getElementById('e_fecha_contrato').value;
                                                                                                                                            var movilizacion = document.getElementById('e_movilizacion').value;
                                                                                                                                            var sueldo = document.getElementById('e_sueldo').value;
                                                                                                                                            var tipo_contrato = document.getElementById('e_tipo_contrato').value;
                                                                                                                                            var valores = '?id=' + id + '&rut=' + rut + '&rol=7' + '&colocacion=' + colocacion + '&fecha_contrato=' + fecha_contrato + '&movilizacion=' + movilizacion + '&sueldo=' + sueldo + '&tipo_contrato="' + tipo_contrato + '"';
                                                                                                                                            console.log(valores);
                                                                                                                                            consulta_m.open("PUT", 'usuarios/' + document.getElementById('id_user_rol').value + '/' + valores, true);
                                                                                                                                            consulta_m.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                                                                                                                                            consulta_m.send();
                                                                                                                                        } else {
                                                                                                                                            alert('Los valores de los campos no puedes estas vacios');
                                                                                                                                        }

                                                                                                                                    }
                                                                                                                                    if (document.getElementById('roles_select').value !== '6' && document.getElementById('roles_select').value !== '3' && document.getElementById('roles_select').value !== '7') {
                                                                                                                                        var id = document.getElementById('id_user_rol').value;
                                                                                                                                        var valores = '?id=' + id + '&rol=' + document.getElementById('roles_select').value;
                                                                                                                                        consulta_m.open("PUT", 'usuarios/' + document.getElementById('id_user_rol').value + '/' + valores, true);
                                                                                                                                        consulta_m.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                                                                                                                                        consulta_m.send();
                                                                                                                                    }
                                                                                                                                } else {
                                                                                                                                    alert('Seleccione un nuevo rol');
                                                                                                                                }
                                                                                                                            });
                                                                                                                            $('#passModal').on('click', '#btn_pass', function (e) {

                                                                                                                                e.preventDefault();
                                                                                                                                var consulta = new XMLHttpRequest();
                                                                                                                                if (!window.XMLHttpRequest) {
                                                                                                                                    consulta = new ActiveXObject("Microsoft.XMLHTTP");
                                                                                                                                }
                                                                                                                                consulta.onreadystatechange = function () {
                                                                                                                                    if (consulta.readyState === 4 && consulta.status === 200) {

                                                                                                                                        var tabla = $("#html5-extension").DataTable();
                                                                                                                                        tabla.ajax.reload();
                                                                                                                                        mostrar_notificacion(consulta.responseText);
                                                                                                                                        $('#passModal').modal('hide');
                                                                                                                                    }

                                                                                                                                };
                                                                                                                                consulta.open("PUT", 'usuarios_pass/' + document.getElementById('id_usuario_pass').value + '/?pass=' + document.getElementById('pass_new').value, true);
                                                                                                                                consulta.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                                                                                                                                // consulta.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                                                                                                                consulta.send();
                                                                                                                            });


                                                                                                                        }
                                                                                                                        );
                                                                                                                    </script>

                                                                                                                    @endsection

