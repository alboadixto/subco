<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Producto;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    $user = Auth::user();
    if ($user) {
        return redirect('/admin');
    } else {
        return redirect('/login');
    }
});

Route::get('/home', 'AdminController@inicioAdministracion');
//Autenticacion de usuarios
Auth::routes();
//Panel
Route::get('/admin', 'AdminController@inicioAdministracion');

//PRODUCTOS
Route::get('/listar_productos', 'ProductosController@listar_view');
Route::put('/productos/enviar/{id}', 'ProductosController@enviar');
Route::resource('/productos', 'ProductosController');
Route::get('/productos_pto/{id}', 'ProductosController@productos_pto');
//USUARIOS
Route::get('/listar_usuarios', 'UsuariosController@listar_view');
Route::put('/usuarios_pass/{id}', 'UsuariosController@cambiar_pass');
Route::get('/mi_usuario', 'UsuariosController@mi_usuario');
Route::resource('/usuarios', 'UsuariosController');
//COMPRA Y VENTA
Route::get('/listar_cv', 'CompraVentasController@listar_view');
Route::get('/listar_compras', 'CompraVentasController@listar_compras');
Route::get('/listar_ventas', 'CompraVentasController@listar_ventas');

Route::resource('/cv', 'CompraVentasController');

//FACTURAS
Route::get('/listar_clientes', 'FacturasController@clientes_view');
Route::get('/listar_facturas', 'FacturasController@listar_view');
Route::put('/act_estado/{id}/', 'FacturasController@update_estado');
Route::get('/facturas/clientes', 'FacturasController@clientes');
Route::get('/facturas/ventas', 'FacturasController@ventas');
Route::get('/facturas/compras', 'FacturasController@compras');
Route::get('/facturas/compras/{f_inicio}/{f_final}', 'FacturasController@compras_rango');
Route::get('/facturas/venta/balance/{f_inicio}/{f_final}', 'FacturasController@venta_balance');
Route::get('/facturas/compra/balance/{f_inicio}/{f_final}', 'FacturasController@compra_balance');
Route::get('/facturas/ventas/{f_inicio}/{f_final}', 'FacturasController@ventas_rango');
Route::resource('/facturas', 'FacturasController');

//EMPLEADOS
Route::get('/listar_empleados', 'EmpleadosController@listar_view');
Route::resource('/empleados', 'EmpleadosController');
Route::get('empleados/detalles/{id}', 'EmpleadosController@usuario_de_empleado');
Route::get('empleados/punto/{id}', 'EmpleadosController@punto');
Route::put('empleados/asignar/{id_user}/{id_pto}', 'EmpleadosController@asignar_pto');

//PUNTO DE SERVICIO
Route::put('inventario/mod_cant/{id}', 'PuntoController@mod_cant');
Route::get('/listar_puntos', 'PuntoController@listar_view');
Route::get('/inventario/{id}', 'PuntoController@inventario');
Route::get('/inventario_punto/{id}', 'PuntoController@listar_inventario');
Route::delete('inventario/quitar_prod_inv/{id}', 'PuntoController@elimiar_prod_inventario');
Route::resource('/puntos', 'PuntoController');

//CONTRATOS 
Route::get('contratos/crear_contrato_punto', 'ContratoPuntoController@crear_contrato_view');
Route::post('contratos/crear_pto', 'ContratoPuntoController@crear_contrato');
Route::get('contratos/pto/ver/{id}', 'ContratoPuntoController@mostrar_contrato_punto');
Route::post('contratos/pto/edit/{id}', 'ContratoPuntoController@edit_contrato_punto');
Route::get('contratos/crear_contrato_empleado', 'ContratoEmpleadoController@crear_contrato_empleado_view');
Route::post('contratos/crear', 'ContratoEmpleadoController@crear');
Route::post('contratos/actualizar', 'ContratoEmpleadoController@actualizar');
Route::get('contratos/ver/{id_empleado}', 'ContratoEmpleadoController@mostrar_contrato_empleado');
Route::get('contratos/empleado/termino', 'ContratoEmpleadoController@crear_termino_empleado_view');
Route::post('contratos/empleado/termino/crear', 'ContratoEmpleadoController@crear_termino_empleado');
Route::get('contratos/empleado/termino/detalles/{id}', 'ContratoEmpleadoController@detalles_termino_empleado');
Route::delete('contratos/empleado/termino/eliminar/{id}', 'ContratoEmpleadoController@eliminar_termino_contrato_empleado');
Route::get('contratos/empleado/mostar_terminos', 'ContratoEmpleadoController@termino_empleado_view');
Route::get('contratos/empleado/termino_json', 'ContratoEmpleadoController@terminos_json');

//ASISTENCIA
Route::get('asistencia/hoy/{id_empleado}', 'AsistenciaController@hoy_view');
Route::post('asistencia/confirmar/{id}', 'AsistenciaController@asistencia_confirmada');
Route::post('asistencia/denegar/{id}', 'AsistenciaController@asistencia_denegada');
Route::get('asistencia/dias_antes/{id_empleado}', 'AsistenciaController@dias_entes_view');
Route::get('asistencia/empleado_view/{id}/{inicio}/{fin}', 'AsistenciaController@asistencia_empleado_view');
Route::get('asistencia/empleados_view', 'AsistenciaController@empleados_view');
Route::get('asistencia/empleado/{id}/{inicio}/{fin}', 'AsistenciaController@asistencia_empleado');
Route::get('asistencia/empleados', 'AsistenciaController@asistencia_empleados');
//Route::get('asistencia/general', 'AsistenciaController@asistencia_empleados');
Route::post('asistencia/hoy/informar/{id_empleado}', 'AsistenciaController@informar_hoy');
Route::post('asistencia/dia/informar/{id_empleado}', 'AsistenciaController@informar_dia');
//NOTIFICACIÓN
Route::put('notificacion/crear', 'NotificacionsController@crear');
//INFORMAR DE PERDIDAS
Route::get('perdidas/calcular/{inicio}/{fin}', 'PerdidasController@calcular_total_perdidas');
Route::get('perdidas/informar', 'PerdidasController@informar_view');
Route::get('perdidas/json', 'PerdidasController@perdidas_json');
Route::get('perdidas/listar_perdidas', 'PerdidasController@listar_view');
Route::get('perdidas/listar_perdidas_json_all', 'PerdidasController@perdidas_json_all');
Route::get('perdidas/listar_perdidas_all', 'PerdidasController@listar_view_all');
Route::post('perdidas/confirmar/{id}', 'PerdidasController@confirmar');
Route::post('perdidas/eliminar/{id}', 'PerdidasController@eliminar');
//UNIFORME
Route::resource('/uniforme','UniformeController');
Route::get('/uniforme/ver/empleados_tallas_json','UniformeController@empleados_talla_json');
Route::get('/uniforme/ver/lista_empleados','UniformeController@listar_empleados');
Route::get('/uniforme/asignar/{id}','UniformeController@asignar_view');
Route::post('/uniforme/empleado/asignar','UniformeController@guardar_uniforme_empleado');
Route::get('/uniforme/empleado/eliminar/{id}','UniformeController@eliminar_uniforme_empleado');
Route::get('/uniforme/gastos/ver','UniformeController@uniforme_gastos');
Route::get('/uniforme/gastos/calcular/{inicio}/{fin}','UniformeController@uniforme_gastos_calculo');
//NOMINA
Route::get('nomina/{id_empleado}/{inicio}/{fin}/{provida}/{anticipo}/{bono}/{otro_pago}/{otro_descuento}', 'AsistenciaController@pago_empleados');
Route::get('nomina/todos','AsistenciaController@listar');
//INFORMES
Route::resource('informe','InformesController');
Route::get('informe/calculo/empleados','InformesController@calcular_empleados');
Route::get('informe/calculo/pto','InformesController@calcular_pto_servicio');
